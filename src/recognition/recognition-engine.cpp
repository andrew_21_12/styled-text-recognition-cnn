#include "recognition/recognition-engine.hpp"

#include <QTemporaryFile>
#include <qmath.h>
#include <QThread>
#include <QDebug>

#include "utility/images-processor.hpp"
#include "subroutines/cnn-learning-helper.hpp"
#include "utility/math-helper.hpp"
#include "utility/boxes-helper.hpp"
#include "recognition/energy-batch-evaluator.hpp"

RecognitionEngine::RecognitionEngine(bool& rbSuccess, QObject* pParent):
        QObject(pParent),
        m_SymbolsAlphabet(QLatin1String("0123456789"
                                        "abcdefghijklmnopqrstuvwzyx"
                                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ")),
        m_pHunspell(Q_NULLPTR)
{
    // Opening serialized net resource.
    QFile serializedNetResFile(QLatin1String(":/resources/nets/recognition_latest.dat"));
    if (!serializedNetResFile.open(QFile::ReadOnly))
    {
        rbSuccess = false;
        return;
    }

    // Opening dictionary files.
    QFile dictionaryAffResFile(QLatin1String(":/resources/dictionaries/en_US/en_US.aff"));
    QFile dictionaryDicResFile(QLatin1String(":/resources/dictionaries/en_US/en_US.dic"));
    if (!dictionaryAffResFile.open(QFile::ReadOnly) || !dictionaryDicResFile.open(QFile::ReadOnly))
    {
        rbSuccess = false;
        return;
    }



    // Writing serialized net to the external temp file.
    QTemporaryFile tempFileToLoadNet;
    if (!tempFileToLoadNet.open())
    {
        serializedNetResFile.close();
        dictionaryAffResFile.close();
        dictionaryDicResFile.close();
        rbSuccess = false;
        return;
    }
    tempFileToLoadNet.write(serializedNetResFile.readAll());
    serializedNetResFile.close();
    tempFileToLoadNet.close();

    // Deserializing temp file extracted from resources.
    dlib::deserialize(tempFileToLoadNet.fileName().toStdString()) >> m_LearnedNet;
    rbSuccess = true;



    // Writing loaded dictionary files to the external temp files.
    QTemporaryFile tempFileToLoadAff, tempFileToLoadDic;
    if (!tempFileToLoadAff.open() || !tempFileToLoadDic.open())
    {
        dictionaryAffResFile.close();
        dictionaryDicResFile.close();
        tempFileToLoadAff.close();
        tempFileToLoadDic.close();
        rbSuccess = false;
        return;
    }
    tempFileToLoadAff.write(dictionaryAffResFile.readAll());
    tempFileToLoadDic.write(dictionaryDicResFile.readAll());
    dictionaryAffResFile.close();
    dictionaryDicResFile.close();
    tempFileToLoadAff.close();
    tempFileToLoadDic.close();

    // Initializing Hunspell.
    m_pHunspell = new Hunspell(
            tempFileToLoadAff.fileName().toLatin1().data(),
            tempFileToLoadDic.fileName().toLatin1().data());
}

RecognitionEngine::~RecognitionEngine()
{
    delete m_pHunspell;
}

RecognitionEngine& RecognitionEngine::setSymbolsAlphabet(const QString& rcSymbolsAlphabet)
{
    m_SymbolsAlphabet = rcSymbolsAlphabet;
    return *this;
}



void RecognitionEngine::recognizeDetectedWordImage(const QString& rcImageFilePath,
                                                   QString& rOutRecognizedString,
                                                   QString& rPossibleError,
                                                   const bool cbVerbose)
{
    // Checking and loading image.
    rPossibleError = QString(); // resetting an error string
    if (rcImageFilePath == QString())
    {
        rPossibleError = tr("An empty file path to the image was provided");
        return;
    }
    const QImage cImageToBeRecognized(rcImageFilePath);

    // Recognizing an image.
    recognizeDetectedWordImage(cImageToBeRecognized, rOutRecognizedString, rPossibleError, cbVerbose);
}

void RecognitionEngine::recognizeDetectedWordImage(const QImage& rcWordImage,
                                                   QString& rOutRecognizedString,
                                                   QString& rPossibleError,
                                                   const bool cbVerbose)
{
    // Configs.
    const quint16 cnScaledImgHeight = 96; // the height to scale image to perform all operations
    const qreal cInitialFeatureMinSquareCoef = 0.00002, // default features min square coef
                cFeatureMinSquareStep = 0.00001;        // a step to increase features min square coef
    const quint16 cnMinBoundingBoxHeight = (quint16) qRound(cnScaledImgHeight * 0.45),    // bounding boxes should be at least of 45% of image height
                  cnMinBoundingBoxWidth = (quint16) qRound(cnMinBoundingBoxHeight * 0.2); // BBs widths should be at least of 20% of their heights
    const qreal cTooBroadBoxWidthPart = 2.25;                                             // if the width of the box is more then this part of its height, then we won't take into account this box
    const quint8 cnMaxAttemptsForResegmentation = 5; // how many times we should try to extract bounding boxes
    const quint16 cnMaxBoundingBoxesCount = 300;     // how many boxes can be provided to the classifier at max
    const quint8 cnBoxAdditionalCaptureHorizontal = 3, // extension values for bounding boxes
                 cnBoxAdditionalCaptureVertical = 3,
                 cnBoxWidthAddition = 6,
                 cnBoxHeightAddition = 6;
    const qreal cDuplicatedBoxesThresholdCoef = 0.5 * 0.35; // the second value - is a part of mean width / height to use as a threshold to remove similar bounding boxes
    const QList<quint8> cDeltasForNMS({ });                 // values of deltas for waves of NMS to filter classified windows
    const qreal cMaxSymbolsDistanceInWordCoef = 1.135; // a multiplier for mean distance between all detected windows to evaluate word dividing width
    const quint8 cnMaxBatchSizeForWord = 21;           // how many bounding boxes can we analyze at once to find the best word construction (must be less then 32)



    // Resetting an error string.
    rPossibleError = QString();

    // Checking an image.
    if (rcWordImage.isNull())
    {
        rPossibleError = tr("Provided image is empty or corrupted");
        return;
    }



    // Evaluating proportional width to scale provided images.
    const qreal cScale = cnScaledImgHeight / (qreal) rcWordImage.height();
    const quint16 cnScaledImgWidth = (quint16) qRound(rcWordImage.width() * cScale);



    // Extracting all bounding boxes.
    QList<QRect> boundingBoxes;
    quint8 nCurrentAttempt = 0;
    qreal actualFeatureMinSquareCoef = cInitialFeatureMinSquareCoef;
    auto funcBoundingBoxesExceedsLimitChecker = [&]() -> bool
    {
        return boundingBoxes.size() > cnMaxBoundingBoxesCount;
    };
    do
    {
        if (funcBoundingBoxesExceedsLimitChecker() && cbVerbose)
            qInfo() << tr("Too many bounding boxes were detected (%1), trying to extract them again")
                      .arg(boundingBoxes.size());
        extractAllBoundingBoxes(rcWordImage,
                                cnScaledImgWidth, cnScaledImgHeight,
                                actualFeatureMinSquareCoef,
                                boundingBoxes,
                                rPossibleError);
        if (rPossibleError != QString())
            return;
        // Reducing count of bounding boxes.
        filterAndSortBoundingBoxes(boundingBoxes, cnMinBoundingBoxHeight, cnMinBoundingBoxWidth, cTooBroadBoxWidthPart);
        // Increasing min square coef.
        actualFeatureMinSquareCoef += cFeatureMinSquareStep;
    }
    while (++nCurrentAttempt < cnMaxAttemptsForResegmentation && funcBoundingBoxesExceedsLimitChecker());

    // Checking result of boxes extraction.
    if (funcBoundingBoxesExceedsLimitChecker())
    {
        rPossibleError = tr("Provided image is too big (has %1 features) to perform recognition").arg(boundingBoxes.size());
        return;
    }
    else if (cbVerbose)
        qInfo() << tr("Source image was clusterized, features were extracted: %1").arg(boundingBoxes.size());



    // Preparing an image to extract windows for CNN from.
    const QImage cImageToExtractWindows = rcWordImage.scaled(cnScaledImgWidth, cnScaledImgHeight,
                                                             Qt::KeepAspectRatio, Qt::SmoothTransformation);

    // Running sliding windows.
    QList<qreal> confidences;
    QList<QChar> recognizedSymbols;
    classifyAndModifyBoundingBoxes(cImageToExtractWindows, boundingBoxes,
                                   cnBoxAdditionalCaptureHorizontal, cnBoxAdditionalCaptureVertical,
                                   cnBoxWidthAddition, cnBoxHeightAddition,
                                   confidences, recognizedSymbols,
                                   rPossibleError);
    if (rPossibleError != QString())
        return;
    if (cbVerbose)
        qInfo() << tr("Features are classified");



    // Filtering classified symbols.
    filterClassifiedBoundingBoxes(confidences, boundingBoxes, recognizedSymbols,
                                  cDuplicatedBoxesThresholdCoef, cDeltasForNMS);
    if (cbVerbose)
        qInfo() << tr("Symbols classifications were filtered and reduced to %1").arg(recognizedSymbols.size());



    // Predicting string.
    constructStringFromClassifications(confidences, boundingBoxes, recognizedSymbols,
                                       cMaxSymbolsDistanceInWordCoef,
                                       cnMaxBatchSizeForWord,
                                       rOutRecognizedString,
                                       cbVerbose, rPossibleError);
}





void RecognitionEngine::extractAllBoundingBoxes(const QImage& rcImageToExtractBoundingBoxes,
                                                const quint16 cnWidthToScale,
                                                const quint16 cnHeightToScale,
                                                const qreal cMinSquareCoef,
                                                QList<QRect>& rOutBoundingBoxes,
                                                QString& rPossibleError)
{
    // Checking image.
    if (rcImageToExtractBoundingBoxes.isNull())
        return;



    // Clusterizing the source image and applying a contrast que.
    QImage imageClusterized;
    QImage imageContrastQue;
    ImagesProcessor::clusterizeImageColorsAndApplyContrastQue(rcImageToExtractBoundingBoxes,
                                                              imageClusterized,
                                                              imageContrastQue);
    if (imageClusterized.isNull() || imageContrastQue.isNull())
    {
        rPossibleError = tr("Could not clusterize provided image - maybe its format is invalid");
        return;
    }



    // The first image to search for symbols - the clusterized and scaled one.
    imageClusterized = imageClusterized.scaled(cnWidthToScale, cnHeightToScale,
                                               Qt::KeepAspectRatio, Qt::SmoothTransformation)
                                       .convertToFormat(QImage::Format_Grayscale8);
    ImagesProcessor::normalizeContrast(imageClusterized);



    // The second one - an image with contrast que (scaled too).
    imageContrastQue = imageContrastQue.scaled(cnWidthToScale, cnHeightToScale,
                                               Qt::KeepAspectRatio, Qt::SmoothTransformation);



    // Detecting all features.
    QList<QRect> boundingBoxesFromClusterized;
    QList<QRect> boundingBoxesFromContrastQue;
    ImagesProcessor::detectFeatures(imageClusterized, boundingBoxesFromClusterized, cMinSquareCoef);
    ImagesProcessor::detectFeatures(imageContrastQue, boundingBoxesFromContrastQue, cMinSquareCoef);

    // Merging all features in a one list.
    rOutBoundingBoxes.clear();
    rOutBoundingBoxes.append(boundingBoxesFromClusterized);
    rOutBoundingBoxes.append(boundingBoxesFromContrastQue);
}

void RecognitionEngine::filterAndSortBoundingBoxes(QList<QRect>& rInOutBoundingBoxes,
                                                   const quint16 cnMinBoxHeight,
                                                   const quint16 cnMinBoxWidth,
                                                   const qreal cTooBroadBoxWidthPart)
{
    // The first round of the boxes filtration - removing too small boxes.
    for (auto i = rInOutBoundingBoxes.size() - 1; i >= 0; --i)
    {
        const QRect& rcBox = rInOutBoundingBoxes.at(i);
        if (rcBox.width() < cnMinBoxWidth || rcBox.height() < cnMinBoxHeight)
        {
            rInOutBoundingBoxes.removeAt(i);
            continue;
        }



        // The second round - dividing broad boxes into narrow ones.
        if (rcBox.width() > rcBox.height()) // if width is bigger then height, dividing width on height to get new boxes count
        {
            if (rcBox.width() < qRound(rcBox.height() * qAbs(cTooBroadBoxWidthPart)))
            {
                const int cnPossibleBoxesCount = qCeil(rcBox.width() / (qreal) rcBox.height());
                const int cnBoxesWidth = rcBox.width() / cnPossibleBoxesCount; // all widths of new boxes are equal
                for (auto nNewBoxIndex = 0; nNewBoxIndex < cnPossibleBoxesCount; ++nNewBoxIndex)
                    rInOutBoundingBoxes.append(QRect(
                            rcBox.x() + nNewBoxIndex * cnBoxesWidth,
                            rcBox.y(),
                            cnBoxesWidth,
                            rcBox.height()));
                if (cnPossibleBoxesCount > 2) // removing the source box if it was too broad
                    rInOutBoundingBoxes.removeAt(i);
            }
            else // if width is too broad - we have encountered a wrong box
                rInOutBoundingBoxes.removeAt(i);
        }
    }



    // The final round - removing possible duplicates.
    for (auto i = rInOutBoundingBoxes.size() - 1; i >= 0; --i)
    {
        const QRect& pcBoxToRemove = rInOutBoundingBoxes.at(i);
        for (int j = 0; j < i; ++j)
        {
            const QRect& rcBoxToCompare = rInOutBoundingBoxes.at(j);
            if (pcBoxToRemove == rcBoxToCompare)
            {
                rInOutBoundingBoxes.removeAt(i);
                break;
            }
        }
    }



    // Sorting all boxes from left to right.
    qSort(rInOutBoundingBoxes.begin(), rInOutBoundingBoxes.end(),
          [] (const QRect& rcA, const QRect& rcB) -> bool
          {
              return rcA.x() < rcB.x();
          });
}

void RecognitionEngine::classifyAndModifyBoundingBoxes(const QImage& rcTargetImage,
                                                       QList<QRect>& rInOutBoundingBoxes,
                                                       const quint8 cnBoxAdditionalCaptureHorizontal,
                                                       const quint8 cnBoxAdditionalCaptureVertical,
                                                       const quint8 cnBoxWidthAddition,
                                                       const quint8 cnBoxHeightAddition,
                                                       QList<qreal>& rOutConfidences,
                                                       QList<QChar>& rOutRecognizedSymbols,
                                                       QString& rPossibleError)
{
    // Checking source variables.
    if (rcTargetImage.isNull())
        return;
    if (rInOutBoundingBoxes.size() == 0)
    {
        rPossibleError = tr("No features were detected to perform the classification");
        return;
    }



    // Preparing variables to extract and recognize sliding windows.
    QRect rectResized;
    QImage imageWindow;
    QChar recognizedSymbol;
    qreal confidence;
    bool bSuccess;



    // Clearing output variables.
    rOutConfidences.clear();
    rOutRecognizedSymbols.clear();



    // Iterating through all filtered bounding boxes.
    for (auto i = 0; i < rInOutBoundingBoxes.size(); ++i)
    {
        const QRect& rcRect = rInOutBoundingBoxes.at(i);

        // We have two fitting cases - brutal for bigger widths and with extending widths for bigger heights.
        if (rcRect.width() > rcRect.height())
        {
            rectResized = QRect(rcRect.x() - cnBoxAdditionalCaptureHorizontal,
                                rcRect.y() - cnBoxAdditionalCaptureVertical,
                                rcRect.width() + cnBoxWidthAddition,
                                rcRect.height() + cnBoxHeightAddition);
            // Extracting window from the image.
            imageWindow = rcTargetImage
                    .copy(rectResized)
                    .scaled(CNN_INPUT_SIDE_SIZE, CNN_INPUT_SIDE_SIZE, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
            rInOutBoundingBoxes.replace(i, rectResized);
        }
        else
        {
            rectResized = QRect(rcRect.x() - (rcRect.height() - rcRect.width()) / 2 - cnBoxAdditionalCaptureHorizontal,
                                rcRect.y() - cnBoxAdditionalCaptureVertical,
                                rcRect.height() + cnBoxWidthAddition,
                                rcRect.height() + cnBoxHeightAddition);
            // Extracting window from the image.
            imageWindow = rcTargetImage
                    .copy(rectResized)
                    .scaled(CNN_INPUT_SIDE_SIZE, CNN_INPUT_SIDE_SIZE, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
            // Updating window bounding box a little bit.
            rectResized = QRect(rcRect.x() - cnBoxAdditionalCaptureHorizontal,
                                rcRect.y() - cnBoxAdditionalCaptureVertical,
                                rcRect.width()  + cnBoxWidthAddition,
                                rcRect.height() + cnBoxHeightAddition);
            rInOutBoundingBoxes.replace(i, rectResized);
        }



        // Getting net's prediction for sliding window and saving result.
        CNNLearningHelper::getSymbolWithConfidence(m_LearnedNet,
                                                   imageWindow,
                                                   m_SymbolsAlphabet, bSuccess, recognizedSymbol, confidence);
        if (!bSuccess)
        {
            rPossibleError = tr("Something went wrong while sliding window classification");
            return;
        }
        rOutConfidences.append(confidence);
        rOutRecognizedSymbols.append(recognizedSymbol);
    }
}

void RecognitionEngine::filterClassifiedBoundingBoxes(QList<qreal>& rInOutConfidences,
                                                      QList<QRect>& rInOutBoundingBoxes,
                                                      QList<QChar>& rInOutRecognizedSymbols,
                                                      const qreal cDuplicatedBoxesThresholdCoef,
                                                      const QList<quint8>& rcDeltasForNMS)
{
    // Applying NMS.
    for (const quint8 cDelta : rcDeltasForNMS)
        MathHelper::NMS(rInOutConfidences, cDelta);

    // Removing filtered values.
    QList<QRect> filteredBoxesNMS;
    for (auto i = rInOutConfidences.size() - 1; i >= 0; --i)
        if (rInOutConfidences.at(i) == 0)
        {
            rInOutConfidences.removeAt(i);
            rInOutRecognizedSymbols.removeAt(i);
        }
        else
            filteredBoxesNMS.insert(0, rInOutBoundingBoxes.at(i));



    // Removing duplicated similar rectangles.
    quint16 nHorizontalThreshold,
            nVerticalThreshold;
    for (auto i = filteredBoxesNMS.size() - 1; i >= 0; --i)
    {
        const QRect& rcSrcRect = filteredBoxesNMS.at(i);
        for (auto j = 0; j < i; ++j)
        {
            const QRect& rcCmpRect = filteredBoxesNMS.at(j);
            nHorizontalThreshold = (quint16) qRound((rcSrcRect.width()  + rcCmpRect.width())  * cDuplicatedBoxesThresholdCoef);
            nVerticalThreshold   = (quint16) qRound((rcSrcRect.height() + rcCmpRect.height()) * cDuplicatedBoxesThresholdCoef);
            if (!(qAbs(rcSrcRect.x() - rcCmpRect.x()) <= nHorizontalThreshold &&
                  qAbs(rcSrcRect.y() - rcCmpRect.y()) <= nVerticalThreshold &&
                  qAbs(rcSrcRect.width() - rcCmpRect.width()) <= nHorizontalThreshold &&
                  qAbs(rcSrcRect.height() - rcCmpRect.height()) <= nVerticalThreshold &&
                  rInOutRecognizedSymbols.at(i) == rInOutRecognizedSymbols.at(j)))
                continue;
            if (rInOutConfidences.at(i) < rInOutConfidences.at(j))
            {
                const QRect cTempRect = rcSrcRect;
                filteredBoxesNMS.replace(i, rcCmpRect);
                filteredBoxesNMS.replace(j, cTempRect);
            }
            filteredBoxesNMS.removeAt(i);
            rInOutConfidences.removeAt(i);
            rInOutRecognizedSymbols.removeAt(i);
            break;
        }
    }

    // Returning filtered boxes.
    rInOutBoundingBoxes.clear();
    rInOutBoundingBoxes = filteredBoxesNMS;
}

void RecognitionEngine::constructStringFromClassifications(const QList<qreal>& rcConfidences,
                                                           const QList<QRect>& rcBoundingBoxes,
                                                           const QList<QChar>& rcRecognizedSymbols,
                                                           const qreal cMaxSymbolsDistanceInWordCoef,
                                                           const quint8 cnMaxBatchSizeForWord,
                                                           QString& rOutRecognizedString,
                                                           const bool cbVerbose,
                                                           QString& rPossibleError)
{
    // Checking source variables.
    if (rcConfidences.size() == 0 || rcBoundingBoxes.size() == 0 || rcRecognizedSymbols.size() == 0)
    {
        rPossibleError = tr("Can not construct a string from empty classifications");
        return;
    }



    // Looking for the best selection of all found windows by searching ones with bigger confidence and more consistent spacings.
    QList<qreal> confidencesMiniBatch; // temp variables
    QList<QRect> boxesMiniBatch;
    QList<QChar> recognizedSymbolsMiniBatch;
    int nLeftBound = 0;
    QString recognitionResult;



    // Evaluating mean distance between symbols to divide the line into words.
    qreal maxSymbolDistanceInWord = 0;
    for (auto i = 0; i < rcBoundingBoxes.size(); ++i)
    {
        const QRect& rcBoundingBox = rcBoundingBoxes.at(i);
        const auto j = i + 1;
        if (j >= rcBoundingBoxes.size())
            break;
        maxSymbolDistanceInWord += BoxesHelper::distanceBetweenBoxesCenters(rcBoundingBox, rcBoundingBoxes.at(j));
    }
    if (rcBoundingBoxes.size() != 0 && rcBoundingBoxes.size() != 1)
        maxSymbolDistanceInWord /= (rcBoundingBoxes.size() - 1);
    maxSymbolDistanceInWord = maxSymbolDistanceInWord * cMaxSymbolsDistanceInWordCoef;



    // Searching for words.
    for (auto nBoxIndex = 0; nBoxIndex < rcBoundingBoxes.size(); ++nBoxIndex)
    {
        // Always adding values to the batch.
        const QRect& rcCurrentBoundingBox = rcBoundingBoxes.at(nBoxIndex);
        confidencesMiniBatch.append(rcConfidences.at(nBoxIndex));
        boxesMiniBatch.append(rcCurrentBoundingBox);
        recognizedSymbolsMiniBatch.append(rcRecognizedSymbols.at(nBoxIndex));

        // Performing word recognition - if we have reached the end of boxes or we have too broad distance between the next symbol or our batch is too big.
        if (nBoxIndex == rcBoundingBoxes.size() - 1 ||
                BoxesHelper::distanceBetweenBoxesBounds(boxesMiniBatch.at(boxesMiniBatch.size() - 1),
                                                        rcBoundingBoxes.at(nBoxIndex + 1)) > maxSymbolDistanceInWord ||
                boxesMiniBatch.size() >= cnMaxBatchSizeForWord)
        {



            // How many combinations do we have in this batch.
            const quint32 cnCombinationsCount = (quint32) qPow(2, confidencesMiniBatch.size());

            // How many threads do we need for that.
            const quint8 cnOptimalThreadsCount = cnCombinationsCount < QThread::idealThreadCount()
                                                 ? (quint8) cnCombinationsCount
                                                 : (quint8) QThread::idealThreadCount();
            quint8 nBatchesCompleted = 0;
            QMutex mutexUpdateBatchesCount;
            QMutex mutexWaitEvaluations;
            QWaitCondition condAllBatchesEvaluated;
            QList<EnergyBatchEvaluator*> evaluators;
            quint32 cnCombinationsPerBatch = cnCombinationsCount / cnOptimalThreadsCount;
            for (auto i = 0; i < cnOptimalThreadsCount; ++i)
            {
                evaluators.append(new EnergyBatchEvaluator(nBatchesCompleted, mutexUpdateBatchesCount, condAllBatchesEvaluated,
                                                           confidencesMiniBatch, boxesMiniBatch, recognizedSymbolsMiniBatch,
                                                           i * cnCombinationsPerBatch,
                                                           (i != cnOptimalThreadsCount - 1)
                                                           ? (i * cnCombinationsPerBatch) + cnCombinationsPerBatch
                                                           : cnCombinationsCount,
                                                           m_pHunspell));
            }
            for (EnergyBatchEvaluator * pEvaluator : evaluators)
                pEvaluator->evaluate();

            // Waiting for evaluations.
            QMutexLocker lock(&mutexWaitEvaluations);
            while (nBatchesCompleted < cnOptimalThreadsCount)
                condAllBatchesEvaluated.wait(&mutexWaitEvaluations);

            // Extracting max energy and combination from all batches.
            qreal maxEnergy = 0;
            quint32 nMaxEnergyCombination = 0;
            QString recognizedWord;
            for (EnergyBatchEvaluator* pEvaluator : evaluators)
            {
                if (pEvaluator->getEvaluatedMaxEnergy() > maxEnergy)
                {
                    maxEnergy = pEvaluator->getEvaluatedMaxEnergy();
                    nMaxEnergyCombination = pEvaluator->getEvaluatedMaxEnergyCombination();
                    recognizedWord = pEvaluator->getPredictedWord();
                }
                pEvaluator->deleteLater();
            }



            // Intermediate result.
            if (cbVerbose)
                qInfo() << tr("Word %1 was recognized with energy %2").arg(recognizedWord).arg(maxEnergy);
            if (recognitionResult != QString())
                recognitionResult.append(QLatin1Char(' '));
            recognitionResult.append(recognizedWord);



            // Resetting batches for the next word recognition.
            nLeftBound += confidencesMiniBatch.size();
            confidencesMiniBatch.clear();
            boxesMiniBatch.clear();
            recognizedSymbolsMiniBatch.clear();
        }
    }



    // Returning recognition result.
    rOutRecognizedString = recognitionResult;
}
