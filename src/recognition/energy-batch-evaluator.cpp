#include "recognition/energy-batch-evaluator.hpp"

#include <cassert>

#include "utility/boxes-helper.hpp"

EnergyBatchEvaluator::EnergyBatchEvaluator(quint8& rnEvaluatedBatchesCount,
                                           QMutex& rUpdateBatchesCountMutex,
                                           QWaitCondition& rCondAllBatchesEvaluated,
                                           const QList<qreal>& rcConfidencesMiniBatch,
                                           const QList<QRect>& rcBoxesMiniBatch,
                                           const QList<QChar>& rcRecognizedCharsMiniBatch,
                                           const quint32 cnStartCombinationInclusive,
                                           const quint32 cnEndCombinationNonInclusive,
                                           Hunspell* pHunspell,
                                           QObject* pParent):
        m_rnEvaluatedBatchesCount(rnEvaluatedBatchesCount),
        m_rUpdateBatchesCountMutex(rUpdateBatchesCountMutex),
        m_rCondAllBatchesEvaluated(rCondAllBatchesEvaluated),
        m_rcConfidencesMiniBatch(rcConfidencesMiniBatch),
        m_rcBoxesMiniBatch(rcBoxesMiniBatch),
        m_rcRecognizedCharsMiniBatch(rcRecognizedCharsMiniBatch),
        m_cnStartCombinationInclusive(cnStartCombinationInclusive),
        m_cnEndCombinationNonInclusive(cnEndCombinationNonInclusive),
        m_pHunspell(pHunspell),
        QThread(pParent)
{}



void EnergyBatchEvaluator::evaluate()
{
    assert(!isRunning());
    QThread::start();
}

void EnergyBatchEvaluator::stopEvaluation()
{
    requestInterruption();
}

qreal EnergyBatchEvaluator::getEvaluatedMaxEnergy() const
{
    return m_EvaluatedMaxEnergy;
}

quint32 EnergyBatchEvaluator::getEvaluatedMaxEnergyCombination() const
{
    return m_nEvaluatedMaxEnergyCombination;
}

QString EnergyBatchEvaluator::getPredictedWord() const
{
    return m_PredictedWord;
}





void EnergyBatchEvaluator::run()
{
    // Resetting values.
    m_EvaluatedMaxEnergy = 0;
    m_nEvaluatedMaxEnergyCombination = 0;
    m_PredictedWord.clear();

    // Preparing variables for energy's evaluation.
    QList<qreal> confidencesToEvalEnergy;
    QList<QRect> boxesToEvalEnergy;
    QString wordToEvalEnergy;



    // Iterating through all provided combinations to eval energies.
    for (quint32 nCombinationIndex = m_cnStartCombinationInclusive;
                 nCombinationIndex < m_cnEndCombinationNonInclusive; ++nCombinationIndex)
    {
        confidencesToEvalEnergy.clear();
        boxesToEvalEnergy.clear();
        wordToEvalEnergy.clear();

        // Loading combination. Ideally we should make an interruption check, but it will slowdown our thread.
        for (auto i = 0; i < m_rcConfidencesMiniBatch.size(); ++i) // don't forget that we have a 32-bit integer (it's max limit here)
            if (nCombinationIndex & (1 << i)) // if we include a window in the batch
            {
                confidencesToEvalEnergy.append(m_rcConfidencesMiniBatch.at(i));
                boxesToEvalEnergy.append(m_rcBoxesMiniBatch.at(i));
                wordToEvalEnergy.append(m_rcRecognizedCharsMiniBatch.at(i));
            }

        // Evaluating energy and updating the max one if needed.
        const qreal cCurrentEnergy = evalEnergyForWindows(confidencesToEvalEnergy, boxesToEvalEnergy, wordToEvalEnergy);
        if (cCurrentEnergy > m_EvaluatedMaxEnergy)
        {
            m_EvaluatedMaxEnergy = cCurrentEnergy;
            m_nEvaluatedMaxEnergyCombination = nCombinationIndex;
            m_PredictedWord = wordToEvalEnergy;
        }
    }



    // Updating evaluated batches count.
    m_rUpdateBatchesCountMutex.lock();
    ++m_rnEvaluatedBatchesCount;
    m_rUpdateBatchesCountMutex.unlock();



    // Waking the main thread being waited for results.
    m_rCondAllBatchesEvaluated.wakeOne();
}





qreal EnergyBatchEvaluator::evalEnergyForWindows(const QList<qreal>& rcConfidences,
                                                 const QList<QRect>& rcWindowsBoxes,
                                                 const QString& rcWord,
                                                 const qreal cAlpha,
                                                 const qreal cBetta,
                                                 const qreal cGamma)
{
    // Evaluating all distances.
    QList<qreal> distances;
    for (auto i = 0; i < rcWindowsBoxes.size(); ++i)
    {
        const int j = i + 1;
        if (j >= rcWindowsBoxes.size())
            break;
        distances.append(BoxesHelper::distanceBetweenBoxesCenters(rcWindowsBoxes.at(i), rcWindowsBoxes.at(j)));
    }

    // All evaluations with distances.
    qreal energyDistances = 0;
    if (distances.size() != 0)
    {
        // Max.
        qreal buffer = 0;
        for (const qreal cDistance : distances)
            if (cDistance > buffer)
                buffer = cDistance;

        // Normalized.
        for (auto i = 0; i < distances.size(); ++i)
            distances.replace(i, distances.at(i) / buffer);

        // Mean.
        buffer = 0;
        for (const qreal cNormalizedDistance : distances)
            buffer += cNormalizedDistance;
        buffer /= distances.size();

        // Appending to the energy.
        for (const qreal cNormalizedDistance : distances)
            energyDistances += (1 - qAbs(buffer - cNormalizedDistance));
        energyDistances /= rcWindowsBoxes.size();
    }



    // Confidence energy.
    qreal energyConfidences = 0;
    for (const qreal cConfidence : rcConfidences)
        energyConfidences += cConfidence;
    energyConfidences /= rcConfidences.size();



    // Word knowledge energy.
    int energyKnowledge = m_pHunspell->spell(rcWord.toLower().toStdString()) ? 1 : 0;



    // Total energy.
    return cAlpha * energyDistances + cBetta * energyConfidences + cGamma * energyKnowledge;
}