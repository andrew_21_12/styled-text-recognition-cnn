#include "utility/math-helper.hpp"

#include <qmath.h>

void MathHelper::softmax(const QList<qreal>& rcSrcValues, QList<qreal>& rOutSoftmaxValues)
{
    // Checking source values.
    if (rcSrcValues.size() == 0)
        return;
    const QList<qreal> cSrcValuesCopy(rcSrcValues);
    rOutSoftmaxValues.clear();



    // Initial evaluations.
    QList<qreal> exponents;
    qreal exponentsSum = 0;
    for (const qreal cSrcValue : cSrcValuesCopy)
    {
        exponents.append(qExp(cSrcValue));
        exponentsSum += exponents.at(exponents.size() - 1);
    }



    // Returning softmax values.
    for (const qreal cExpValue : exponents)
        rOutSoftmaxValues.append(cExpValue / exponentsSum);
}

void MathHelper::NMS(QList<qreal>& rInOutValues, const quint8 cnDelta)
{
    // First wave NMS.
    int nIndex;
    for (auto i = 0; i < rInOutValues.size(); ++i)
        for (auto j = -cnDelta; j <= cnDelta; ++j)
        {
            nIndex = i + j;
            if (nIndex < 0 || nIndex >= rInOutValues.size())
                continue;
            if (rInOutValues.at(i) < rInOutValues.at(nIndex))
            {
                rInOutValues.replace(i, 0);
                break;
            }
        }
}