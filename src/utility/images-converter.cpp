#include "utility/images-converter.hpp"



void ImagesConverter::QImageToGrayDlibMatrix(const QImage &rcSrcImage, dlib::matrix<unsigned char> &rOutMatrix)
{
    // Checking source image.
    if (rcSrcImage.isNull() ||
        rcSrcImage.width() == rcSrcImage.height() == 0)
        return;

    // A source image or its grayscale format.
    const QImage& rcPreparedGrayImage = (rcSrcImage.format() != QImage::Format_Grayscale8)
                                        ? rcSrcImage.convertToFormat(QImage::Format_Grayscale8)
                                        : rcSrcImage;

    // Resizing result mat.
    rOutMatrix.set_size(rcPreparedGrayImage.height(), rcPreparedGrayImage.width());

    // Iterating through all pixels and converting.
    for (auto i = 0; i < rcPreparedGrayImage.height(); ++i)
    {
        const quint8* pcLine = rcPreparedGrayImage.scanLine(i);
        for (auto j = 0; j < rcPreparedGrayImage.width(); ++j)
            rOutMatrix(i, j) = pcLine[j];
    }
}

void ImagesConverter::cvMatToQImage(const cv::Mat& rcSrcMat, QImage& rOutImage, const bool cbInCloneImageData)
{
    // Checking source image.
    if (rcSrcMat.empty() || !rcSrcMat.data)
        return;

    // Converting depending on type.
    switch (rcSrcMat.type())
    {
        case CV_8UC4:
        {
            rOutImage = QImage(
                    rcSrcMat.data,
                    rcSrcMat.cols, rcSrcMat.rows,
                    static_cast<int>(rcSrcMat.step),
                    QImage::Format_ARGB32_Premultiplied);
            if (cbInCloneImageData)
                rOutImage = rOutImage.copy();
            break;
        }
        case CV_8UC3:
        {
            rOutImage = QImage(
                    rcSrcMat.data,
                    rcSrcMat.cols, rcSrcMat.rows,
                    static_cast<int>(rcSrcMat.step),
                    QImage::Format_RGB888).rgbSwapped();
            if (cbInCloneImageData)
                rOutImage = rOutImage.copy();
            break;
        }
        case CV_8UC1:
        {
            static QVector<QRgb> sColorTable(256);
            if (sColorTable.isEmpty())
                for (int i = 0; i < 256; ++i)
                    sColorTable[i] = qRgb(i, i, i);
            rOutImage = QImage(
                    rcSrcMat.data,
                    rcSrcMat.cols, rcSrcMat.rows,
                    static_cast<int>(rcSrcMat.step),
                    QImage::Format_Grayscale8);
            rOutImage.setColorTable(sColorTable);
            if (cbInCloneImageData)
                rOutImage = rOutImage.copy();
            break;
        }
        default: break;
    }
}

void ImagesConverter::QImageToCvMat(const QImage& rcSrcImage, cv::Mat& rOutMat, const bool cbInCloneImageData)
{
    // Checking source image.
    if (rcSrcImage.isNull())
        return;

    // Converting to mat depending on format.
    switch (rcSrcImage.format())
    {
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
        case QImage::Format_ARGB32_Premultiplied:
        {
            cv::Mat mat(
                    rcSrcImage.height(), rcSrcImage.width(),
                    CV_8UC4,
                    const_cast<uchar*>(rcSrcImage.bits()),
                    static_cast<size_t>(rcSrcImage.bytesPerLine()));
            rOutMat = (cbInCloneImageData ? mat.clone() : mat);
            break;
        }
        case QImage::Format_RGB888:
        {
            cv::Mat mat(
                    rcSrcImage.height(), rcSrcImage.width(),
                    CV_8UC3,
                    const_cast<uchar*>(rcSrcImage.bits()),
                    static_cast<size_t>(rcSrcImage.bytesPerLine()));
            cv::cvtColor(mat, rOutMat, CV_BGR2RGB);
            break;
        }
        case QImage::Format_Grayscale8:
        case QImage::Format_Indexed8:
        {
            cv::Mat mat(
                    rcSrcImage.height(), rcSrcImage.width(),
                    CV_8UC1,
                    const_cast<uchar*>(rcSrcImage.bits()),
                    static_cast<size_t>(rcSrcImage.bytesPerLine()));
            rOutMat = (cbInCloneImageData ? mat.clone() : mat);
            break;
        }

        default: break;
    }
}