#include "utility/symbols-encoder.hpp"

#include <QRegExpValidator>

void SymbolsEncoder::encodeDir(const QChar& rcSymbolIn, QString& rDirOut, const QString& rcAlphabet)
{
    // Optional alphabet check.
    if (rcAlphabet != QString() && !rcAlphabet.contains(rcSymbolIn, Qt::CaseSensitive))
        return;

    // Encoding.
    if (rcSymbolIn.isUpper())
        rDirOut = QString::fromLocal8Bit("_%1").arg(rcSymbolIn);
    else
        rDirOut = rcSymbolIn;
}

void SymbolsEncoder::decodeDir(const QString& rcDirIn,
                               QChar& rSymbolOut,
                               bool& rbSuccess,
                               const QString& rcAlphabet,
                               const bool cbConvertNonexistentUpperToLower)
{
    // Checking if we have encountered a cap symbol.
    QRegExp rx(QLatin1String("^[_]{1}[a-zA-Z]{1}$"));
    const QRegExpValidator cValidator(rx);
    int nPos = 0;
    QString nonConstDirIn(rcDirIn);
    QChar charExtracted;

    // A cap symbol was detected.
    if (cValidator.validate(nonConstDirIn, nPos) == QRegExpValidator::Acceptable)
    {
        rx = QRegExp(QLatin1String("[a-zA-Z]{1}"));
        charExtracted = rcDirIn.at(rcDirIn.lastIndexOf(rx));

        // No alphabet check case.
        if (rcAlphabet == QString())
        {
            rSymbolOut = charExtracted;
            rbSuccess = true;
            return;
        }

        // With possible alphabet check.
        if (rcAlphabet.contains(charExtracted, Qt::CaseSensitive))
        {
            rSymbolOut = charExtracted;
            rbSuccess = true;
        }
        else
        {
            // We can also check if we just need a symbol to be presented in the lower case in the alphabet.
            if (!cbConvertNonexistentUpperToLower)
            {
                rbSuccess = false;
                return;
            }
            charExtracted = charExtracted.toLower();
            if (rcAlphabet.contains(charExtracted, Qt::CaseSensitive))
            {
                rSymbolOut = charExtracted;
                rbSuccess = true;
            }
            else
                rbSuccess = false;
        }
    }



    // Not a cap symbol detected.
    else
    {
        if (rcDirIn.size() != 1)
        {
            rbSuccess = false;
            return;
        }

        // Getting char without alphabet.
        charExtracted = rcDirIn.at(0);
        if (rcAlphabet == QString())
        {
            rSymbolOut = charExtracted;
            rbSuccess = true;
            return;
        }

        // With possible alphabet check.
        if (rcAlphabet.contains(charExtracted, Qt::CaseSensitive))
        {
            rSymbolOut = charExtracted;
            rbSuccess = true;
        }
        else
            rbSuccess = false;
    }
}



void SymbolsEncoder::encodeSymbol(const QChar& rcSrcSymbol,
                                  const QString& rcAlphabet,
                                  unsigned long& rOutCode,
                                  bool& rbSuccess)
{
    if (rcAlphabet.contains(rcSrcSymbol))
    {
        rOutCode = (unsigned long) rcAlphabet.indexOf(rcSrcSymbol, 0, Qt::CaseSensitive);
        rbSuccess = true;
    }
    else
        rbSuccess = false;
}

void SymbolsEncoder::decodeSymbol(const unsigned long cnSrcCode,
                                  const QString& rcAlphabet,
                                  QChar& rOutSymbol,
                                  bool& rbSuccess)
{
    if (rcAlphabet.size() <= cnSrcCode)
    {
        rbSuccess = false;
        return;
    }
    rOutSymbol = rcAlphabet.at((int) cnSrcCode);
    rbSuccess = true;
}