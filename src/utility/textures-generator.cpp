#include "utility/textures-generator.hpp"

#include "utility/images-converter.hpp"
#include "utility/randoms-helper.hpp"

TexturesGenerator::TexturesGenerator()
{
    setNonTextTexturesImagesPaths(QList<QString>()
                                          << QLatin1String(":/resources/textures/1.jpg")
                                          << QLatin1String(":/resources/textures/2.jpg")
                                          << QLatin1String(":/resources/textures/3.jpg")
                                          << QLatin1String(":/resources/textures/4.jpg")
                                          << QLatin1String(":/resources/textures/5.jpg")
                                          << QLatin1String(":/resources/textures/6.jpg")
                                          << QLatin1String(":/resources/textures/12.jpg")
                                          << QLatin1String(":/resources/textures/13.jpg")
                                          << QLatin1String(":/resources/textures/14.jpg")
                                          << QLatin1String(":/resources/textures/15.jpg")
                                          << QLatin1String(":/resources/textures/16.jpg")
                                          << QLatin1String(":/resources/textures/17.jpg")
                                          << QLatin1String(":/resources/textures/20.jpg")
                                          << QLatin1String(":/resources/textures/22.jpg")
                                          << QLatin1String(":/resources/textures/23.jpg")
                                          << QLatin1String(":/resources/textures/24.jpg"));
    setTextTexturesImagesPaths(QList<QString>()
                                       << QLatin1String(":/resources/textures/7.jpg")
                                       << QLatin1String(":/resources/textures/8.jpg")
                                       << QLatin1String(":/resources/textures/9.jpg")
                                       << QLatin1String(":/resources/textures/10.jpg")
                                       << QLatin1String(":/resources/textures/11.jpg")
                                       << QLatin1String(":/resources/textures/18.jpg")
                                       << QLatin1String(":/resources/textures/19.jpg")
                                       << QLatin1String(":/resources/textures/21.jpg"));
}

TexturesGenerator& TexturesGenerator::setNonTextTexturesImagesPaths(const QList<QString> &rcNonTextTexturesImagesPaths)
{
    m_NonTextTexturesImagesPaths = rcNonTextTexturesImagesPaths;
    reloadNonTextTextures();
    return *this;
}

TexturesGenerator& TexturesGenerator::setTextTexturesImagesPaths(const QList<QString>& rcTextTexturesImagesPaths)
{
    m_TextTexturesImagesPaths = rcTextTexturesImagesPaths;
    reloadTextTextures();
    return *this;
}



void TexturesGenerator::generateRandomTexture(const quint16 cnMinCropAndOutSize,
                                              QImage& rOutTexture,
                                              bool& rbOutIsLight,
                                              bool& rbSuccessful,
                                              const bool cbNoTextOnly,
                                              const quint8 cnThreshold) const
{
    // Checking if we have texture sources at all.
    if ((cbNoTextOnly && m_LoadedNonTextTextures.size() == 0) ||
       (!cbNoTextOnly && m_LoadedTextTextures.size() == 0 && m_LoadedNonTextTextures.size() == 0))
    {
        rbSuccessful = false;
        return;
    }



    // Extracting random texture source.
    const int cnTextureIndex = RandomsHelper::genRandIntInRange(
            0,
            cbNoTextOnly
            ? m_LoadedNonTextTextures.size() - 1
            : m_LoadedNonTextTextures.size() + m_LoadedTextTextures.size() - 1);
    const QImage& rcTextureImage = cbNoTextOnly
                                   ? m_LoadedNonTextTextures.at(cnTextureIndex)
                                   : (cnTextureIndex < m_LoadedNonTextTextures.size()
                                      ? m_LoadedNonTextTextures.at(cnTextureIndex)
                                      : m_LoadedTextTextures.at(cnTextureIndex - m_LoadedNonTextTextures.size()));
    if (rcTextureImage.isNull())
    {
        rbSuccessful = false;
        return;
    }



    // Preparing a mat for all filtrations for texture.
    cv::Mat matTextureBuffer;
    ImagesConverter::QImageToCvMat(rcTextureImage, matTextureBuffer, true); // deep copy to avoid corrupts of the source



    // Determinating the brightness type.
    rbOutIsLight = RandomsHelper::flipCoin();



    // Getting mean intensity of the texture.
    const cv::Scalar cAvgPixelIntensity = cv::mean(matTextureBuffer);
    const qreal cMeanIntensity = cAvgPixelIntensity[0];

    // Evaluating random brightening bounds.
    const int cBottomBound = qRound(rbOutIsLight ? 255 - cMeanIntensity - cnThreshold : - cMeanIntensity);
    const int  cUpperBound = qRound(rbOutIsLight ? 255 - cMeanIntensity               : - cMeanIntensity + cnThreshold);

    // Applying brightening.
    matTextureBuffer.convertTo(matTextureBuffer, -1, 1, RandomsHelper::genRandIntInRange(cBottomBound, cUpperBound));



    // Cropping randomly prepared texture.
    const int cnCropSize = RandomsHelper::genRandIntInRange(cnMinCropAndOutSize, matTextureBuffer.rows - 1);
    const int cnMaxCoord = matTextureBuffer.rows - cnCropSize - 1;
    const int cnCropX = RandomsHelper::genRandIntInRange(0, cnMaxCoord);
    const int cnCropY = RandomsHelper::genRandIntInRange(0, cnMaxCoord);
    const cv::Rect cCropRect(cnCropX, cnCropY, cnCropSize, cnCropSize);
    matTextureBuffer = matTextureBuffer(cCropRect);



    // Returning prepared texture with resizing to required size.
    ImagesConverter::cvMatToQImage(matTextureBuffer, rOutTexture);
    rOutTexture = rOutTexture.scaled(cnMinCropAndOutSize, cnMinCropAndOutSize,
                                     Qt::KeepAspectRatio, Qt::SmoothTransformation);
    rbSuccessful = true;
}





void TexturesGenerator::reloadNonTextTextures()
{
    // Resetting previous textures.
    m_LoadedNonTextTextures.clear();

    // And loading new ones.
    QImage imageBuffer;
    for (const QString& rcTextureImagePath : m_NonTextTexturesImagesPaths)
    {
        imageBuffer = QImage(rcTextureImagePath);
        if (imageBuffer.isNull())
            continue;
        m_LoadedNonTextTextures.append(imageBuffer.convertToFormat(QImage::Format::Format_Grayscale8));
    }
}

void TexturesGenerator::reloadTextTextures()
{
    // Resetting previous textures.
    m_LoadedTextTextures.clear();

    // And loading new ones.
    QImage imageBuffer;
    for (const QString& rcTextureImagePath : m_TextTexturesImagesPaths)
    {
        imageBuffer = QImage(rcTextureImagePath);
        if (imageBuffer.isNull())
            continue;
        m_LoadedTextTextures.append(imageBuffer.convertToFormat(QImage::Format::Format_Grayscale8));
    }
}
