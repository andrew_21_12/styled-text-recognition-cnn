#include "utility/boxes-helper.hpp"

qreal BoxesHelper::distanceBetweenBoxesCenters(const QRect& rcBox1, const QRect& rcBox2)
{
    return qAbs((rcBox2.x() + rcBox2.width() / 2) - (rcBox1.x() + rcBox1.width() / 2));
}

qreal BoxesHelper::distanceBetweenBoxesBounds(const QRect& rcBoxLeft, const QRect& rcBoxRight)
{
    return rcBoxRight.x() - (rcBoxLeft.x() + rcBoxLeft.width());
}