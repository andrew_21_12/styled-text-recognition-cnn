#include "utility/images-processor.hpp"

#include "utility/images-converter.hpp"
#include "utility/randoms-helper.hpp"

#include <QPainter>
#include <qmath.h>



void ImagesProcessor::wellCenterSymbolImage(QImage& rInOutImage,
                                            const QColor& rcBackColor,
                                            int& rnHorizontalShift,
                                            int& rnVerticalShift)
{
    // Checking image and looking for content's bounds.
    int nTop, nLeft, nBottom, nRight;
    bool bSuccessful;
    checkImageAndGetContentBounds(rInOutImage, rcBackColor, nTop, nLeft, nBottom, nRight, bSuccessful);
    if (!bSuccessful)
        return;

    // Getting image's type and back colors.
    const bool cbUseIndexForColor = // there may be other types too, so be careful
            (rInOutImage.format() == QImage::Format_Grayscale8 || rInOutImage.format() == QImage::Format_Indexed8);
    const auto cBackColor = cbUseIndexForColor ? (quint8) qGray(rcBackColor.rgba()) : rcBackColor.rgba();



    // Evaluating required shifts and directions.
    rnHorizontalShift = (rInOutImage.width() - 1)  / 2 - ((nLeft + nRight) / 2);
    rnVerticalShift   = (rInOutImage.height() - 1) / 2 - ((nTop + nBottom) / 2);
    if (rnHorizontalShift == 0 && rnVerticalShift == 0) // no available shifts
        return;
    const bool cbRightToLeft = rnHorizontalShift < 0;
    const bool cbBottomToTop = rnVerticalShift < 0;

    // Making pixelwise shifting.
    int nTargetX, nTargetY;
    for (int x = cbRightToLeft ? nLeft : nRight; cbRightToLeft ? x <= nRight : x >= nLeft; cbRightToLeft ? ++x : --x)
        for (int y = cbBottomToTop ? nTop : nBottom; cbBottomToTop ? y <= nBottom : y >= nTop; cbBottomToTop ? ++y : --y)
        {
            nTargetX = x + rnHorizontalShift;
            nTargetY = y + rnVerticalShift;
            if (!(nTargetX < 0 || nTargetX >= rInOutImage.width() || nTargetY < 0 || nTargetY >= rInOutImage.height()))
                rInOutImage.setPixel(
                        nTargetX, nTargetY,
                        cbUseIndexForColor ? (quint8) qGray(rInOutImage.pixel(x, y)) : rInOutImage.pixel(x, y));
            rInOutImage.setPixel(x, y, cBackColor);
        }
}

void ImagesProcessor::upscaleSymbolImage(const QImage& rcTargetImage,
                                         const QColor& rcBackColor,
                                         const quint16 cnTargetContentSide,
                                         QImage& rOutResultImage)
{
    // Checking image and looking for content's bounds.
    int nTop, nLeft, nBottom, nRight;
    bool bSuccessful;
    checkImageAndGetContentBounds(rcTargetImage, rcBackColor, nTop, nLeft, nBottom, nRight, bSuccessful);
    if (!bSuccessful)
        return;

    // Evaluating source content width and height.
    const int cnContentWidth = nRight - nLeft + 1;
    const int cnContentHeight = nBottom - nTop + 1;

    // A scale and crop to apply.
    qreal scale = cnTargetContentSide / (qreal) qMax(cnContentWidth, cnContentHeight);
    const int cnNewWidth = qRound(scale * rcTargetImage.width()),
             cnNewHeight = qRound(scale * rcTargetImage.height());
    const QRect cCropRect((cnNewWidth - rcTargetImage.width()) / 2,
                          (cnNewHeight - rcTargetImage.height()) / 2,
                          rcTargetImage.width(), rcTargetImage.height());

    // Scaling, cropping and writing result.
    rOutResultImage = rcTargetImage
            .scaled(cnNewWidth, cnNewHeight, Qt::KeepAspectRatio, Qt::SmoothTransformation)
            .copy(cCropRect);
}

void ImagesProcessor::randomWarp(const QImage& rcTargetImage,
                                 const QColor& rcBackColor,
                                 QImage& rOutResultImage,
                                 const quint16 cnWarpMaxShift)
{
    // Checking source image.
    if (rcTargetImage.isNull())
        return;

    // Points from and to.
    cv::Point2f inputQuad[4];
    cv::Point2f outputQuad[4];
    // Warp matrix.
    cv::Mat lambda(2, 4, CV_32FC1);
    // Input and output images.
    cv::Mat matBuffer;
    // Converting image to OpenCV format with deep copy.
    ImagesConverter::QImageToCvMat(rcTargetImage, matBuffer, true);
    // Checking.
    if (matBuffer.empty())
        return;

    // Setting the lambda matrix to the same type and size as input.
    lambda = cv::Mat::zeros(matBuffer.rows, matBuffer.cols, matBuffer.type());



    // 4 source points to apply the warp from (from top-left in clockwise order).
    inputQuad[0] = cv::Point2f(0, 0);
    inputQuad[1] = cv::Point2f(matBuffer.cols, 0);
    inputQuad[2] = cv::Point2f(matBuffer.cols, matBuffer.rows);
    inputQuad[3] = cv::Point2f(0, matBuffer.rows);

    // Points to warp the source image to (in the same order).
    const int cnNegBound = - cnWarpMaxShift,
              cnPosBound = - cnNegBound;
    outputQuad[0] = cv::Point2f(
            RandomsHelper::genRandIntInRange(cnNegBound, cnPosBound),
            RandomsHelper::genRandIntInRange(cnNegBound, cnPosBound));
    outputQuad[1] = cv::Point2f(
            matBuffer.cols + RandomsHelper::genRandIntInRange(cnNegBound, cnPosBound),
            RandomsHelper::genRandIntInRange(cnNegBound, cnPosBound));
    outputQuad[2] = cv::Point2f(
            matBuffer.cols + RandomsHelper::genRandIntInRange(cnNegBound, cnPosBound),
            matBuffer.rows + RandomsHelper::genRandIntInRange(cnNegBound, cnPosBound));
    outputQuad[3] = cv::Point2f(
            RandomsHelper::genRandIntInRange(cnNegBound, cnPosBound),
            matBuffer.rows + RandomsHelper::genRandIntInRange(cnNegBound, cnPosBound));

    // Getting the perspective transform matrix and applying it.
    lambda = getPerspectiveTransform(inputQuad, outputQuad);
    warpPerspective(matBuffer, matBuffer, lambda, matBuffer.size(), cv::INTER_LINEAR, cv::BORDER_CONSTANT,
                    cv::Scalar(rcBackColor.red(), rcBackColor.green(), rcBackColor.blue(), rcBackColor.alpha()));



    // Returning result.
    ImagesConverter::cvMatToQImage(matBuffer, rOutResultImage, true);
}

void ImagesProcessor::randomShift(const QImage& rcTargetImage,
                                  const QColor& rcBackColor,
                                  QImage& rOutResultImage,
                                  const quint16 cnMaxShift)
{
    // Checking source image.
    if (rcTargetImage.isNull())
        return;

    // Points from and to.
    cv::Point2f inputQuad[4];
    cv::Point2f outputQuad[4];
    // Shift matrix.
    cv::Mat lambda(2, 4, CV_32FC1);
    // Input and output images.
    cv::Mat matBuffer;
    // Converting image to OpenCV format with deep copy.
    ImagesConverter::QImageToCvMat(rcTargetImage, matBuffer, true);
    // Checking.
    if (matBuffer.empty())
        return;

    // Setting the lambda matrix to the same type and size as input.
    lambda = cv::Mat::zeros(matBuffer.rows, matBuffer.cols, matBuffer.type());



    // 4 source points to apply the shift from (from top-left in clockwise order).
    inputQuad[0] = cv::Point2f(0, 0);
    inputQuad[1] = cv::Point2f(matBuffer.cols, 0);
    inputQuad[2] = cv::Point2f(matBuffer.cols, matBuffer.rows);
    inputQuad[3] = cv::Point2f(0, matBuffer.rows);

    // Points to warp the source image to (in the same order).
    const int cnNegBound = - cnMaxShift,
              cnPosBound = - cnNegBound;
    const int cnHorizontalShift = RandomsHelper::genRandIntInRange(cnNegBound, cnPosBound),
              cnVerticalShift = RandomsHelper::genRandIntInRange(cnNegBound, cnPosBound);
    outputQuad[0] = cv::Point2f(cnHorizontalShift, cnVerticalShift);
    outputQuad[1] = cv::Point2f(matBuffer.cols + cnHorizontalShift, cnVerticalShift);
    outputQuad[2] = cv::Point2f(matBuffer.cols + cnHorizontalShift, matBuffer.rows + cnVerticalShift);
    outputQuad[3] = cv::Point2f(cnHorizontalShift, matBuffer.rows + cnVerticalShift);

    // Getting the perspective transform matrix and applying it.
    lambda = getPerspectiveTransform(inputQuad, outputQuad);
    warpPerspective(matBuffer, matBuffer, lambda, matBuffer.size(), cv::INTER_LINEAR, cv::BORDER_CONSTANT,
                    cv::Scalar(rcBackColor.red(), rcBackColor.green(), rcBackColor.blue(), rcBackColor.alpha()));



    // Returning result.
    ImagesConverter::cvMatToQImage(matBuffer, rOutResultImage, true);
}

void ImagesProcessor::resizeToFitCenterWithMeanBackground(const QImage& rcTargetImage,
                                                          const quint16 cnWidth, const quint16 cnHeight,
                                                          QImage& rOutResultImage,
                                                          QColor& rOutMeanColor,
                                                          const quint8 cnHorizontalPadding,
                                                          const quint8 cnVerticalPadding)
{
    // Config.
    const quint8 cnMinImageSize = 9;

    // Checking image.
    if (rcTargetImage.isNull() || rcTargetImage.width() < cnMinImageSize || rcTargetImage.height() < cnMinImageSize)
        return;



    // Preparing mat to perform all ops.
    cv::Mat matBuffer;
    ImagesConverter::QImageToCvMat(rcTargetImage, matBuffer, true); // detaching from the source image

    // Getting mean color and resizing mat.
    const cv::Scalar cMeanColor = cv::mean(matBuffer);
    const qreal cScale = qMin(
            (cnWidth  - cnHorizontalPadding) / (qreal) matBuffer.cols,
            (cnHeight - cnVerticalPadding)   / (qreal) matBuffer.rows);
    const int cnNewWidth  = qRound(matBuffer.cols * cScale);
    const int cnNewHeight = qRound(matBuffer.rows * cScale);
    cv::resize(matBuffer, matBuffer, cv::Size(cnNewWidth, cnNewHeight));



    // Preparing back to fill empty spaces with mean color.
    cv::Mat matBufferBack(cnHeight, cnWidth, matBuffer.type());
    matBufferBack.setTo(cMeanColor);
    matBuffer.copyTo(matBufferBack(
            cv::Rect((matBufferBack.cols - cnNewWidth)  / 2,
                     (matBufferBack.rows - cnNewHeight) / 2,
                     matBuffer.cols,
                     matBuffer.rows)));

    // Returning results.
    ImagesConverter::cvMatToQImage(matBufferBack, rOutResultImage, true);
    switch (matBufferBack.channels())
    {
        case 1:
        {
            rOutMeanColor = QColor((int) cMeanColor[0], (int) cMeanColor[0], (int) cMeanColor[0]);
            break;
        }
        case 3:
        {
            rOutMeanColor = QColor((int) cMeanColor[0], (int) cMeanColor[1], (int) cMeanColor[2]);
            break;
        }
        default: // 4 channels
        {
            rOutMeanColor = QColor((int) cMeanColor[0], (int) cMeanColor[1], (int) cMeanColor[2], (int) cMeanColor[3]);
            break;
        }
    }
}



quint8 ImagesProcessor::generateRandomGrayColorShift(const bool cbIsDark, const quint8 cnThreshold)
{
    // Generating random color.
    return (quint8) RandomsHelper::genRandIntInRange(
            cbIsDark ? 255 - cnThreshold : 0,
            cbIsDark ? 255 : cnThreshold);
}

void ImagesProcessor::appendSymbolOnTextureWithColorizing(const QImage& rcSymbolImage,
                                                          const quint8 cnColorShiftForSymbol,
                                                          const QImage& rcTexture,
                                                          QImage& rOutResultImage)
{
    // Checking sources.
    if (rcSymbolImage.isNull() || rcTexture.isNull() ||
        rcSymbolImage.width() != rcTexture.width() || rcSymbolImage.height() != rcTexture.height())
        return;



    // Getting image's type.
    const bool cbUseIndexForColor = // there may be other types too, so be careful
            (rcSymbolImage.format() == QImage::Format_Grayscale8 || rcSymbolImage.format() == QImage::Format_Indexed8);



    // Colorizing symbol's image.
    rOutResultImage = QImage(rcSymbolImage.size(), rcSymbolImage.format());
    for (auto y = 0; y < rcSymbolImage.height(); ++y)
    {
        const uchar * pcLine = rcSymbolImage.scanLine(y);
        for (auto x = 0; x < rcSymbolImage.width(); ++x)
        {
            if (cbUseIndexForColor)
                rOutResultImage.setPixel(x, y, ((quint8*) pcLine)[x] - cnColorShiftForSymbol);
            else
            {
                const QRgb& crPixel = ((QRgb*) pcLine)[x];
                const QColor cColor(qMax(0, qRed(crPixel)   - cnColorShiftForSymbol),
                                    qMax(0, qGreen(crPixel) - cnColorShiftForSymbol),
                                    qMax(0, qBlue(crPixel)  - cnColorShiftForSymbol),
                                    qAlpha(crPixel));
                rOutResultImage.setPixel(x, y, cColor.rgba());
            }
        }
    }



    // Drawing the symbol over the texture.
    QPainter painter(&rOutResultImage);
    painter.setCompositionMode(QPainter::CompositionMode_DestinationOver);
    painter.drawImage(0, 0, rcTexture);
    painter.end();
}

void ImagesProcessor::invertImageColors(const QImage& rcTargetImage, QImage& rOutResultImage)
{
    // Checking.
    if (rcTargetImage.isNull())
        return;

    // To make all required ops.
    cv::Mat matBuffer;
    ImagesConverter::QImageToCvMat(rcTargetImage, matBuffer, true);

    // Inverting.
    cv::bitwise_not(matBuffer, matBuffer);

    // Returning result.
    ImagesConverter::cvMatToQImage(matBuffer, rOutResultImage, true);
}



void ImagesProcessor::normalizeContrast(QImage& rInOutImage)
{
    // Checking provided mat.
    if (rInOutImage.isNull())
        return;

    // A mat to perform all operations.
    cv::Mat matBuffer;
    ImagesConverter::QImageToCvMat(rInOutImage, matBuffer, false);

    // Converting to grayscale.
    const bool cbNotGrayscale = (matBuffer.type() != CV_8UC1);
    if (cbNotGrayscale)
        cv::cvtColor(matBuffer, matBuffer, CV_BGR2GRAY);

    // Applying histogram equalization.
    cv::equalizeHist(matBuffer, matBuffer);

    // Returning copied image.
    if (cbNotGrayscale)
        ImagesConverter::cvMatToQImage(matBuffer, rInOutImage, true);
}

void ImagesProcessor::applyZCAWhitening(QImage& rInOutImage, const float cEpsilon)
{
    // To perform all ops.
    cv::Mat matBuffer;
    ImagesConverter::QImageToCvMat(rInOutImage, matBuffer, false);

    // Checking format and fixing if needed.
    if (matBuffer.type() != CV_8UC1)
        cv::cvtColor(matBuffer, matBuffer, CV_BGR2GRAY);

    // Converting to SVD-applicable format and scale.
    matBuffer.convertTo(matBuffer, CV_32FC1, 1.0 / 255.0);

    // Evaluating mean color (intensity).
    const cv::Scalar cMean = cv::mean(matBuffer)[0];

    // Preparing SVD evaluator and output mats.
    const cv::SVD cSVD;
    cv::Mat w, u, vt;

    // Evaluating ZCA.
    const cv::Mat cMatZeroCentered = (matBuffer - cMean);
    cSVD.compute(cMatZeroCentered, w, u, vt);
    cv::Mat W = cv::Mat::zeros(w.rows, w.rows, CV_32FC1);
    for (auto i = 0; i < w.rows; ++i)
        W.at<float>(i, i) = (float) (1.0 / (sqrt(w.at<float>(i) + cEpsilon)));

    // Applying ZCA.
    matBuffer = u * W * u.t() * cMatZeroCentered;
    cv::normalize(matBuffer, matBuffer, 0, 1, cv::NORM_MINMAX);
    matBuffer.convertTo(matBuffer, CV_8UC1, 255.0, 0); // converting and scaling back to CV_8UC1

    // Returning processed image.
    ImagesConverter::cvMatToQImage(matBuffer, rInOutImage, true);
}

void ImagesProcessor::blur(QImage& rInOutImage, const quint8 cnPower)
{
    // Checking image.
    if (rInOutImage.isNull())
        return;

    // Applying blur.
    cv::Mat matBuffer;
    ImagesConverter::QImageToCvMat(rInOutImage, matBuffer);
    cv::blur(matBuffer, matBuffer, cv::Size(cnPower, cnPower));

    // Returning image.
    ImagesConverter::cvMatToQImage(matBuffer, rInOutImage, true);
}

void ImagesProcessor::clusterizeImageColorsAndApplyContrastQue(const QImage& rcTargetImage,
                                                               QImage& rOutClusterizedImage,
                                                               QImage& rOutContrastQueImage,
                                                               const bool cbNeedContrastCue,
                                                               const quint8 cnClustersCount)
{
    // Checking image.
    if (rcTargetImage.isNull())
        return;



    // Converting the source image to the LAB color system (with making a deep copy).
    cv::Mat matLab;
    ImagesConverter::QImageToCvMat(rcTargetImage, matLab, false);
    if (!(matLab.channels() == 3 || matLab.channels() == 4)) // LAB color format is not available for non-RGB images
        return;
    cv::cvtColor(matLab, matLab, CV_BGR2Lab);

    // Flatten the source mat.
    cv::Mat matFlat(matLab.rows * matLab.cols, 3, CV_32F);
    int nShiftX;
    for (auto y = 0; y < matLab.rows; ++y)
        for (auto x = 0; x < matLab.cols; ++x)
        {
            nShiftX = x * matLab.rows;
            for (auto z = 0; z < 3; ++z)
                matFlat.at<float>(y + nShiftX, z) = matLab.at<cv::Vec3b>(y, x)[z];
        }

    // Creating mats which will contain clusters labels for pixels and corresponding to them cluster colors.
    cv::Mat matClustersLabels;
    cv::Mat matClustersColors;

    // Applying clusterization.
    cv::kmeans(matFlat, cnClustersCount, matClustersLabels,
               cv::TermCriteria(CV_TERMCRIT_EPS|CV_TERMCRIT_ITER, 10, 0.1),
               5,
               cv::KMEANS_PP_CENTERS, matClustersColors);

    // Preparing the mat for final clusterized image.
    cv::Mat matClustered(matLab.size(), matLab.type());
    for (auto y = 0; y < matLab.rows; ++y)
        for (auto x = 0; x < matLab.cols; ++x)
        {
            const int nClusterLabel = matClustersLabels.at<int>(y + x * matLab.rows, 0);
            matClustered.at<cv::Vec3b>(y,x)[0] = (uchar) matClustersColors.at<float>(nClusterLabel, 0);
            matClustered.at<cv::Vec3b>(y,x)[1] = (uchar) matClustersColors.at<float>(nClusterLabel, 1);
            matClustered.at<cv::Vec3b>(y,x)[2] = (uchar) matClustersColors.at<float>(nClusterLabel, 2);
        }

    // Converting back LAB to the BGR color.
    cv::cvtColor(matClustered, matClustered, CV_Lab2BGR);

    // Returning result image.
    ImagesConverter::cvMatToQImage(matClustered, rOutClusterizedImage);



    // Do we need to apply a contrast que?
    if (!cbNeedContrastCue) return;

    // Applying the contrast que for clustered image.
    QList<quint32> pixelsPerClusters; // an index (position) of the count - is a cluster's label
    for (auto i = 0; i < cnClustersCount; ++i)
        pixelsPerClusters.append((quint32) cv::countNonZero(matClustersLabels == i));

    // Evaluating contrast cues for clusters.
    QList<qreal> contrastQuesForClusters;
    for (auto i = 0; i < cnClustersCount; ++i)
        contrastQuesForClusters.append(0);
    for (auto k = 0; k < cnClustersCount; ++k)
        for (auto i = 0; i < cnClustersCount; ++i)
        {
            if (i == k)
                continue;
            contrastQuesForClusters.replace(k, contrastQuesForClusters.at(k) +
                    (pixelsPerClusters.at(i) / (qreal) matClustersLabels.rows *
                     qSqrt(qPow(matClustersColors.at<float>(k, 0) - matClustersColors.at<float>(i, 0), 2) +
                           qPow(matClustersColors.at<float>(k, 1) - matClustersColors.at<float>(i, 1), 2) +
                           qPow(matClustersColors.at<float>(k, 2) - matClustersColors.at<float>(i, 2), 2))));
        }

    // Constructing result image.
    cv::Mat matContrastQueResult(matLab.size(), CV_8UC1);
    for (auto y = 0; y < matLab.rows; ++y)
        for (auto x = 0; x < matLab.cols; ++x)
        {
            const int cnClusterLabel = matClustersLabels.at<int>(y + x * matLab.rows, 0);
            matContrastQueResult.at<uchar>(y,x) = (uchar) contrastQuesForClusters.at(cnClusterLabel);
        }

    // Returning result image.
    ImagesConverter::cvMatToQImage(matContrastQueResult, rOutContrastQueImage);
}



void ImagesProcessor::detectFeatures(const QImage& rcTargetImage, QList<QRect>& rFeatureBoxes, const qreal cMinSquareCoef)
{
    // Checking image.
    if (rcTargetImage.isNull())
        return;

    // A mat to perform all ops.
    cv::Mat matBuffer;
    ImagesConverter::QImageToCvMat(rcTargetImage, matBuffer, false);
    if (matBuffer.type() != CV_8UC1)
        cv::cvtColor(matBuffer, matBuffer, CV_BGR2GRAY);

    // Preparing vars to evaluate MSER.
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Rect> boundingBoxes;
    const cv::Ptr<cv::MSER> pcMSER = cv::MSER::create(1,
                                                      (int) (cMinSquareCoef * matBuffer.cols * matBuffer.rows),
                                                      (int) (0.1 * matBuffer.cols * matBuffer.rows),
                                                      5,
                                                      2.5);

    // Applying a MSER to detect features.
    pcMSER->detectRegions(matBuffer, contours, boundingBoxes);

    // Returning result bounding boxes.
    rFeatureBoxes.clear();
    for (unsigned long i = 0; i < boundingBoxes.size(); ++i)
    {
        const cv::Rect cBoundingBox = boundingBoxes.at(i);
        rFeatureBoxes.append(QRect(cBoundingBox.x, cBoundingBox.y, cBoundingBox.width, cBoundingBox.height));
    }
}





void ImagesProcessor::checkImageAndGetContentBounds(const QImage& rcTargetImage, const QColor& rcBackColor,
                                                    int& rnTop, int& rnLeft, int& rnBottom, int& rnRight,
                                                    bool& rbSuccess)
{
    // Config.
    const quint8 cnMinImageSize = 3;

    // Checking image.
    rbSuccess = false;
    if (rcTargetImage.isNull() || rcTargetImage.width() < cnMinImageSize || rcTargetImage.height() < cnMinImageSize)
        return;

    // Initial bounds of the drawing.
    rnTop    = rcTargetImage.height() - 1,
    rnLeft   = rcTargetImage.width()  - 1,
    rnBottom = 0,
    rnRight  = 0;

    // There may be other types too, so be careful.
    const bool cbUseIndexForColor = (rcTargetImage.format() == QImage::Format_Indexed8 ||
                                     rcTargetImage.format() == QImage::Format_Grayscale8);

    // Looking for bounds of the drawing.
    bool bIsDrawingFound;
    for (int y = 0; y < rcTargetImage.height(); ++y)
    {
        const uchar* rcLinePixels = rcTargetImage.scanLine(y);
        for (int x = 0; x < rcTargetImage.width(); ++x)
        {
            if (cbUseIndexForColor)
                bIsDrawingFound = ((quint8*) rcLinePixels)[x] != (quint8) qGray(rcBackColor.rgba());
            else // may be some other format excepting RGB ones which will tend to be the source of errors
                bIsDrawingFound = ((QRgb*) rcLinePixels)[x] != rcBackColor.rgba();
            if (bIsDrawingFound)
            {
                rnTop = qMin(rnTop, y);
                rnLeft = qMin(rnLeft, x);
                rnBottom = qMax(rnBottom, y);
                rnRight = qMax(rnRight, x);
            }
        }
    }

    // Bounds were successfully found.
    rbSuccess = true;
}