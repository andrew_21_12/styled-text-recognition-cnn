#include "utility/randoms-helper.hpp"

#include <qmath.h>

int RandomsHelper::genRandIntInRange(const int cnMin, const int cnMax)
{
    const int cnModule = qAbs(cnMax - cnMin) + 1;
    return (qrand() % cnModule) + qMin(cnMin, cnMax);
}

bool RandomsHelper::flipCoin()
{
    return qrand() % 2 == 0;
}

int RandomsHelper::getRandInt(const int cnTopBound)
{
    return qrand() % cnTopBound;
}