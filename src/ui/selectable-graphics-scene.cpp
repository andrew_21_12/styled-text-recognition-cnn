#include "ui/selectable-graphics-scene.hpp"

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QWidget>



// For fast image loaded check.
#define CHECK_LOADED_RETURN if (!isSomethingLoaded()) return;



SelectableGraphicsScene::SelectableGraphicsScene(QObject* pParent):
        QGraphicsScene(pParent),
        m_pGriSelection(Q_NULLPTR),
        m_bIsMousePressed(false)
{
    // Instantiating selection.
    restoreSelection();
}

SelectableGraphicsScene::~SelectableGraphicsScene()
{
    // Releasing selection rectangle from memory.
    delete m_pGriSelection;
}



void SelectableGraphicsScene::safeClear()
{
    // Clearing from all items.
    clear();

    // Clearing saved lists of items.
    m_AddedTexts.clear();
    m_AddedRects.clear();

    // But creating selection again.
    restoreSelection();
}

void SelectableGraphicsScene::selectAgain()
{
    // Simply showing the last active selection.
    m_pGriSelection->show();

    // Firing signal to notify connected objects.
    emit selectionApplied();
}

void SelectableGraphicsScene::deselect()
{
    // Hiding current selection rectangle.
    m_pGriSelection->hide();

    // Firing signal to notify that selection was disabled
    emit selectionDisabled();
}

bool SelectableGraphicsScene::isSelectionActive()
{
    // Selection rectangle's visibility is an indicator of selection's activeness.
    return m_pGriSelection->isVisible();
}

QRect SelectableGraphicsScene::getSelectionRectangle()
{
    // Simply returning coordinates of active (must be checked before) selection's rectangle.
    return QRect((int) m_pGriSelection->rect().x(),
                 (int) m_pGriSelection->rect().y(),
                 (int) m_pGriSelection->rect().width(),
                 (int) m_pGriSelection->rect().height());
}

bool SelectableGraphicsScene::isSomethingLoaded()
{
    // When nothing is loaded, there is no children items.
    return items().length() != 0;
}



void SelectableGraphicsScene::addRecognitionLabel(const QRect& rcBounds, const QString& rcRecognizedString)
{
    // Configs.
    const int cnSelectionStrokeWidth = 2;
    const qreal cBasicFontScale = 1.5;

    // Blue color for recognition.
    QPen pen(Qt::blue);

    // Rounded style.
    pen.setCapStyle(Qt::RoundCap);
    pen.setWidth(qRound(cnSelectionStrokeWidth / getWidthDensityCoef()));

    // Adding rect with label.
    QGraphicsRectItem* pAddedRect = addRect(rcBounds, pen);
    QGraphicsTextItem* pAddedText = new QGraphicsTextItem;
    pAddedText->setPos(rcBounds.x(), rcBounds.y() + rcBounds.height());
    pAddedText->setDefaultTextColor(Qt::blue);
    pAddedText->setScale(cBasicFontScale / getWidthDensityCoef());
    pAddedText->setPlainText(rcRecognizedString.toUpper());
    addItem(pAddedText);

    m_AddedRects.append(pAddedRect);
    m_AddedTexts.append(pAddedText);
}

void SelectableGraphicsScene::removeAllRecognitionLabels()
{
    for (QGraphicsRectItem* pGraphicsRectItem : m_AddedRects)
        removeItem(pGraphicsRectItem);
    for (QGraphicsTextItem* pGraphicsTextItem : m_AddedTexts)
        removeItem(pGraphicsTextItem);
    m_AddedRects.clear();
    m_AddedTexts.clear();
}





void SelectableGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent* pEvent)
{
    m_bIsMousePressed = true;

    // We don't want to select empty scene.
    CHECK_LOADED_RETURN

    // Config.
    const int cnSelectionStrokeWidth = 4;

    // Adding selection layer into the scene, only if it wasn't added yet.
    if (!this->items().contains(m_pGriSelection))
        this->addItem(m_pGriSelection);

    // Setting appropriate brush width depending on image scale.
    QPen pen = m_pGriSelection->pen();
    pen.setWidth(qRound(cnSelectionStrokeWidth / getWidthDensityCoef()));
    m_pGriSelection->setPen(pen);

    // Remembering mouse down position (preventing starting point from out of scene bounds).
    m_PointStart.setX(qRound(qMin(width()  - 1, qMax(0.0, pEvent->scenePos().x()))));
    m_PointStart.setY(qRound(qMin(height() - 1, qMax(0.0, pEvent->scenePos().y()))));

    deselect();
}

void SelectableGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent* pEvent)
{
    if (!m_bIsMousePressed)
        return;

    // We don't want to select empty scene.
    CHECK_LOADED_RETURN



    // Evaluating width and height of the selection.
    int nSelectionWidth  = qRound(pEvent->scenePos().x() - m_PointStart.x());
    int nSelectionHeight = qRound(pEvent->scenePos().y() - m_PointStart.y());

    // Getting current first point of the selection.
    int nSelectionStartX = m_PointStart.x();
    int nSelectionStartY = m_PointStart.y();



    // Changing values' order to prevent evaluation from negative width.
    if (nSelectionWidth < 0) {
        // Preventing left bound from out of scene coordinate.
        nSelectionStartX = qRound(qMax(0.0, pEvent->scenePos().x()));
        nSelectionWidth  = m_PointStart.x() - nSelectionStartX;
    }

    // Changing values' order to prevent evaluation from negative height.
    if (nSelectionHeight < 0) {
        // Preventing upper bound from out of scene coordinate.
        nSelectionStartY = qRound(qMax(0.0, pEvent->scenePos().y()));
        nSelectionHeight = m_PointStart.y() - nSelectionStartY;
    }

    // Preventing right and bottom bound from out of screen coordinates.
    if ((nSelectionStartX + nSelectionWidth) >= width())
        nSelectionWidth = qRound(width() - nSelectionStartX);
    if ((nSelectionStartY + nSelectionHeight) >= height())
        nSelectionHeight = qRound(height() - nSelectionStartY);



    // Setting current selection bounds to draw.
    m_pGriSelection->setRect(nSelectionStartX, nSelectionStartY,
                           nSelectionWidth,  nSelectionHeight);

    if (isSelectionBiggerThanMinimalPossible(nSelectionWidth, nSelectionHeight))
        selectAgain();
    else
        deselect();
}

void SelectableGraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent* pEvent)
{
    m_bIsMousePressed = false;
}





qreal SelectableGraphicsScene::getWidthDensityCoef()
{
    const QObject* pcParentObject = parent();
    const QWidget* pcParentWidget = dynamic_cast<const QWidget*>(pcParentObject);
    if (pcParentWidget != Q_NULLPTR)
        return pcParentWidget->width() / width();
    else
        return 1.0;
}

qreal SelectableGraphicsScene::getHeightDensityCoef()
{
    const QObject* pcParentObject = parent();
    const QWidget* pcParentWidget = dynamic_cast<const QWidget*>(pcParentObject);
    if (pcParentWidget != Q_NULLPTR)
        return pcParentWidget->height() / height();
    else
        return 1.0;
}

bool SelectableGraphicsScene::isSelectionBiggerThanMinimalPossible(const int cnSelectionWidth,
                                                                   const int cnSelectionHeight)
{
    // Config.
    const int cnMinSelectionSizePx = 4;

    // If we have exceeded the bounds limit, returning positive.
    return cnSelectionWidth * getWidthDensityCoef() >= cnMinSelectionSizePx ||
           cnSelectionHeight * getHeightDensityCoef() >= cnMinSelectionSizePx;
}

void SelectableGraphicsScene::restoreSelection()
{
    // Initializing item to draw selection rect.
    m_pGriSelection = new QGraphicsRectItem();

    // Red color for selection.
    QPen pen(Qt::red);

    // Dashed and rounded style.
    pen.setStyle(Qt::DashLine);
    pen.setCapStyle(Qt::RoundCap);

    // Applying style for the selection.
    m_pGriSelection->setPen(pen);

    // Setting selection always on top.
    m_pGriSelection->setZValue(1);

    // Hidden by default - but not emitting signal to turn on / off possible menu actions.
    m_pGriSelection->hide();
}