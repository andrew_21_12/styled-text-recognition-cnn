#include "ui/main-window.hpp"

#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>

#include "borrowed-detector.hpp"

MainWindow::MainWindow(QWidget* pParent, Qt::WindowFlags flags):
        QMainWindow(pParent, flags),
        m_pSelectableGraphicsScene(Q_NULLPTR)
{
    // Basic configs.
    setAttribute(Qt::WA_DeleteOnClose, true);



    // Initializing recognition engine.
    bool bSuccess;
    m_pRecognitionEngine = new RecognitionEngine(bSuccess, this);
    if (!bSuccess)
    {
        qCritical() << tr("Could not intialize a recognition engine");
        close();
        return;
    }
    m_pRecognitionEngine->setSymbolsAlphabet(QLatin1String("0123456789"
                                                           "abcdefghijklmnopqrstuvwzyx"
                                                           "ABDEFGHIJLMNQRTY")); // reduced alphabet - no same symbols



    // Loading and configuring UI.
    setupUi(this);
    setFixedSize(width(), height());
    configureUI();



    // Preparing menu actions.
    configureMenuActions();
    onUpdateMenuActionsAvailability();
}





void MainWindow::onOpenFile()
{
    // A dialog to select an image.
    const QString cSelectedImgPath = QFileDialog::getOpenFileName(this,
                                                                  tr("Choose an image to open"),
                                                                  QDir::currentPath(),
                                                                  tr("Image files (*.png *.jpg)"));

    // Checking path.
    if (cSelectedImgPath.isEmpty())
        return;

    // Loading image.
    const QImage cImage(cSelectedImgPath);

    // Checking image.
    if (cImage.isNull())
    {
        m_pStatusBar->showMessage(tr("Selected image is corrupted"));
        return;
    }



    // Saving image.
    m_LoadedImage.fill(Qt::white);
    m_LoadedImage = cImage;

    // Clearing from previous image.
    m_pSelectableGraphicsScene->safeClear();

    // Loading current image.
    m_pSelectableGraphicsScene->addPixmap(QPixmap::fromImage(cImage));
    m_pSelectableGraphicsScene->setSceneRect(cImage.rect());
    m_pGraphicsView->fitInView(cImage.rect(), Qt::KeepAspectRatio);



    // Updating visibility of menu actions.
    onUpdateMenuActionsAvailability();
}

void MainWindow::onDetectAndRecognize()
{
    // Checking image.
    if (m_LoadedImage.isNull())
    {
        m_pStatusBar->showMessage(tr("No image available to be processed"));
        return;
    }

    // Detecting text regions.
    QList<QRect> foundRegions;
    BorrowedDetector::detectAllTextRegions(m_LoadedImage, foundRegions);



    // Drawing recognitions.
    QString recognizedWord, error;
    m_pSelectableGraphicsScene->removeAllRecognitionLabels();
    for (const QRect& rcSelectionRect : foundRegions)
    {
        m_pRecognitionEngine->recognizeDetectedWordImage(
                m_LoadedImage.copy(rcSelectionRect), recognizedWord, error, true);
        if (error != QString()) continue;
        m_pSelectableGraphicsScene->addRecognitionLabel(rcSelectionRect, recognizedWord);
    }
}

void MainWindow::onRecognizeSelected()
{
    // Checking selection and image.
    if (!m_pSelectableGraphicsScene->isSelectionActive())
    {
        m_pStatusBar->showMessage(tr("Nothing was selected"));
        return;
    }
    if (m_LoadedImage.isNull())
    {
        m_pStatusBar->showMessage(tr("No image available to be processed"));
        return;
    }



    // Getting selection rect.
    const QRect cSelectionRect = m_pSelectableGraphicsScene->getSelectionRectangle();



    // Recognizing word.
    QString recognizedWord, error;
    m_pRecognitionEngine->recognizeDetectedWordImage(m_LoadedImage.copy(cSelectionRect), recognizedWord, error, true);

    // Something went wrong case.
    if (error != QString())
    {
        QMessageBox::critical(this,
                              tr("Could not recognize"),
                              error);
        return;
    }



    // Resetting and adding recognition.
    m_pSelectableGraphicsScene->removeAllRecognitionLabels();
    m_pSelectableGraphicsScene->addRecognitionLabel(cSelectionRect, recognizedWord);
}

void MainWindow::onUpdateMenuActionsAvailability()
{
    m_pActionDetectAndRecognize->setEnabled(m_pSelectableGraphicsScene->isSomethingLoaded());
    m_pActionRecognizeSelected->setEnabled(m_pSelectableGraphicsScene->isSelectionActive());
}





void MainWindow::configureMenuActions()
{
    connect(m_pActionOpen, SIGNAL(triggered()), this, SLOT(onOpenFile()));
    connect(m_pActionExit, SIGNAL(triggered()), this, SLOT(close()));
    connect(m_pActionDetectAndRecognize, SIGNAL(triggered()), this, SLOT(onDetectAndRecognize()));
    connect(m_pActionRecognizeSelected, SIGNAL(triggered()), this, SLOT(onRecognizeSelected()));
    m_pActionOpen->setShortcut(QKeySequence::Open);
    m_pActionExit->setShortcut(QKeySequence::Quit);
    m_pActionDetectAndRecognize->setShortcut(QKeySequence::Find);
    m_pActionRecognizeSelected->setShortcut(QKeySequence::WhatsThis);
}

void MainWindow::configureUI()
{
    // Generating preview container.
    m_pSelectableGraphicsScene = new SelectableGraphicsScene(this);
    m_pGraphicsView->setScene(m_pSelectableGraphicsScene);

    // Connecting selection state with recognize selected action availability.
    connect(m_pSelectableGraphicsScene, SIGNAL(selectionApplied()), this, SLOT(onUpdateMenuActionsAvailability()));
    connect(m_pSelectableGraphicsScene, SIGNAL(selectionDisabled()), this, SLOT(onUpdateMenuActionsAvailability()));
}