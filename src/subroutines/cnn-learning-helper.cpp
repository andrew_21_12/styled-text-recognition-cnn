#include "subroutines/cnn-learning-helper.hpp"

#include <QDebug>

#include "utility/symbols-encoder.hpp"
#include "utility/images-processor.hpp"
#include "utility/images-converter.hpp"
#include "utility/math-helper.hpp"

CNNLearningHelper::CNNLearningHelper(QObject* pParent):
        QObject(pParent),
        m_SymbolsAlphabet(QLatin1String("0123456789"
                                        "abcdefghijklmnopqrstuvwzyx"
                                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ")),
        m_nMaxTrainSamplesPerSymbolFromDB(-1),
        m_DetectionSamplesValidationPart(1.0 / 6.0),
        m_SyncFileName(QLatin1String("net_sync")),
        m_nSecondsIntervalForNetsSync(120),
        m_SerializedLearnedNetFileName(QLatin1String("learned_net.dat"))
{}

CNNLearningHelper&  CNNLearningHelper::setPathsToSamples(const QList<QDir>& rcPathsToSamples)
{
    m_PathsToSamples = rcPathsToSamples;
    return *this;
}

CNNLearningHelper& CNNLearningHelper::setSymbolsAlphabet(const QString& rcSymbolsAlphabet)
{
    m_SymbolsAlphabet = rcSymbolsAlphabet;
    return *this;
}

CNNLearningHelper& CNNLearningHelper::setMaxTrainSamplesPerSymbolFromDB(const int cnMaxTrainSamplesPerSymbolFromDB)
{
    m_nMaxTrainSamplesPerSymbolFromDB = cnMaxTrainSamplesPerSymbolFromDB;
    return *this;
}

CNNLearningHelper& CNNLearningHelper::setDetectionSamplesValidationPart(const qreal cDetectionSamplesValidationPart)
{
    m_DetectionSamplesValidationPart = cDetectionSamplesValidationPart;
    return *this;
}

CNNLearningHelper& CNNLearningHelper::setSyncFileName(const QString& rcSyncFileName)
{
    m_SyncFileName = rcSyncFileName;
    return *this;
}

CNNLearningHelper& CNNLearningHelper::setSecondsIntervalForNetsSync(const qulonglong cnSecondsIntervalForNetsSync)
{
    m_nSecondsIntervalForNetsSync = cnSecondsIntervalForNetsSync;
    return *this;
}

CNNLearningHelper& CNNLearningHelper::setSerializedLearnedNetFileName(const QString& rcSerializedLearnedNetFileName)
{
    m_SerializedLearnedNetFileName = rcSerializedLearnedNetFileName;
    return *this;
}



void CNNLearningHelper::trainAndValidateRecognitionNet(QString& rPossibleError, const bool cbIsVerbose)
{
    // Resetting an error.
    rPossibleError = QString();

    // Preparing variables for samples.
    std::vector<dlib::matrix<unsigned char>> trainImages;
    std::vector<unsigned long>               trainLabels;
    std::vector<dlib::matrix<unsigned char>> testImages;
    std::vector<unsigned long>               testLabels;

    // Making initial checks and loading datasets.
    makeInitialChecksAndLoadSamplesForRecognition(trainImages, trainLabels, testImages, testLabels,
                                                  true,
                                                  cbIsVerbose, rPossibleError);
    if (rPossibleError != QString()) return;



    // Creating network's instance and trainer.
    CNNRecognition net;
    dlib::dnn_trainer<CNNRecognition, dlib::adam> trainer(net, dlib::adam(1e-8, 0.9, 0.999));
    trainer.set_learning_rate(1e-3);
    trainer.set_iterations_without_progress_threshold(2000);
    trainer.set_learning_rate_shrink_factor(0.1);
    if (cbIsVerbose)
        trainer.be_verbose();

    // Enabling synchronization file.
    if (m_SyncFileName != QString())
        trainer.set_synchronization_file(
                m_SyncFileName.toStdString(), std::chrono::seconds(m_nSecondsIntervalForNetsSync));



    // Training the net.
    const auto cnMiniBatchSize = 128;
    std::vector<dlib::matrix<unsigned char>> miniBatchSamples;
    std::vector<unsigned long> miniBatchLabels;
    dlib::rand rnd(time(0));
    while(trainer.get_learning_rate() >= 1e-6)
    {
        miniBatchSamples.clear();
        miniBatchLabels.clear();
        while(miniBatchSamples.size() < cnMiniBatchSize)
        {
            const auto cnRandIndex = rnd.get_random_32bit_number() % trainImages.size();
            miniBatchSamples.push_back(trainImages[cnRandIndex]);
            miniBatchLabels.push_back(trainLabels[cnRandIndex]);
        }
        trainer.train_one_step(miniBatchSamples, miniBatchLabels);
    }



    // Synchronizing network's learning threads, cleaning it from working trash, saving result network to the file.
    trainer.get_net();
    net.clean();
    if (m_SerializedLearnedNetFileName != QString())
        auto _ = dlib::serialize(m_SerializedLearnedNetFileName.toStdString()) << net;



    // Performing quality tests.
    testAndValidateLearnedRecognitionNet(net,
                                         trainImages, trainLabels, testImages, testLabels,
                                         cbIsVerbose);
}

void CNNLearningHelper::trainAndValidateDetectionNet(QString& rPossibleError, const bool cbIsVerbose)
{
    // Resetting an error.
    rPossibleError = QString();

    // Preparing variables for samples.
    std::vector<dlib::matrix<unsigned char>> trainImages;
    std::vector<unsigned long>               trainLabels;
    std::vector<dlib::matrix<unsigned char>> testImages;
    std::vector<unsigned long>               testLabels;

    // Making initial checks and loading datasets.
    makeInitialChecksAndLoadSamplesForRecognition(trainImages, trainLabels, testImages, testLabels,
                                                  false,
                                                  cbIsVerbose, rPossibleError);
    if (rPossibleError != QString()) return;



    // Creating network's instance and trainer.
    CNNDetection net;
    dlib::dnn_trainer<CNNDetection, dlib::adam> trainer(net, dlib::adam(0.0005, 0.9, 0.999));
    trainer.set_learning_rate(1e-3);
    trainer.set_iterations_without_progress_threshold(2000);
    trainer.set_learning_rate_shrink_factor(0.1);
    if (cbIsVerbose)
        trainer.be_verbose();

    // Enabling synchronization file.
    if (m_SyncFileName != QString())
        trainer.set_synchronization_file(
                m_SyncFileName.toStdString(), std::chrono::seconds(m_nSecondsIntervalForNetsSync));



    // Training the net.
    const auto cnMiniBatchSize = 128;
    std::vector<dlib::matrix<unsigned char>> miniBatchSamples;
    std::vector<unsigned long> miniBatchLabels;
    dlib::rand rnd(time(0));
    while(trainer.get_learning_rate() >= 1e-6)
    {
        miniBatchSamples.clear();
        miniBatchLabels.clear();
        while(miniBatchSamples.size() < cnMiniBatchSize)
        {
            const auto cnRandIndex = rnd.get_random_32bit_number() % trainImages.size();
            miniBatchSamples.push_back(trainImages[cnRandIndex]);
            miniBatchLabels.push_back(trainLabels[cnRandIndex]);
        }
        trainer.train_one_step(miniBatchSamples, miniBatchLabels);
    }



    // Synchronizing network's learning threads, cleaning it from working trash, saving result network to the file.
    trainer.get_net();
    net.clean();
    if (m_SerializedLearnedNetFileName.size() != 0)
        auto _ = dlib::serialize(m_SerializedLearnedNetFileName.toStdString()) << net;



    // Performing quality tests.
    testAndValidateLearnedDetectionNet(net, trainImages, trainLabels, testImages, testLabels);
}



void CNNLearningHelper::getSymbolWithConfidence(CNNRecognition& rLearnedNet,
                                                const QImage& rcWindowImage,
                                                const QString& rcAlphabet,
                                                bool& rbSuccess,
                                                QChar& rProposedSymbol,
                                                qreal& rSymbolConfidence)
{
    // Making method return failure status by default.
    rbSuccess = false;

    // Checking provided image.
    if (rcWindowImage.isNull())
        return;

    // Checking provided alphabet.
    if (rcAlphabet.size() < 2)
        return;



    // Preparing vars to load sample in.
    std::vector<dlib::matrix<unsigned char>> samples;
    dlib::matrix<unsigned char> matrix;

    // Detaching an image for pre-processings.
    QImage imageProcessed = rcWindowImage.copy();
    preprocessImage(imageProcessed);

    // Converting image to the dlib format.
    ImagesConverter::QImageToGrayDlibMatrix(imageProcessed, matrix);
    samples.push_back(matrix);



    // Pushing sample to the net.
    std::vector<unsigned long> results = rLearnedNet(samples);

    // Getting pre-output layer with classification confidences.
    const auto cLayerWithProbabilities = dlib::layer<1>(rLearnedNet);
    dlib::resizable_tensor outputProbabilities = cLayerWithProbabilities.get_output();



    // Iterating through all out probabilities for classes.
    unsigned long nIndexOfTheSymbol = 0;
    QChar decodedSymbol;
    bool  bSuccess;
    QMap<QChar, qreal> charsWithProbabilities;
    for (dlib::tensor::iterator it = outputProbabilities.begin(); it != outputProbabilities.end(); ++it)
    {

        // Getting probability for symbol.
        SymbolsEncoder::decodeSymbol(nIndexOfTheSymbol++, rcAlphabet, decodedSymbol, bSuccess);
        if (bSuccess)
            charsWithProbabilities.insert(decodedSymbol, *it);
    }

    // Sorting probabilities of classes.
    QList<qreal> probabilitiesValues = charsWithProbabilities.values();
    qSort(probabilitiesValues.begin(), probabilitiesValues.end());
    // Getting symbol with the max confidence as the recognized one.
    rProposedSymbol = charsWithProbabilities.key(probabilitiesValues.at(probabilitiesValues.size() - 1));
    MathHelper::softmax(probabilitiesValues, probabilitiesValues);



    // Getting the max confidence as the confidence margin between top two classes.
    rSymbolConfidence = probabilitiesValues.at(probabilitiesValues.size() - 1) -
                        probabilitiesValues.at(probabilitiesValues.size() - 2);

    // Everything is ok.
    rbSuccess = true;
}





void CNNLearningHelper::makeInitialChecksAndLoadSamplesForRecognition(
        std::vector<dlib::matrix<unsigned char>>& rOutTrainImages,
        std::vector<unsigned long>& rOutTrainLabels,
        std::vector<dlib::matrix<unsigned char>>& rOutTestImages,
        std::vector<unsigned long>& rOutTestLabels,
        const bool cbRecognitionMode,
        const bool cbIsVerbose,
        QString& rPossibleError)
{
    // Checking alphabet.
    if (cbRecognitionMode && m_SymbolsAlphabet.size() < 2)
    {
        rPossibleError = tr("Provided alphabet is too small, it should have at least 2 symbols");
        return;
    }

    // Checking provided dirs with samples count.
    if (m_PathsToSamples.size() < 2)
    {
        if (cbRecognitionMode)
        {
            if (cbIsVerbose)
                qWarning() << tr("To run the training and validation of the recognition net "
                                 "you should provide at least two dirs: "
                                 "one with training samples and the second with validation samples, - "
                                 "otherwise only training will be performed");
        }
        else
        {
            rPossibleError = tr("To run the training of the detection net "
                                "at least two folders with samples are required: "
                                "all first folders - for samples with symbols on them, "
                                "the one last folder - with just backgrounds without symbols");
            return;
        }
    }

    // Checking if some name for serialized net was provided.
    if (m_SerializedLearnedNetFileName == QString())
    {
        if (cbIsVerbose)
            qWarning() << tr("Please provide some name for learned net serialization file: "
                             "otherwise you will be able only to train net and see its results, "
                             "but you won't get the generated net for further usages");
    }



    // Iterating through provided dirs to load corresponding samples.
    for (auto nDirIndex = 0; nDirIndex < m_PathsToSamples.size(); ++nDirIndex)
    {
        const QDir& rcDirWithSamples = m_PathsToSamples.at(nDirIndex);
        if (!rcDirWithSamples.exists() || !QFileInfo(rcDirWithSamples.absolutePath()).isReadable())
        {
            rPossibleError = tr("The directory with samples %1 does not exist or is not readable")
                    .arg(rcDirWithSamples.absolutePath());
            return;
        }

        // Setting flag with samples type.
        const bool cbIsTrainingOrSymbolsDataset = (nDirIndex != m_PathsToSamples.size() - 1 || m_PathsToSamples.size() == 1);
        const unsigned long cnSampleClassForDetection = cbIsTrainingOrSymbolsDataset ? 1 : 0; // only last folder with backs (0)



        // Temp variables to be required next.
        QChar symbol;
        unsigned long codeSymbol = 0;
        QImage imageSymbol;
        dlib::matrix<unsigned char> matrixSymbol;
        bool bSuccess;



        // Iterating through folders with symbols samples.
        const QStringList cSymbolDirsNames = rcDirWithSamples.entryList(QDir::Filter::Dirs | QDir::NoDotAndDotDot);
        for (const QString& rcSymbolDirName : cSymbolDirsNames)
        {
            // Decoding the dir of the symbol.
            SymbolsEncoder::decodeDir(rcSymbolDirName, symbol, bSuccess, m_SymbolsAlphabet, true);
            if (!bSuccess)
            {
                if (cbIsVerbose)
                    qInfo() << tr("Could not decode symbol name from directory %1").arg(rcSymbolDirName);
                continue;
            }
            // Encoding of the symbol is only required in the recognition mode.
            if (cbRecognitionMode)
            {
                SymbolsEncoder::encodeSymbol(symbol, m_SymbolsAlphabet, codeSymbol, bSuccess);
                if (!bSuccess)
                {
                    if (cbIsVerbose)
                        qInfo() << tr("Could not encode symbol %1").arg(symbol);
                    continue;
                }
            }



            // Preparing a dir with all samples for one symbol.
            const QDir cDirWithSymbols = rcDirWithSamples.filePath(rcSymbolDirName);
            if (!cDirWithSymbols.exists() || !QFileInfo(cDirWithSymbols.absolutePath()).isReadable())
            {
                if (cbIsVerbose)
                    qInfo() << tr("The directory with symbols %1 does not exist or is not readable")
                               .arg(cDirWithSymbols.absolutePath());
                continue;
            }



            // Preparing list of symbols images in the dir and iterating through it.
            qulonglong nLoadedTrainImagesCount = 0; // to keep optional limit for training samples per symbol from DB
            const QStringList cImagesList = cDirWithSymbols.entryList(QDir::Filter::Files | QDir::NoDotAndDotDot);
            for (const QString& rcImageFileName : cImagesList)
            {

                // Only for detection - how many symbols will be in the validation.
                quint64 maxDetectionTrainingSamples = 0;
                if (!cbRecognitionMode)
                    maxDetectionTrainingSamples =
                            (m_DetectionSamplesValidationPart >= 0 && m_DetectionSamplesValidationPart <= 1)
                            ? (quint64) qRound(cImagesList.size() * (1 - m_DetectionSamplesValidationPart))
                            : (quint64) cImagesList.size();

                // Loading and checking symbol's image.
                const QString cFullPathToSymbolImage = cDirWithSymbols.filePath(rcImageFileName);
                imageSymbol = QImage(cFullPathToSymbolImage);
                if (imageSymbol.isNull())
                {
                    if (cbIsVerbose)
                        qInfo() << tr("Could not load sample symbol image %1").arg(cFullPathToSymbolImage);
                    continue;
                }

                // An image was loaded, making required pre-processings and converting to dlib format.
                preprocessImage(imageSymbol);
                ImagesConverter::QImageToGrayDlibMatrix(imageSymbol, matrixSymbol);

                // Saving image and its code. Recognition mode.
                if (cbRecognitionMode)
                {
                    if (cbIsTrainingOrSymbolsDataset) // all first dirs are for training samples
                    {
                        if (nLoadedTrainImagesCount == m_nMaxTrainSamplesPerSymbolFromDB) // more then restriction
                            break;
                        rOutTrainLabels.push_back(codeSymbol);
                        rOutTrainImages.push_back(matrixSymbol);
                        ++nLoadedTrainImagesCount;
                    }
                    else                              // the last dir is for validation samples
                    {
                        rOutTestLabels.push_back(codeSymbol);
                        rOutTestImages.push_back(matrixSymbol);
                    }
                }

                // Detection mode.
                else
                {
                    if (nLoadedTrainImagesCount != m_nMaxTrainSamplesPerSymbolFromDB &&
                            nLoadedTrainImagesCount < maxDetectionTrainingSamples)
                    {
                        rOutTrainLabels.push_back(cnSampleClassForDetection);
                        rOutTrainImages.push_back(matrixSymbol);
                        ++nLoadedTrainImagesCount;
                    }
                    else
                    {
                        rOutTestLabels.push_back(cnSampleClassForDetection);
                        rOutTestImages.push_back(matrixSymbol);
                    }
                }
            }

            // Notifying about status update.
            if (cbIsVerbose)
                qInfo() << tr("Samples for symbol %1 were loaded from %2")
                           .arg(symbol).arg(rcDirWithSamples.dirName());
        }
    }



    // Checking and notifying about loaded datasets.
    if (rOutTrainImages.size() == 0 || rOutTrainLabels.size() == 0)
    {
        rPossibleError = tr("The dataset of train samples is empty (no images or labels)");
        return;
    }
    if ((rOutTestImages.size() == 0 || rOutTestLabels.size() == 0) && cbIsVerbose)
        qWarning() << tr("The dataset of test samples is empty (no images or labels), validation will not be performed");
    if (cbIsVerbose)
    {
        qInfo() << tr("All datasets were successfully loaded: "
                      "train has %1 labels and %2 images, test has %3 labels and %4 images")
                    .arg(rOutTrainLabels.size()).arg(rOutTrainImages.size())
                    .arg(rOutTestLabels.size()).arg(rOutTestImages.size());
    }
}



void CNNLearningHelper::testAndValidateLearnedRecognitionNet(CNNRecognition& rLearnedNet,
                                                             const std::vector<dlib::matrix<unsigned char>>& rcTrainImages,
                                                             const std::vector<unsigned long>& rcTrainLabels,
                                                             const std::vector<dlib::matrix<unsigned char>>& rcTestImages,
                                                             const std::vector<unsigned long>& rcTestLabels,
                                                             const bool cbIsVerbose)
{
    // Evaluating predictions on training samples.
    std::vector<unsigned long> predictedLabels = rLearnedNet(rcTrainImages);
    QChar symbolPredicted, symbolTrue;
    bool bSuccessPredicted, bSuccessTrue;
    qulonglong nNumRight = 0;
    qulonglong nNumWrong = 0;
    for (auto i = 0; i < rcTrainImages.size(); ++i)
    {
        if (predictedLabels[i] == rcTrainLabels[i])
            ++nNumRight;
        else
        {
            if (cbIsVerbose)
            {
                SymbolsEncoder::decodeSymbol(predictedLabels[i], m_SymbolsAlphabet, symbolPredicted, bSuccessPredicted);
                SymbolsEncoder::decodeSymbol(rcTrainLabels[i], m_SymbolsAlphabet, symbolTrue, bSuccessTrue);
                if (bSuccessTrue && bSuccessPredicted)
                    qInfo() << tr("False prediction: %1, true: %2").arg(symbolPredicted).arg(symbolTrue);
            }
            ++nNumWrong;
        }
    }
    qInfo() << tr("Training num right: %1").arg(nNumRight);
    qInfo() << tr("Training num wrong: %1").arg(nNumWrong);
    qInfo() << tr("Training accuracy: %1").arg(nNumRight / (qreal) (nNumRight + nNumWrong));



    // Evaluating predictions on validation samples.
    if (rcTestImages.size() == 0 || rcTestLabels.size() == 0)
        return;
    predictedLabels = rLearnedNet(rcTestImages);
    nNumRight = 0;
    nNumWrong = 0;
    for (auto i = 0; i < rcTestImages.size(); ++i)
    {
        if (predictedLabels[i] == rcTestLabels[i])
            ++nNumRight;
        else
        {
            if (cbIsVerbose)
            {
                SymbolsEncoder::decodeSymbol(predictedLabels[i], m_SymbolsAlphabet, symbolPredicted, bSuccessPredicted);
                SymbolsEncoder::decodeSymbol(rcTestLabels[i], m_SymbolsAlphabet, symbolTrue, bSuccessTrue);
                if (bSuccessTrue && bSuccessPredicted)
                    qInfo() << tr("False prediction: %1, true: %2").arg(symbolPredicted).arg(symbolTrue);
            }
            ++nNumWrong;
        }
    }
    qInfo() << tr("Validation num right: %1").arg(nNumRight);
    qInfo() << tr("Validation num wrong: %1").arg(nNumWrong);
    qInfo() << tr("Validation accuracy: %1").arg(nNumRight / (qreal) (nNumRight + nNumWrong));
}

void CNNLearningHelper::testAndValidateLearnedDetectionNet(CNNDetection& rLearnedNet,
                                        const std::vector<dlib::matrix<unsigned char>>& rcTrainImages,
                                        const std::vector<unsigned long>& rcTrainLabels,
                                        const std::vector<dlib::matrix<unsigned char>>& rcTestImages,
                                        const std::vector<unsigned long>& rcTestLabels)
{
    // Running through training samples.
    std::vector<unsigned long> predictedLabels = rLearnedNet(rcTrainImages);
    qulonglong nNumRight = 0;
    qulonglong nNumWrong = 0;
    for (auto i = 0; i < rcTrainImages.size(); ++i)
    {
        if (predictedLabels[i] == rcTrainLabels[i])
            ++nNumRight;
        else
            ++nNumWrong;
    }
    qInfo() << tr("Training num right: %1").arg(nNumRight);
    qInfo() << tr("Training num wrong: %1").arg(nNumWrong);
    qInfo() << tr("Training accuracy: %1").arg(nNumRight / (qreal) (nNumRight + nNumWrong));



    // Making validation.
    if (rcTestImages.size() == 0 || rcTestLabels.size() == 0)
        return;
    predictedLabels = rLearnedNet(rcTestImages);
    nNumRight = 0;
    nNumWrong = 0;
    for (auto i = 0; i < rcTestImages.size(); ++i)
    {
        if (predictedLabels[i] == rcTestLabels[i])
            ++nNumRight;
        else
            ++nNumWrong;
    }
    qInfo() << tr("Validation num right: %1").arg(nNumRight);
    qInfo() << tr("Validation num wrong: %1").arg(nNumWrong);
    qInfo() << tr("Validation accuracy: %1").arg(nNumRight / (qreal) (nNumRight + nNumWrong));
}



void CNNLearningHelper::preprocessImage(QImage& rInOutImage)
{
    ImagesProcessor::normalizeContrast(rInOutImage);
    ImagesProcessor::applyZCAWhitening(rInOutImage);
    // ImagesProcessor::blur(rInOutImage);
}