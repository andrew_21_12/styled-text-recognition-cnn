#include "subroutines/icdar-helper.hpp"

#include <QColor>
#include <QDomDocument>
#include <QDebug>

#include "utility/symbols-encoder.hpp"
#include "utility/images-converter.hpp"
#include "utility/images-processor.hpp"

ICDARHelper::ICDARHelper(QObject* pParent):
        QObject(pParent),
        m_PreparedSamplesOutFolderName(QLatin1String("icdar03_data")),
        m_SymbolsAlphabet(QLatin1String("0123456789"
                                        "abcdefghijklmnopqrstuvwzyx"
                                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ")),
        m_nPreparedSamplesSize(32)
{}

ICDARHelper& ICDARHelper::setPreparedSamplesOutPath(const QDir& rcPreparedSamplesOutPath)
{
    m_PreparedSamplesOutPath = rcPreparedSamplesOutPath;
    return *this;
}

ICDARHelper& ICDARHelper::setPreparedSamplesOutFolderName(const QString& rcPreparedSamplesOutFolderName)
{
    m_PreparedSamplesOutFolderName = rcPreparedSamplesOutFolderName;
    return *this;
}

ICDARHelper& ICDARHelper::setSymbolsAlphabet(const QString& rcSymbolsAlphabet)
{
    m_SymbolsAlphabet = rcSymbolsAlphabet;
    return *this;
}

ICDARHelper& ICDARHelper::setPreparedSamplesSize(const quint8 cnPreparedSamplesSize)
{
    m_nPreparedSamplesSize = cnPreparedSamplesSize;
    return *this;
}



void ICDARHelper::prepareICDAR03(const QString& rcDatabaseDescFilePath,
                                 QString &rPossibleError,
                                 const bool cbVerbose)
{
    // Resetting an error.
    rPossibleError = QString();

    // Checking desc file path.
    if (rcDatabaseDescFilePath == QString())
    {
        rPossibleError = tr("Please provide a non-empty path to the database description file");
        return;
    }

    // Trying to open provided file for reading.
    QFile dbDescFile(rcDatabaseDescFilePath);
    if (!dbDescFile.open(QIODevice::OpenModeFlag::ReadOnly))
    {
        rPossibleError = tr("Could not open db description file %1").arg(rcDatabaseDescFilePath);
        return;
    }



    // Preparing and checking dirs to output prepared samples.
    QDir samplesOutRootDir;
    checkAndPrepareDirs(dbDescFile, samplesOutRootDir, cbVerbose, rPossibleError);
    if (rPossibleError != QString())
    {
        dbDescFile.close();
        return;
    }



    // Parsing DB description.
    QDomDocument documentDescXML;
    const bool cbSuccess = documentDescXML.setContent(dbDescFile.readAll(), true, &rPossibleError);
    dbDescFile.close();
    if (!cbSuccess) // the error message will already be filled
        return;



    // Iterating through parsed xml description.
    qulonglong nSavedImageIndex = 0;
    const QDir& rcDirWithSourceSamples = QFileInfo(dbDescFile).absoluteDir();
    const QDomElement cElementImagesList = documentDescXML.firstChildElement(QLatin1String("imagelist"));
    if (cElementImagesList.isNull())
    {
        rPossibleError = tr("ICDAR has wrong description file format");
        return;
    }
    const QString cImageNodeName = QLatin1String("image");
    QDomElement elementImage = cElementImagesList.firstChildElement(cImageNodeName);
    if (elementImage.isNull())
    {
        rPossibleError = tr("ICDAR description file has no images elements");
        return;
    }
    while (!elementImage.isNull())
    {

        // Extracting and checking sample's data.
        const QString& rcFilePath = elementImage.attribute(QLatin1String("file"));
        if (rcFilePath == QString())
        {
            if (cbVerbose)
                qInfo() << tr("Node in the description doesn't have a file path");
            elementImage = elementImage.nextSiblingElement(cImageNodeName);
            continue;
        }
        const QString& rcTag = elementImage.attribute(QLatin1String("tag"));
        if (rcTag == QString())
        {
            if (cbVerbose)
                qInfo() << tr("Node in the description doesn't have a tag for file");
            elementImage = elementImage.nextSiblingElement(cImageNodeName);
            continue;
        }

        // Processing sample and going to the next one.
        if (m_SymbolsAlphabet.contains(rcTag, Qt::CaseSensitive))
        {
            processAndSaveSample(rcDirWithSourceSamples, rcFilePath, rcTag,
                                 samplesOutRootDir, nSavedImageIndex, cbVerbose, rPossibleError);
            if (rPossibleError != QString()) return;
        }
        else if (cbVerbose)
            qInfo() << tr("There is no %1 symbol in the alphabet").arg(rcTag);
        elementImage = elementImage.nextSiblingElement(cImageNodeName);
    }

    // Notifying about finish.
    if (cbVerbose)
        qInfo() << tr("All samples of the ICDAR 2003 were prepared");
}





void ICDARHelper::checkAndPrepareDirs(const QFile& rcDbDescFile,
                                      QDir& rPreparedSamplesOutRootDir,
                                      const bool cbVerbose,
                                      QString& rPossibleError)
{
    // Checking and preparing out dir (which will contain a folder with prepared samples).
    QDir dirToCreateRootSamplesFolderIn;
    if (m_PreparedSamplesOutPath == QDir() ||
            !m_PreparedSamplesOutPath.exists() || !QFileInfo(m_PreparedSamplesOutPath.absolutePath()).isWritable())
    {
        if (cbVerbose)
            qWarning() << tr("The directory %1 is not available to write prepared database to, "
                             "trying to place prepared samples folder near (inside) the source samples root folder")
                    .arg(m_PreparedSamplesOutPath.absolutePath());
        if (!rcDbDescFile.exists())
        {
            rPossibleError = tr("No correct dir was provided to place prepared samples in and "
                                "unfortunately it's impossible to place prepared samples near the source ones");
            return;
        }
        dirToCreateRootSamplesFolderIn = QFileInfo(rcDbDescFile).absoluteDir();
    }
    else
        dirToCreateRootSamplesFolderIn = m_PreparedSamplesOutPath;



    // Creating a dir to write prepared samples to.
    if (!QDir(dirToCreateRootSamplesFolderIn.filePath(m_PreparedSamplesOutFolderName)).exists())
    {
        const bool cbDirCreationResult = dirToCreateRootSamplesFolderIn.mkdir(m_PreparedSamplesOutFolderName);
        if (!cbDirCreationResult)
        {
            rPossibleError = tr("Could not create a dir to write prepared samples database to");
            return;
        }
    }
    dirToCreateRootSamplesFolderIn = dirToCreateRootSamplesFolderIn.filePath(m_PreparedSamplesOutFolderName);
    if (!dirToCreateRootSamplesFolderIn.exists() || !QFileInfo(dirToCreateRootSamplesFolderIn.absolutePath()).isWritable())
    {
        rPossibleError = tr("The target directory to write prepared samples to is not available for writing");
        return;
    }

    // Returning prepared dir.
    rPreparedSamplesOutRootDir = dirToCreateRootSamplesFolderIn;
}

void ICDARHelper::processAndSaveSample(const QDir& rcDirWithRawSamples,
                                       const QString& rcRelativeFilename,
                                       const QString& rcSymbolTag,
                                       const QDir& rcDirToWritePreparedSample,
                                       qulonglong& rnIndex,
                                       const bool cbVerbose,
                                       QString& rPossibleError)
{
    // Config.
    const int cnMinSizeOfRawSample = 9;
    const quint16 cnWarpPower = 3;
    const qreal cMaxScaleToApplyBrutallyResizing = 2.25; // if image's scale is more then this value, we won't generate brutally resized samples



    // Preparing a directory to save processed samples.
    if (rcSymbolTag.size() == 0)
    {
        if (cbVerbose)
            qInfo() << tr("There is no tag for symbol with path %1").arg(rcRelativeFilename);
        return;
    }
    QString dirNameToPlaceCurrentSymbolSample;
    SymbolsEncoder::encodeDir(rcSymbolTag.at(0), dirNameToPlaceCurrentSymbolSample, m_SymbolsAlphabet);
    if (dirNameToPlaceCurrentSymbolSample == QString()) return;



    // Preparing a dir for current symbol.
    const QDir cDirToPlaceCurrentSymbolSample = rcDirToWritePreparedSample.filePath(dirNameToPlaceCurrentSymbolSample);
    if (cDirToPlaceCurrentSymbolSample.exists())
    {
        if (!QFileInfo(cDirToPlaceCurrentSymbolSample.absolutePath()).isWritable())
        {
            rPossibleError = tr("The output directory for samples of symbol %1 is not writable").arg(rcSymbolTag);
            return;
        }
    }
    else if (!rcDirToWritePreparedSample.mkdir(dirNameToPlaceCurrentSymbolSample) ||
            !QFileInfo(cDirToPlaceCurrentSymbolSample.absolutePath()).isWritable())
    {
        rPossibleError = tr("Could not create a writable out dir for prepared samples of symbol %1").arg(rcSymbolTag);
        return;
    }



    // Loading and checking raw sample.
    QImage imageRawSample(rcDirWithRawSamples.filePath(rcRelativeFilename));
    if (imageRawSample.isNull() ||
            imageRawSample.width() < cnMinSizeOfRawSample || imageRawSample.height() <= cnMinSizeOfRawSample)
    {
        if (cbVerbose)
            qInfo() << tr("Sample for %1 with relative path %2 is too small or corrupted")
                       .arg(rcSymbolTag).arg(rcRelativeFilename);
        return;
    }

    // Converting to grayscale to make ops faster.
    imageRawSample = imageRawSample.convertToFormat(QImage::Format_Grayscale8);



    // The first way to prepare the sample - just to resize it with losing sides ratio. This is not applicable for all source images.
    const qreal cImageScale = qMax(imageRawSample.width(), imageRawSample.height()) /
                              (qreal) qMin(imageRawSample.width(), imageRawSample.height());
    const bool cbNeedBrutResizing = (cImageScale <= cMaxScaleToApplyBrutallyResizing);
    QImage imagePreparedBrutallyResized;
    if (cbNeedBrutResizing)
        imagePreparedBrutallyResized = imageRawSample.scaled(m_nPreparedSamplesSize, m_nPreparedSamplesSize,
                                                             Qt::IgnoreAspectRatio, Qt::SmoothTransformation)
                                                     .convertToFormat(QImage::Format_Grayscale8);



    // The second way - is to resize with keeping aspect ratio and feeling empty spaces with mean color.
    QImage imagePreparedAccuratelyResized;
    QColor meanQColor;
    ImagesProcessor::resizeToFitCenterWithMeanBackground(imageRawSample,
                                                         m_nPreparedSamplesSize, m_nPreparedSamplesSize,
                                                         imagePreparedAccuratelyResized, meanQColor);



    // Generating additional warp samples for both ways of preparations.
    QImage imagePrepAccWarpSlight, imagePrepAccWarpPower, imagePrepBrutWarpSlight, imagePrepBrutWarpPower;
    ImagesProcessor::randomWarp(imagePreparedAccuratelyResized, meanQColor, imagePrepAccWarpSlight);
    ImagesProcessor::randomWarp(imagePreparedAccuratelyResized, meanQColor, imagePrepAccWarpPower, cnWarpPower);
    if (cbNeedBrutResizing)
    {
        ImagesProcessor::randomWarp(imagePreparedBrutallyResized, meanQColor, imagePrepBrutWarpSlight);
        ImagesProcessor::randomWarp(imagePreparedBrutallyResized, meanQColor, imagePrepBrutWarpPower, cnWarpPower);
    }



    // A function to simplify saving.
    QImage imageInvertedBuffer;
    bool bWriteResult;
    auto funcToSaveSampleOrigAndInverted = [&] (const QImage& rcImageToSave) -> bool
    {
        // Writing prepared sample on disk.
        bWriteResult = rcImageToSave.save(
                cDirToPlaceCurrentSymbolSample.filePath(QString::fromLocal8Bit("%1.png").arg(++rnIndex)));
        if (!bWriteResult)
        {
            rPossibleError = tr("Could not save prepared sample of symbol %1 on disk").arg(rcSymbolTag);
            return bWriteResult;
        }

        // Inverting sample.
        ImagesProcessor::invertImageColors(rcImageToSave, imageInvertedBuffer);

        // Saving inverted sample.
        bWriteResult = imageInvertedBuffer.save(
                cDirToPlaceCurrentSymbolSample.filePath(QString::fromLocal8Bit("%1.png").arg(++rnIndex)));
        if (!bWriteResult)
            rPossibleError = tr("Could not save prepared inverted sample of symbol %1 on disk").arg(rcSymbolTag);
        return bWriteResult;
    };



    // Saving all prepared images.
    if (!funcToSaveSampleOrigAndInverted(imagePreparedAccuratelyResized)) return;
    if (!funcToSaveSampleOrigAndInverted(imagePrepAccWarpSlight)) return;
    if (!funcToSaveSampleOrigAndInverted(imagePrepAccWarpPower)) return;
    if (cbNeedBrutResizing)
    {
        if (!funcToSaveSampleOrigAndInverted(imagePreparedBrutallyResized)) return;
        if (!funcToSaveSampleOrigAndInverted(imagePrepBrutWarpSlight)) return;
        if (!funcToSaveSampleOrigAndInverted(imagePrepBrutWarpPower)) return;
    }
}
