#include "subroutines/samples-generator.hpp"

#include <QDebug>
#include <QPainter>
#include <QFontDatabase>

#include "utility/images-processor.hpp"
#include "utility/symbols-encoder.hpp"
#include "utility/randoms-helper.hpp"

SamplesGenerator::SamplesGenerator(QObject* pParent):
        QObject(pParent),
        m_SamplesOutFolderPath(QDir::homePath()),
        m_SamplesOutFolderName(QLatin1String("train_data")),
        m_nSizeOfSamplesSide(32),
        m_nWidthOfOutputSamples(32),
        m_bOnlyBacks(false),
        m_bNoTextualTextures(false),
        m_bFixedBackBlackColor(false),
        m_FontsPaths(QList<QString>()
                             << QLatin1String(":/resources/fonts/Akkurat-Mono.ttf")
                             << QLatin1String(":/resources/fonts/DejaVuMathTeXGyre.ttf")
                             << QLatin1String(":/resources/fonts/DejaVuSans-ExtraLight.ttf")
                             << QLatin1String(":/resources/fonts/DejaVuSansMono.ttf")
                             << QLatin1String(":/resources/fonts/DejaVuSerif-Bold.ttf")
                             << QLatin1String(":/resources/fonts/DejaVuSerif.ttf")
                             << QLatin1String(":/resources/fonts/DejaVuSerifCondensed-Bold.ttf")
                             << QLatin1String(":/resources/fonts/Merchant Copy Doublesize.ttf")
                             << QLatin1String(":/resources/fonts/Merchant Copy Wide.ttf")
                             << QLatin1String(":/resources/fonts/Merchant Copy.ttf")
                             << QLatin1String(":/resources/fonts/monaco.ttf")
                             << QLatin1String(":/resources/fonts/Telidon Ink Regular.ttf")
                             << QLatin1String(":/resources/fonts/B52.ttf")
                             << QLatin1String(":/resources/fonts/BadScript-Regular.ttf")
                             << QLatin1String(":/resources/fonts/Beast vs SpreadTall.ttf")
                             << QLatin1String(":/resources/fonts/Bird cherry lite.ttf")
                             << QLatin1String(":/resources/fonts/Bird cherry.ttf")
                             << QLatin1String(":/resources/fonts/ClearSans-Bold.ttf")
                             << QLatin1String(":/resources/fonts/ClearSans-BoldItalic.ttf")
                             << QLatin1String(":/resources/fonts/ClearSans-Italic.ttf")
                             << QLatin1String(":/resources/fonts/ClearSans-Light.ttf")
                             << QLatin1String(":/resources/fonts/ClearSans-Regular.ttf")
                             << QLatin1String(":/resources/fonts/ClearSans-Thin.ttf")
                             << QLatin1String(":/resources/fonts/Epson1.TTF")
                             << QLatin1String(":/resources/fonts/Exo2-Black.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-BlackItalic.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-Bold.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-BoldItalic.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-ExtraBold.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-ExtraBoldItalic.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-ExtraLight.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-ExtraLightItalic.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-Italic.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-Light.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-LightItalic.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-Medium.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-MediumItalic.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-Regular.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-SemiBold.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-SemiBoldItalic.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-Thin.ttf")
                             << QLatin1String(":/resources/fonts/Exo2-ThinItalic.ttf")
                             << QLatin1String(":/resources/fonts/fontinsans_cyrillic_b_46b.ttf")
                             << QLatin1String(":/resources/fonts/fontinsans_cyrillic_bi_46b.ttf")
                             << QLatin1String(":/resources/fonts/fontinsans_cyrillic_i_46b.ttf")
                             << QLatin1String(":/resources/fonts/fontinsans_cyrillic_r_46b.ttf")
                             << QLatin1String(":/resources/fonts/Hangyaboly.ttf")
                             << QLatin1String(":/resources/fonts/IkraSlab.ttf")
                             << QLatin1String(":/resources/fonts/inglobal.ttf")
                             << QLatin1String(":/resources/fonts/inglobalb.ttf")
                             << QLatin1String(":/resources/fonts/inglobalbi.ttf")
                             << QLatin1String(":/resources/fonts/inglobali.ttf")
                             << QLatin1String(":/resources/fonts/isocpeui.ttf")
                             << QLatin1String(":/resources/fonts/isocpeur.ttf")
                             << QLatin1String(":/resources/fonts/isocteui.ttf")
                             << QLatin1String(":/resources/fonts/isocteur.ttf")
                             << QLatin1String(":/resources/fonts/Kurale-Regular.ttf")
                             << QLatin1String(":/resources/fonts/lobster.ttf")
                             << QLatin1String(":/resources/fonts/Lombardina Two.ttf")
                             << QLatin1String(":/resources/fonts/Lora-Bold.ttf")
                             << QLatin1String(":/resources/fonts/Lora-BoldItalic.ttf")
                             << QLatin1String(":/resources/fonts/Lora-Italic.ttf")
                             << QLatin1String(":/resources/fonts/Lora-Regular.ttf")
                             << QLatin1String(":/resources/fonts/MarckScript-Regular.ttf")
                             << QLatin1String(":/resources/fonts/merriweather-bold.ttf")
                             << QLatin1String(":/resources/fonts/merriweather-boldit.ttf")
                             << QLatin1String(":/resources/fonts/merriweather-italic.ttf")
                             << QLatin1String(":/resources/fonts/merriweather-light.ttf")
                             << QLatin1String(":/resources/fonts/merriweather-lightit.ttf")
                             << QLatin1String(":/resources/fonts/merriweather-regular.ttf")
                             << QLatin1String(":/resources/fonts/merriweather-ultrabdit.ttf")
                             << QLatin1String(":/resources/fonts/merriweather-ultrabold.ttf")
                             << QLatin1String(":/resources/fonts/mipgost.ttf")
                             << QLatin1String(":/resources/fonts/mirvoshar_font_by_dab-d31q5nk.ttf")
                             << QLatin1String(":/resources/fonts/Neucha.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-Bold.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-BoldItalic.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-CondBold.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-CondLight.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-CondLightItalic.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-ExtraBold.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-ExtraBoldItalic.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-Italic.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-Light.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-LightItalic.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-Regular.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-Semibold.ttf")
                             << QLatin1String(":/resources/fonts/OpenSans-SemiboldItalic.ttf")
                             << QLatin1String(":/resources/fonts/PFScandalPro-Black.ttf")
                             << QLatin1String(":/resources/fonts/PressStart2P-Regular.ttf")
                             << QLatin1String(":/resources/fonts/PTF75F.ttf")
                             << QLatin1String(":/resources/fonts/PTF76F.ttf")
                             << QLatin1String(":/resources/fonts/PTM55F.ttf")
                             << QLatin1String(":/resources/fonts/PTM75F.ttf")
                             << QLatin1String(":/resources/fonts/raleway-black.ttf")
                             << QLatin1String(":/resources/fonts/raleway-bold.ttf")
                             << QLatin1String(":/resources/fonts/raleway-extrabold.ttf")
                             << QLatin1String(":/resources/fonts/raleway-extralight.ttf")
                             << QLatin1String(":/resources/fonts/raleway-light.ttf")
                             << QLatin1String(":/resources/fonts/raleway-medium.ttf")
                             << QLatin1String(":/resources/fonts/raleway-regular.ttf")
                             << QLatin1String(":/resources/fonts/raleway-semibold.ttf")
                             << QLatin1String(":/resources/fonts/raleway-thin.ttf")
                             << QLatin1String(":/resources/fonts/Roboto-Black.ttf")
                             << QLatin1String(":/resources/fonts/Roboto-BlackItalic.ttf")
                             << QLatin1String(":/resources/fonts/Roboto-Bold.ttf")
                             << QLatin1String(":/resources/fonts/Roboto-BoldItalic.ttf")
                             << QLatin1String(":/resources/fonts/Roboto-Italic.ttf")
                             << QLatin1String(":/resources/fonts/Roboto-Light.ttf")
                             << QLatin1String(":/resources/fonts/Roboto-LightItalic.ttf")
                             << QLatin1String(":/resources/fonts/Roboto-Medium.ttf")
                             << QLatin1String(":/resources/fonts/Roboto-MediumItalic.ttf")
                             << QLatin1String(":/resources/fonts/Roboto-Regular.ttf")
                             << QLatin1String(":/resources/fonts/Roboto-Thin.ttf")
                             << QLatin1String(":/resources/fonts/Roboto-ThinItalic.ttf")
                             << QLatin1String(":/resources/fonts/RobotoSlab-Bold.ttf")
                             << QLatin1String(":/resources/fonts/RobotoSlab-Light.ttf")
                             << QLatin1String(":/resources/fonts/RobotoSlab-Regular.ttf")
                             << QLatin1String(":/resources/fonts/RobotoSlab-Thin.ttf")
                             << QLatin1String(":/resources/fonts/ubuntu-b.ttf")
                             << QLatin1String(":/resources/fonts/ubuntu-bi.ttf")
                             << QLatin1String(":/resources/fonts/ubuntu-c.ttf")
                             << QLatin1String(":/resources/fonts/ubuntu-l.ttf")
                             << QLatin1String(":/resources/fonts/ubuntu-li.ttf")
                             << QLatin1String(":/resources/fonts/ubuntu-m.ttf")
                             << QLatin1String(":/resources/fonts/ubuntu-mi.ttf")
                             << QLatin1String(":/resources/fonts/ubuntu-r.ttf")
                             << QLatin1String(":/resources/fonts/ubuntu-ri.ttf")
                             << QLatin1String(":/resources/fonts/ubuntumono-b.ttf")
                             << QLatin1String(":/resources/fonts/ubuntumono-bi.ttf")
                             << QLatin1String(":/resources/fonts/ubuntumono-r.ttf")
                             << QLatin1String(":/resources/fonts/ubuntumono-ri.ttf")),
        m_bUseOnlyFontsFromPaths(false),
        m_FontsExceptions(QList<QString>()
                                  << QLatin1String("Adobe Kaiti Std")          << QLatin1String("Adobe Ming Std")
                                  << QLatin1String("Al Tarikh")                << QLatin1String("Andale Mono")
                                  << QLatin1String("Apple Chancery")           << QLatin1String("Apple Symbols")
                                  << QLatin1String("Arial Unicode MS")         << QLatin1String("Avenir Next")
                                  << QLatin1String("Big Caslon")               << QLatin1String("Brush Script Std")
                                  << QLatin1String("Candara")                  << QLatin1String("Chalkboard SE")
                                  << QLatin1String("Chalkduster")              << QLatin1String("Chaparral Pro")
                                  << QLatin1String("Charlemagne Std")          << QLatin1String("Charter")
                                  << QLatin1String("Constantia")               << QLatin1String("Copperplate")
                                  << QLatin1String("Copperplate Gothic Bold")  << QLatin1String("GB18030 Bitmap")
                                  << QLatin1String("Bangla MN")                << QLatin1String("Blackoak Std")
                                  << QLatin1String("Bodoni Ornaments")         << QLatin1String("Braggadocio")
                                  << QLatin1String("Khmer MN")                 << QLatin1String("Khmer Sangam MN")
                                  << QLatin1String("Lucida Blackletter")       << QLatin1String("Rosewood Std")
                                  << QLatin1String("STIXIntegralsD")           << QLatin1String("STIXIntegralsUpD")
                                  << QLatin1String("STIXSizeFourSym")          << QLatin1String("STIXSizeThreeSym")
                                  << QLatin1String("STIXSizeTwoSym")           << QLatin1String("Wingdings")
                                  << QLatin1String("Wingdings 2")              << QLatin1String("Wingdings 3")
                                  << QLatin1String("Zapfino")                  << QLatin1String("Cooper Std")
                                  << QLatin1String("Copperplate Gothic Light") << QLatin1String("Desdemona")
                                  << QLatin1String("Lithos Pro")               << QLatin1String("Mesquite Std")
                                  << QLatin1String("Orator Std")               << QLatin1String("Perpetua Titling MT")
                                  << QLatin1String("Phosphate")                << QLatin1String("Stencil")
                                  << QLatin1String("Stencil Std")              << QLatin1String("STIXSizeOneSym")
                                  << QLatin1String("TeamViewer12")             << QLatin1String("Trajan Pro")
                                  << QLatin1String("Trajan Pro 3")             << QLatin1String("Bodoni 72 Smallcaps")
                                  << QLatin1String("Engravers MT")             << QLatin1String("Haettenschweiler")
                                  << QLatin1String("Poplar Std")               << QLatin1String("Apple Color Emoji")
                                  << QLatin1String("Herculanum")),
        m_FontSizes(QList<quint8>() << 32),
        m_bWithNeighbourSymbols(true),
        m_SpaceBetweenNeighbours(QLatin1String("   ")),
        m_bIsWellCentered(true),
        m_nAdditionalWarpedSamplesCount(5),
        m_SymbolsAlphabet(QLatin1String("0123456789"
                                        "abcdefghijklmnopqrstuvwzyx"
                                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"))
{}

SamplesGenerator& SamplesGenerator::setSamplesOutFolderPath(const QDir& rcSamplesOutFolderPath)
{
    m_SamplesOutFolderPath = rcSamplesOutFolderPath;
    return *this;
}

SamplesGenerator& SamplesGenerator::setSamplesOutFolderName(const QString& rcSamplesOutFolderName)
{
    m_SamplesOutFolderName = rcSamplesOutFolderName;
    return *this;
}

SamplesGenerator& SamplesGenerator::setSizeOfSamplesSide(const quint8 cnSizeOfSamplesSide)
{
    m_nSizeOfSamplesSide = cnSizeOfSamplesSide;
    return *this;
}

SamplesGenerator& SamplesGenerator::setWidthOfOutputSamples(const quint8 cnWidthOfOutputSamples)
{
    m_nWidthOfOutputSamples = cnWidthOfOutputSamples;
    return *this;
}

SamplesGenerator& SamplesGenerator::setOnlyBacks(const bool cbOnlyBacks)
{
    m_bOnlyBacks = cbOnlyBacks;
    return *this;
}

SamplesGenerator& SamplesGenerator::setNoTextualTextures(const bool cbNoTextualTextures)
{
    m_bNoTextualTextures = cbNoTextualTextures;
    return *this;
}

SamplesGenerator& SamplesGenerator::setFixedBackBlackColor(const bool cbFixedBackBlackColor)
{
    m_bFixedBackBlackColor = cbFixedBackBlackColor;
    return *this;
}

SamplesGenerator& SamplesGenerator::setFontsPaths(const QList<QString>& rcFontsPaths)
{
    m_FontsPaths = rcFontsPaths;
    return *this;;
}

SamplesGenerator& SamplesGenerator::setUseOnlyFontsFromPaths(const bool cbUseOnlyFontsFromPaths)
{
    m_bUseOnlyFontsFromPaths = cbUseOnlyFontsFromPaths;
    return *this;
}

SamplesGenerator& SamplesGenerator::setFontsExceptions(const QList<QString>& rcFontsExceptions)
{
    m_FontsExceptions = rcFontsExceptions;
    return *this;
}

SamplesGenerator& SamplesGenerator::setFontsSizes(const QList<quint8>& rcFontSizes)
{
    m_FontSizes = rcFontSizes;
    return *this;
}

SamplesGenerator& SamplesGenerator::setWithNeighbourSymbols(const bool cbWithNeighbourSymbols)
{
    m_bWithNeighbourSymbols = cbWithNeighbourSymbols;
    return *this;
}

SamplesGenerator& SamplesGenerator::setSpaceBetweenNeighbours(const QString& rcSpaceBetweenNeighbours)
{
    m_SpaceBetweenNeighbours = rcSpaceBetweenNeighbours;
    return *this;
}

SamplesGenerator& SamplesGenerator::setWellCentered(const bool cbIsWellCentered)
{
    m_bIsWellCentered = cbIsWellCentered;
    return *this;
}

SamplesGenerator& SamplesGenerator::setUpscalingSizes(const QList<quint8>& rcUpscalingSizes)
{
    m_UpscalingSizes = rcUpscalingSizes;
    return *this;
}

SamplesGenerator& SamplesGenerator::setAdditionalWarpedSamplesCount(const quint8 cnAdditionalWarpedSamplesCount)
{
    m_nAdditionalWarpedSamplesCount = cnAdditionalWarpedSamplesCount;
    return *this;
}

SamplesGenerator& SamplesGenerator::setAdditionalShiftedSamplesCount(const quint8 cnAdditionalShiftedSamplesCount)
{
    m_nAdditionalShiftedSamplesCount = cnAdditionalShiftedSamplesCount;
    return *this;
}

SamplesGenerator& SamplesGenerator::setSymbolsAlphabet(const QString& rcSymbolsAlphabet)
{
    m_SymbolsAlphabet = rcSymbolsAlphabet;
    return *this;
}



void SamplesGenerator::generate(QString& rPossibleCriticalError, const bool cbVerbose)
{
    // Checking path to create samples in.
    rPossibleCriticalError = QString();
    checkSamplesOutFolder(rPossibleCriticalError);
    if (rPossibleCriticalError != QString())
        return;

    // Checking samples' sizes.
    checkSizeOfSamplesSide(rPossibleCriticalError);
    if (rPossibleCriticalError != QString())
        return;

    // Loading provided fonts.
    QStringList fontFamilies;
    for (const QString& rcFontPath : m_FontsPaths)
    {
        const int cnFontLoadingResult = QFontDatabase::addApplicationFont(rcFontPath);
        if (cnFontLoadingResult == -1)
        {
            if (cbVerbose)
                qInfo() << tr("Could not load provided font with path %1").arg(rcFontPath);
        }
        else if (m_bUseOnlyFontsFromPaths)
            fontFamilies.append(QFontDatabase::applicationFontFamilies(cnFontLoadingResult));
    }

    // Preparing and checking fonts variations.
    const QFontDatabase cFontDatabase;
    if (!m_bUseOnlyFontsFromPaths)
        fontFamilies.append(cFontDatabase.families());
    for (const QString& rcFontFamilyExcluded : m_FontsExceptions) // excluding exceptions
        fontFamilies.removeAll(rcFontFamilyExcluded);
    if (fontFamilies.size() == 0)
    {
        rPossibleCriticalError = tr("There is no available fonts to generate samples: "
                                    "please make sure that at least one font is available for this generator");
        return;
    }

    // Checking font sizes availability.
    if (m_FontSizes.size() == 0)
    {
        rPossibleCriticalError = tr("No font sizes were provided to generate samples: "
                                    "please provide at least one font size");
        return;
    }

    // Creating and checking the dir to write samples in.
    QDir fullDirToCreateSamples;
    createAndCheckSamplesOutDir(fullDirToCreateSamples, rPossibleCriticalError);
    if (rPossibleCriticalError != QString())
        return;
    if (cbVerbose)
        qInfo() << tr("Samples dir was successfully created in %1").arg(fullDirToCreateSamples.absolutePath());



    // Creating samples for all particular symbols of the alphabet.
    QString symbolDirName;
    // Increasing the size of image buffer to create samples to avoid quality losses.
    QImage imageBuffer(m_nSizeOfSamplesSide * 2, m_nSizeOfSamplesSide * 2, QImage::Format::Format_ARGB32_Premultiplied);
    for (const QChar& rcSymbol : m_SymbolsAlphabet)
    {
        symbolDirName = QString();
        SymbolsEncoder::encodeDir(rcSymbol, symbolDirName);
        if (symbolDirName == QString())
        {
            if (cbVerbose)
                qInfo() << tr("Could not generate a folder's name for symbol %1").arg(rcSymbol);
            continue;
        }

        // Creating all samples for current symbol from the alphabet.
        createSamplesForSymbol(rcSymbol, symbolDirName, fullDirToCreateSamples,
                               fontFamilies,
                               imageBuffer, rPossibleCriticalError);
        if (rPossibleCriticalError != QString())
            return;
        if (cbVerbose)
            qInfo() << tr("Samples were generated for symbol %1").arg(rcSymbol);
    }

    // Notifying about success.
    if (cbVerbose)
        qInfo() << tr("All samples were successfully generated");
}





void SamplesGenerator::checkSamplesOutFolder(QString& rPossibleError)
{
    const QFileInfo cFileInfo(m_SamplesOutFolderPath.absolutePath());
    if (!cFileInfo.exists() || !cFileInfo.isDir())
        rPossibleError = tr("The directory to create samples does not exist or is not a directory");
    else if (!cFileInfo.isWritable())
        rPossibleError = tr("The directory to create is not available for writing");
}

void SamplesGenerator::createAndCheckSamplesOutDir(QDir& rCreatedSamplesOutDir, QString& rPossibleError)
{
    // Preparing a directory to create new samples.
    QString targetSamplesFolderName = m_SamplesOutFolderName;
    QFileInfo fiOfFullDirToCreateSamples;
    qulonglong nSamplesFolderIndex = 1;
    forever
    {
        // Successful creation of the samples' dir or critical error with folder's creation.
        rCreatedSamplesOutDir = m_SamplesOutFolderPath.filePath(targetSamplesFolderName);
        if (!rCreatedSamplesOutDir.exists())
        {
            // Can not create writable dir.
            if (!m_SamplesOutFolderPath.mkdir(targetSamplesFolderName) ||
                !(fiOfFullDirToCreateSamples = QFileInfo(rCreatedSamplesOutDir.absolutePath())).isWritable())
            {
                rPossibleError = tr("Could not create writable samples dir");
                return;
            }

            // Samples' dir was successfully created.
            else
                return;
        }

        // Trying another name.
        ++nSamplesFolderIndex;
        if (nSamplesFolderIndex == 0)
        {
            rPossibleError = tr("Could not find an appropriate name for samples dir");
            return;
        }
        targetSamplesFolderName = QString::fromLocal8Bit("%1_%2").arg(m_SamplesOutFolderName).arg(nSamplesFolderIndex);
    }
}

void SamplesGenerator::checkSizeOfSamplesSide(QString& rPossibleError)
{
    if (m_nSizeOfSamplesSide < 9)
        rPossibleError = tr("Samples sizes are too small");
    else if (m_nSizeOfSamplesSide > 255)
        rPossibleError = tr("Samples sizes are too big");
}



void SamplesGenerator::createSamplesForSymbol(const QChar& rcSymbol,
                                              const QString& rcSymbolDirName,
                                              const QDir& rcFullDirToCreateSamples,
                                              const QStringList& rcFontFamilies,
                                              QImage& rImageBuffer,
                                              QString& rPossibleError)
{
    // Creating and checking a directory to write samples in.
    const QDir cFullDirToCreateSymbolSamples = rcFullDirToCreateSamples.filePath(rcSymbolDirName);
    if (cFullDirToCreateSymbolSamples.exists())
    {
        rPossibleError = tr("The folder with samples for symbol %1 (%2) already exists")
                .arg(rcSymbol)
                .arg(cFullDirToCreateSymbolSamples.absolutePath());
        return;
    }
    if (!rcFullDirToCreateSamples.mkdir(rcSymbolDirName) ||
            !QFileInfo(cFullDirToCreateSymbolSamples.absolutePath()).isWritable())
    {
        rPossibleError = tr("Could not create a writable dir for samples of symbol %1").arg(rcSymbol);
        return;
    }



    // Creating samples with all configured variations.
    qulonglong nSampleIndex = 0;
    for (const QString& rcFontFamily : rcFontFamilies)
        for (const quint8 cnFontSize : m_FontSizes) {

            // Clearing and preparing drawing.
            rImageBuffer.fill(Qt::transparent);
            if (!m_bOnlyBacks)
            {
                QPainter painter;
                painter.begin((&rImageBuffer));
                setRequiredFlagsForPainter(painter);

                // Drawing
                painter.setFont(QFont(rcFontFamily, cnFontSize));
                painter.setPen(Qt::white); // otherwise we will draw using only alpha channel
                painter.drawText(0, 0, rImageBuffer.width(), rImageBuffer.height(), Qt::AlignCenter, rcSymbol);
                painter.end();
            }

            // Centering.
            int nHorShift = 0, nVerShift = 0;
            if (m_bIsWellCentered && !m_bOnlyBacks)
                ImagesProcessor::wellCenterSymbolImage(rImageBuffer, Qt::transparent, nHorShift, nVerShift);

            // Saving sample or generating a collection for all upscaling sizes.
            if (m_UpscalingSizes.size() == 0)
            {
                applyEffectsAndSaveSample(rImageBuffer, cFullDirToCreateSymbolSamples,
                                          nHorShift, nVerShift, rcFontFamily, cnFontSize, nSampleIndex, rPossibleError);
                if (rPossibleError != QString()) return;
            }

            // All upscaling sizes.
            else
            {
                QImage scaledImage;
                for (const quint8 cnUpscalingSize : m_UpscalingSizes) {
                    if (!m_bOnlyBacks)
                        ImagesProcessor::upscaleSymbolImage(rImageBuffer, Qt::transparent, cnUpscalingSize, scaledImage);
                    applyEffectsAndSaveSample(m_bOnlyBacks ? rImageBuffer : scaledImage,
                                              cFullDirToCreateSymbolSamples,
                                              nHorShift, nVerShift, rcFontFamily, cnFontSize,
                                              nSampleIndex, rPossibleError);
                    if (rPossibleError != QString()) return;
                }
            }
        }
}

void SamplesGenerator::applyEffectsAndSaveSample(const QImage& rcImageBuffer,
                                                 const QDir& rcFullDirToCreateSymbolSamples,
                                                 const int cnHorShift,
                                                 const int cnVerShift,
                                                 const QString& rcFontFamily,
                                                 const quint8 cnFontSize,
                                                 qulonglong& rIndex,
                                                 QString& rPossibleError)
{
    // Saves provided image and returns result flag.
    auto saveImageFunc = [&] (QImage& rImageToSave) -> bool
    {
        // Drawing neighbours if needed.
        if (!m_bOnlyBacks && m_bWithNeighbourSymbols)
        {
            QPainter painter;
            painter.begin((&rImageToSave));
            setRequiredFlagsForPainter(painter);

            // Drawing
            painter.setFont(QFont(rcFontFamily, cnFontSize));
            painter.setPen(Qt::white); // otherwise we will draw using only alpha channel
            painter.drawText(cnHorShift, cnVerShift, rImageToSave.width(), rcImageBuffer.height(), Qt::AlignCenter,
                             QString::fromLocal8Bit("%1%2%3") // some spacing between neighbour symbols
                                     .arg(m_SymbolsAlphabet.at(RandomsHelper::getRandInt(m_SymbolsAlphabet.length())))
                                     .arg(m_SpaceBetweenNeighbours)
                                     .arg(m_SymbolsAlphabet.at(RandomsHelper::getRandInt(m_SymbolsAlphabet.length()))));
            painter.end();
        }

        // Generating a texture to fit image.
        QImage textureImage;
        bool bIsTextureLight = false;
        bool bSuccess;
        if (!m_bFixedBackBlackColor)
        {
            m_cTextureGenerator.generateRandomTexture((quint16) rImageToSave.width(),
                                                      textureImage, bIsTextureLight,
                                                      bSuccess, m_bOnlyBacks || m_bNoTextualTextures);
            if (!bSuccess)
                return false;
        }
        else
        {
            textureImage = QImage(rImageToSave.size(), QImage::Format_Grayscale8);
            textureImage.fill(Qt::black);
        }

        // Generating color shift to colorize the symbol if needed.
        if (!m_bOnlyBacks)
        {
            const auto cnColorShift = m_bFixedBackBlackColor
                                      ? (quint8) 0
                                      : ImagesProcessor::generateRandomGrayColorShift(bIsTextureLight);
            // Merging texture with colorized symbol.
            ImagesProcessor::appendSymbolOnTextureWithColorizing(rImageToSave.copy(),
                                                                 cnColorShift,
                                                                 textureImage,
                                                                 rImageToSave);
        }

        // Saving image at last.
        const QImage& rcTargetImageToSave = m_bOnlyBacks ? textureImage : rImageToSave;
        const bool cnSaveResult = rcTargetImageToSave.copy(QRect(
                        (rcTargetImageToSave.width()  - m_nWidthOfOutputSamples) / 2,
                        (rcTargetImageToSave.height() - m_nSizeOfSamplesSide)    / 2,
                        m_nWidthOfOutputSamples, m_nSizeOfSamplesSide)) // cropping result image
                .save(rcFullDirToCreateSymbolSamples.filePath(QString::fromLocal8Bit("%1.png").arg(++rIndex)), "PNG", 100);
        if (!cnSaveResult)
            rPossibleError = tr("Could not save a sample for symbol with index %1").arg(rIndex);
        return cnSaveResult;
    };



    // Always saving the source image without effects.
    QImage imageBufferAdditional = rcImageBuffer.copy();
    if (!saveImageFunc(imageBufferAdditional))
        return;



    // Then saving additional filtered images.
    for (auto i = 0; i < m_nAdditionalWarpedSamplesCount; ++i)
    {
        ImagesProcessor::randomWarp(rcImageBuffer, Qt::transparent, imageBufferAdditional);
        if (!saveImageFunc(imageBufferAdditional))
            return;
    }

    // And additional shifted images.
    for (auto i = 0; i < m_nAdditionalShiftedSamplesCount; ++i)
    {
        ImagesProcessor::randomShift(rcImageBuffer, Qt::transparent, imageBufferAdditional);
        if (!saveImageFunc(imageBufferAdditional))
            return;
    }
}



void SamplesGenerator::setRequiredFlagsForPainter(QPainter& rPainter)
{
    rPainter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing |
                            QPainter::SmoothPixmapTransform | QPainter::HighQualityAntialiasing);
}