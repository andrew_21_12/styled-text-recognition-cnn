#!/bin/bash

# Including external files.
source DEPENDENCIES

# Checking if required dependencies were provided.
if [ -z "${DLIB}" ];
then
	echo Please, provide a path to the dlib library in the DEPENDENCIES file!
	exit
fi
if [ -z "${HUNSPELL}" ];
then
	echo Please, provide a path to the Hunspell library in the DEPENDENCIES file!
	exit
fi

# Declaring variables.
dir_build=build
dir_install=installed

# Creating and entering into the build dir.
# Also creating a dir for an installation.
cd "$(dirname "$0")"
mkdir -p "${dir_build}"
mkdir -p "${dir_install}"
cd "${dir_build}"

# Make files' generation:
# - defining makefiles' type;
# - pointing on the dependencies' dirs (cuDNN is optional);
# - always updating *.ts files;
# - setting a dir to install project.
cmake -G "CodeBlocks - Unix Makefiles" \
	-D DLIB_DIR="${DLIB}" \
	-D HUNSPELL_DIR="${HUNSPELL}" \
	-D CMAKE_PREFIX_PATH="${CUDNN}" \
	-D UPDATE_TRANSLATIONS=ON \
	-D CMAKE_INSTALL_PREFIX="../${dir_install}" \
	../

# Possible commands for make and cmake from the build dir:
# make                             - for usual build (can be optimized with -j options)
# cmake --build . --config Release - to build with all possible code optimizations
# make translations                - for translation files' generation (*.qm files; *.ts files will be updated)
# cmake -P cmake_install.cmake     - to install program
