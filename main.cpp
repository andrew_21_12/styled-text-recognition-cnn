#include <QApplication>
#include <QTranslator>

#include <QDir>
#include <QDebug>

#include "subroutines/samples-generator.hpp"
#include "subroutines/icdar-helper.hpp"
#include "subroutines/cnn-learning-helper.hpp"
#include "recognition/recognition-engine.hpp"

#include "ui/main-window.hpp"



// Paths to generate samples and prepare ICDAR.
#define CONFIG_PATH_TO_GENERATE_SAMPLES "/Devs/QtTempProjects/styled-text-recognition-cnn/data"
#define CONFIG_PATH_TO_ICDAR_DESC_FILE  "/Devs/QtTempProjects/styled-text-recognition-cnn/data/ICDAR03/train/char.xml"



/// Tries to load translations for the app.
void load_translations(const QApplication& rcApp)
{
    // Preparing translator.
    bool bTranslationLoaded = false;
    QTranslator translator;

    // Name of the translation file, prefix before locale and complete string.
    const QLatin1String cTransFileName("styled-text-recognition-cnn");
    const QLatin1String cLocalePrefix("_");
    const QString cFullNameOfTheTransFile = cTransFileName + cLocalePrefix + QLocale::system().name();

    // Where we can find translations.
    const QLatin1String acTranslationSources[] =
            {
                    QLatin1String("."),               // near the executable
                    QLatin1String(":/translations"),  // inside of the resources with "translations" prefix
                    QLatin1String("../translations"), // on one level upper from the exe and inside of the "translations"
                    QLatin1String("translations"),    // inside of the "translations" folder
                    QLatin1String("../../../../")     // for debug purposes in CLion
            };

    // Trying to load some translation.
    const int cnNumOfTransSources = sizeof(acTranslationSources) / sizeof(QLatin1String);
    for (int i = 0; i < cnNumOfTransSources; ++i)
    {
        bTranslationLoaded = translator.load(cFullNameOfTheTransFile,  // translation file
                                             acTranslationSources[i]); // where to search
        if (bTranslationLoaded)
            break;
    }

    // Checking if translation was loaded - loading translation or showing warning.
    if (bTranslationLoaded)
        rcApp.installTranslator(&translator);
    else
        qWarning() << QLatin1String("No translation was provided for the program");
}

/// Generates samples database.
void generate_samples()
{
    SamplesGenerator samplesGenerator;
    samplesGenerator
            .setUpscalingSizes(QList<quint8>() << 30)
            .setSamplesOutFolderPath(QDir(QLatin1String(CONFIG_PATH_TO_GENERATE_SAMPLES)));
    QString error;
    samplesGenerator.generate(error, true);
    if (error != QString())
        qDebug() << error;
}

/// Converts ICDAR samples to the usual database format, applies pre-processings.
void prepare_icdar()
{
    ICDARHelper icdarHelper;
    icdarHelper
            .setPreparedSamplesOutPath(QDir(QLatin1String(CONFIG_PATH_TO_GENERATE_SAMPLES)));
    QString error;
    icdarHelper.prepareICDAR03(
            QLatin1String(CONFIG_PATH_TO_ICDAR_DESC_FILE),
            error,
            true);
    if (error != QString())
        qDebug() << error;
}

/// Trains and tests recognition CNN.
void train_test_recognition(const int cnArgc, char** pArgv)
{
    QList<QDir> pathsToSamples;
    for (int i = 1; i < cnArgc; ++i)
        pathsToSamples << QDir(QString::fromLocal8Bit(pArgv[i]));
    CNNLearningHelper learningHelper;
    learningHelper
            .setPathsToSamples(pathsToSamples)
            .setSymbolsAlphabet(QLatin1String("0123456789"
                                              "abcdefghijklmnopqrstuvwzyx"
                                              "ABDEFGHIJLMNQRTY")); // reduced alphabet - no similar symbols
    QString error;
    learningHelper.trainAndValidateRecognitionNet(error, true);
    if (error != QString())
        qDebug() << error;
}

/// Trains and tests detection CNN.
void train_test_detection(const int cnArgc, char** pArgv)
{
    QList<QDir> pathsToSamples;
    for (int i = 1; i < cnArgc; ++i)
        pathsToSamples << QDir(QString::fromLocal8Bit(pArgv[i]));
    CNNLearningHelper learningHelper;
    learningHelper
            .setPathsToSamples(pathsToSamples);
    QString error;
    learningHelper.trainAndValidateDetectionNet(error, true);
    if (error != QString())
        qDebug() << error;
}

/// Recognizes the word presented on the image.
void recognize_word(const int cnArgc, char** pArgv)
{
    bool bSuccess;
    RecognitionEngine recognitionEngine(bSuccess);
    if (!bSuccess)
        return;
    recognitionEngine.setSymbolsAlphabet(QLatin1String("0123456789"
                                                       "abcdefghijklmnopqrstuvwzyx"
                                                       "ABDEFGHIJLMNQRTY")); // reduced alphabet - no same symbols
    for (auto i = 1; i < cnArgc; ++i)
    {
        QString error;
        QString recognizedString;
        recognitionEngine.recognizeDetectedWordImage(QString::fromLocal8Bit(pArgv[i]), recognizedString, error, true);
        if (error != QString())
            qDebug() << error;
        else
            qDebug() << recognizedString;
    }
}



/// An entry point.
int main(int argc, char** argv)
{
    // Launching as Qt app.
    QApplication app(argc, argv);

    // Applying translations.
    // load_translations(app);

    // Generating samples database.
    // generate_samples();

    // Preparing ICDAR samples.
    // prepare_icdar();

    // Training and testing recognition CNN.
    // train_test_recognition(argc, argv);

    // Training and testing detection CNN.
    // train_test_detection(argc, argv);

    // Testing engine on image with word.
    // recognize_word(argc, argv);

    // Launching the main screen.
    MainWindow* pMainWindow = new MainWindow;
    pMainWindow->show();

    return app.exec();
}
