This programs allows to perform words recognition in a natural context.
The key feature and the core of the program - is a CNN-based classifier.

You can see a small demo here: https://www.dropbox.com/s/qb2v8b4sovaludi/ocr-demo.mov?dl=0

The repo also contains some additional subroutines (to generate samples database, for example).
You can get acquainted with them in the `main.cpp` file.

DEPENDENCIES
-

Make sure that you have next dependency libraries installed:

* dlib (https://github.com/davisking/dlib)

* Qt (https://www.qt.io/ru/download/)

* OpenCV (http://opencv.org/releases.html)

* Hunspell (https://github.com/hunspell/hunspell)

* cuDNN (https://developer.nvidia.com/cudnn), optional

To build the project using `build.sh` script,
you should create a `DEPENDENCIES` file in the root of the project's sources
and fill it with lines below:

    DLIB=/path/to/dlib
    HUNSPELL=/path/to/hunspell
    CUDNN=/path/to/cudnn

To build the project in the Qt Creator or CLion you should provide CMake variables
with paths to external libraries:

    -DDLIB_DIR=/path/to/dlib 
    -DHUNSPELL_DIR=/path/to/hunspell 
    -DCMAKE_PREFIX_PATH=/path/to/cudnn

Hunspell and dlib are required to build the project, cuDNN is optional
(but quite recommended, because GPU-based computations are 20 times faster at least).