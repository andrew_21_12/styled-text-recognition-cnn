#ifndef CNN_LEARNING_HELPER_HPP
#define CNN_LEARNING_HELPER_HPP

#include <QDir>

#include "cnn-structures.hpp"

/// Helps to learn and validate CNNs.
class CNNLearningHelper: private QObject
{
    // Qt features.
    Q_OBJECT

public:

    /// Construction.
    CNNLearningHelper(QObject* pParent = Q_NULLPTR);

    // Setters.
    CNNLearningHelper& setPathsToSamples(const QList<QDir>& rcPathsToSamples);
    CNNLearningHelper& setSymbolsAlphabet(const QString& rcSymbolsAlphabet);
    CNNLearningHelper& setMaxTrainSamplesPerSymbolFromDB(const int cnMaxTrainSamplesPerSymbolFromDB);
    CNNLearningHelper& setDetectionSamplesValidationPart(const qreal cDetectionSamplesValidationPart);
    CNNLearningHelper& setSyncFileName(const QString& rcSyncFileName);
    CNNLearningHelper& setSecondsIntervalForNetsSync(const qulonglong cnSecondsIntervalForNetsSync);
    CNNLearningHelper& setSerializedLearnedNetFileName(const QString& rcSerializedLearnedNetFileName);



    /*!
     * Launches learning for recognition CNN and estimates result.
     *
     * \param rPossibleError to write possible critical errors in.
     * \param cbIsVerbose    a flag showing whether we need to output some status messages.
     */
    void trainAndValidateRecognitionNet(QString& rPossibleError, const bool cbIsVerbose = false);

    /*!
     * Launches learning for detection CNN and estimates result.
     *
     * \param rPossibleError to write possible critical errors in.
     * \param cbIsVerbose    a flag showing whether we need to output some status messages.
     */
    void trainAndValidateDetectionNet(QString& rPossibleError, const bool cbIsVerbose = false);



    /*!
     * Classifies a possible symbol in the provided window.
     *
     * \param rLearnedNet       a CNN to be used for recognition.
     * \param rcWindowImage     a window with the possible symbol.
     * \param rcAlphabet        an alphabet to decode recognized symbol.
     * \param rbSuccess         a flag showing whether the operation was successful.
     * \param rProposedSymbol   the output symbol with the biggest confidence.
     * \param rSymbolConfidence the symbol's confidence.
     */
    static void getSymbolWithConfidence(CNNRecognition& rLearnedNet,
                                        const QImage& rcWindowImage,
                                        const QString& rcAlphabet,
                                        bool& rbSuccess,
                                        QChar& rProposedSymbol,
                                        qreal& rSymbolConfidence);





private:

    /*!
     * Checks initial train datasets, alphabet and loads all samples from datasets to perform recognition net learning.
     *
     * \param rOutTrainImages   here will be written loaded train images.
     * \param rOutTrainLabels   here will be written loaded train labels.
     * \param rOutTestImages    here will be written loaded test images.
     * \param rOutTestLabels    here will be written loaded test labels.
     * \param cbRecognitionMode shows whether this method should prepare samples
     *                          for recognition (true) or detection (false).
     * \param cbIsVerbose       if we should output status messages.
     * \param rPossibleError    here we will write some possible critical errors.
     */
    void makeInitialChecksAndLoadSamplesForRecognition(std::vector<dlib::matrix<unsigned char>>& rOutTrainImages,
                                                       std::vector<unsigned long>& rOutTrainLabels,
                                                       std::vector<dlib::matrix<unsigned char>>& rOutTestImages,
                                                       std::vector<unsigned long>& rOutTestLabels,
                                                       const bool cbRecognitionMode,
                                                       const bool cbIsVerbose,
                                                       QString& rPossibleError);



    /*!
     * Runs the learned recognition net on the train and validation samples and outputs classification results.
     *
     * \param rLearnedNet   a learned net to be launched.
     * \param rcTrainImages all training images.
     * \param rcTrainLabels all training labels.
     * \param rcTestImages  all testing images.
     * \param rcTestLabels  all testing labels.
     * \param cbIsVerbose   if we should output some intermediate logs.
     */
    void testAndValidateLearnedRecognitionNet(CNNRecognition& rLearnedNet,
                                              const std::vector<dlib::matrix<unsigned char>>& rcTrainImages,
                                              const std::vector<unsigned long>& rcTrainLabels,
                                              const std::vector<dlib::matrix<unsigned char>>& rcTestImages,
                                              const std::vector<unsigned long>& rcTestLabels,
                                              const bool cbIsVerbose);

    /*!
     * Runs the learned detection net on the train and validation samples and outputs classification results.
     *
     * \param rLearnedNet   a learned net to be launched.
     * \param rcTrainImages all training images.
     * \param rcTrainLabels all training labels.
     * \param rcTestImages  all testing images.
     * \param rcTestLabels  all testing labels.
     */
    void testAndValidateLearnedDetectionNet(CNNDetection& rLearnedNet,
                                            const std::vector<dlib::matrix<unsigned char>>& rcTrainImages,
                                            const std::vector<unsigned long>& rcTrainLabels,
                                            const std::vector<dlib::matrix<unsigned char>>& rcTestImages,
                                            const std::vector<unsigned long>& rcTestLabels);



    /*!
     * Applies required filterings and pre-processings for provided image.
     *
     * \param rInOutImage a source image to be pre-processed.
     */
    static void preprocessImage(QImage& rInOutImage);



    /// Paths to load training and testing samples from.
    QList<QDir> m_PathsToSamples;

    /// An alphabet of all possible symbols to run the training and validation for.
    QString m_SymbolsAlphabet;

    /// If non-negative - restricts the max number of training samples to load for one symbol from the one database.
    int m_nMaxTrainSamplesPerSymbolFromDB;

    /// Which part (0..1) of samples for symbol from each DB should be used as validation ones for detection mode.
    qreal m_DetectionSamplesValidationPart;

    /// A filename to use for net's sync.
    QString m_SyncFileName;

    /// How many seconds we should wait to write the next sync of the net.
    qulonglong m_nSecondsIntervalForNetsSync;

    /// A filename to use for serialization file of the learned net.
    QString m_SerializedLearnedNetFileName;

};

#endif // CNN_LEARNING_HELPER_HPP
