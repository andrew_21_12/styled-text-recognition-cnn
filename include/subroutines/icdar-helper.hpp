#ifndef ICDAR_HELPER_HPP
#define ICDAR_HELPER_HPP

#include <QDir>

/// Helps to prepare ICDAR samples to the convenient format.
class ICDARHelper: private QObject
{
    // Qt features.
    Q_OBJECT

public:

    /// Construction with possible parent.
    ICDARHelper(QObject* pParent = Q_NULLPTR);

    // Setters.
    ICDARHelper& setPreparedSamplesOutPath(const QDir& rcPreparedSamplesOutPath);
    ICDARHelper& setPreparedSamplesOutFolderName(const QString& rcPreparedSamplesOutFolderName);
    ICDARHelper& setSymbolsAlphabet(const QString& rcSymbolsAlphabet);
    ICDARHelper& setPreparedSamplesSize(const quint8 cnPreparedSamplesSize);



    /*!
     * Aggregates raw ICDAR database to the convenient for the recognition engine format.
     *
     * \param rcDatabaseDescFilePath a required file (a path to it) with database description.
     * \param rPossibleError         to write a message about an error.
     * \param cbVerbose              a flag showing whether we need some status messages.
     */
    void prepareICDAR03(const QString& rcDatabaseDescFilePath,
                        QString& rPossibleError,
                        const bool cbVerbose = false);





private:

    /*!
    * Checks existence, validity and creates required folders to output prepared samples.
    *
    * \param rcDbDescFile               to get DB description file's location and put prepared samples near it
    *                                   (if no other paths were provided).
    * \param rPreparedSamplesOutRootDir a directory passed all checks and ready to write prepared samples to.
    * \param cbVerbose                  a flag showing whether we need some status messages.
    * \param rPossibleError             to write a message about an error.
    */
    void checkAndPrepareDirs(const QFile& rcDbDescFile,
                             QDir& rPreparedSamplesOutRootDir,
                             const bool cbVerbose,
                             QString& rPossibleError);

    /*!
     * Loads raw sample, processes it and saves into the output directory.
     *
     * \param rcDirWithSourceSamples     a dir with all raw source samples.
     * \param rcRelativeFilename         a relative path to the raw sample.
     * \param rcSymbolTag                a symbol's tag.
     * \param rcDirToWritePreparedSample a root directory to write processed images to.
     * \param rnIndex                    an index to save the sample with an unique name.
     * \param cbVerbose                  whether this method should make some status output.
     * \param rPossibleError             some possible errors will be written here.
     */
    void processAndSaveSample(const QDir& rcDirWithSourceSamples,
                              const QString& rcRelativeFilename,
                              const QString& rcSymbolTag,
                              const QDir& rcDirToWritePreparedSample,
                              qulonglong& rnIndex,
                              const bool cbVerbose,
                              QString& rPossibleError);



    /// A directory to save prepared samples in.
    QDir m_PreparedSamplesOutPath;

    /// A folder's name to place prepared samples in.
    QString m_PreparedSamplesOutFolderName;

    /// To deliberately process only symbols from the alphabet.
    QString m_SymbolsAlphabet;

    /// Sets output prepared samples size.
    quint8 m_nPreparedSamplesSize;

};

#endif // ICDAR_HELPER_HPP