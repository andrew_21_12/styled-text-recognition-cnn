#ifndef SAMPLES_GENERATOR_HPP
#define SAMPLES_GENERATOR_HPP

#include <QDir>

#include "utility/textures-generator.hpp"

/// Generates configurable samples databases.
class SamplesGenerator: private QObject
{
    // Using Qt features.
    Q_OBJECT

public:

    /*!
     * Constructs a sample generator's instance with default configs.
     *
     * \param pParent the parent of this object if needed.
     */
    SamplesGenerator(QObject* pParent = Q_NULLPTR);

    // Setters.
    SamplesGenerator& setSamplesOutFolderPath(const QDir& rcSamplesOutFolderPath);
    SamplesGenerator& setSamplesOutFolderName(const QString& rcSamplesOutFolderName);
    SamplesGenerator& setSizeOfSamplesSide(const quint8 cnSizeOfSamplesSide);
    SamplesGenerator& setWidthOfOutputSamples(const quint8 cnWidthOfOutputSamples);
    SamplesGenerator& setOnlyBacks(const bool cbOnlyBacks);
    SamplesGenerator& setNoTextualTextures(const bool cbNoTextualTextures);
    SamplesGenerator& setFixedBackBlackColor(const bool cbFixedBackBlackColor);
    SamplesGenerator& setFontsPaths(const QList<QString>& rcFontsPaths);
    SamplesGenerator& setUseOnlyFontsFromPaths(const bool cbUseOnlyFontsFromPaths);
    SamplesGenerator& setFontsExceptions(const QList<QString>& rcFontsExceptions);
    SamplesGenerator& setFontsSizes(const QList<quint8>& rcFontSizes);
    SamplesGenerator& setWithNeighbourSymbols(const bool cbWithNeighbourSymbols);
    SamplesGenerator& setSpaceBetweenNeighbours(const QString& rcSpaceBetweenNeighbours);
    SamplesGenerator& setWellCentered(const bool cbIsWellCentered);
    SamplesGenerator& setUpscalingSizes(const QList<quint8>& rcUpscalingSizes);
    SamplesGenerator& setAdditionalWarpedSamplesCount(const quint8 cnAdditionalWarpedSamplesCount);
    SamplesGenerator& setAdditionalShiftedSamplesCount(const quint8 cnAdditionalShiftedSamplesCount);
    SamplesGenerator& setSymbolsAlphabet(const QString& rcSymbolsAlphabet);



    /*!
     * Begins samples generation.
     *
     * \param rPossibleCriticalError here will be written a possible critical error (which stops generation).
     * \param cbVerbose              a flag if generation should output status messages.
     */
    void generate(QString& rPossibleCriticalError, const bool cbVerbose = false);





private:

    /**
     * Checks whether provided path to generate samples is correct and possible to write to.
     *
     * \param rPossibleError some possible error after the check.
     * */
    void checkSamplesOutFolder(QString& rPossibleError);

    /*!
     * Creates a directory to write generated samples in and checks it.
     *
     * \param rCreatedSamplesOutDir here will be written a successfully created dir.
     * \param rPossibleError        will contain an error message if something fails.
     */
    void createAndCheckSamplesOutDir(QDir& rCreatedSamplesOutDir, QString& rPossibleError);

    /*!
     * Checks if provided size for sample is too slow or too big.
     *
     * \param rPossibleError if the check has failed.
     */
    void checkSizeOfSamplesSide(QString& rPossibleError);



    /*!
     * Generates a full set of samples for one provided symbol.
     *
     * \param rcSymbol                 a symbol to create samples for.
     * \param rcSymbolDirName          a dir name to write symbol's samples in.
     * \param rcFullDirToCreateSamples a folder to create a directory with samples for symbol in.
     * \param rcFontFamilies           all pre-loaded font families to generate samples with (exception fonts are excluded).
     * \param rImageBuffer             to generate images faster.
     * \param rPossibleError           some possible error while samples creation.
     */
    void createSamplesForSymbol(const QChar& rcSymbol,
                                const QString& rcSymbolDirName,
                                const QDir& rcFullDirToCreateSamples,
                                const QStringList& rcFontFamilies,
                                QImage& rImageBuffer,
                                QString& rPossibleError);

    /*!
     * Applies some effects if needed and saves provided image buffer with sample on disk.
     *
     * \param rcImageBuffer                  an image to be filtered and saved.
     * \param rcFullDirToCreateSymbolSamples a directory to save an image in.
     * \param cnHorShift                     to add neighbour chars.
     * \param cnVerShift                     to add neighbour chars.
     * \param rIndex                         image's index to keep.
     * \param rPossibleError                 some possible error while saving.
     */
    void applyEffectsAndSaveSample(const QImage& rcImageBuffer,
                                   const QDir& rcFullDirToCreateSymbolSamples,
                                   const int cnHorShift,
                                   const int cnVerShift,
                                   const QString& rcFontFamily,
                                   const quint8 cnFontSize,
                                   qulonglong& rIndex,
                                   QString& rPossibleError);



    /*!
     * Configures painter by setting required flags for its rendering.
     *
     * \param rPainter a painter to be updated.
     */
    void setRequiredFlagsForPainter(QPainter& rPainter);



    /// A path to save generated samples in.
    QDir m_SamplesOutFolderPath;

    /// The basic name of the folder to save generated samples in.
    QString m_SamplesOutFolderName;

    /// A size of sides of square samples to be created.
    quint8 m_nSizeOfSamplesSide;

    /// You can crop a central part with provided width from the prepared sample.
    quint8 m_nWidthOfOutputSamples;

    /// Generates only backgrounds without text symbols at all.
    bool m_bOnlyBacks;

    /// Whether we don't textures with texts on them.
    bool m_bNoTextualTextures;

    /// Set it to true if you want to generate symbols with black background instead of textures.
    bool m_bFixedBackBlackColor;

    /// Path to all external custom fonts to be used.
    QList<QString> m_FontsPaths;

    /// If set to true - system fonts won't be used, only one provided in paths.
    bool m_bUseOnlyFontsFromPaths;

    /// Fonts will not be included to generate samples.
    QList<QString> m_FontsExceptions;

    /// How many different font sizes we should use.
    QList<quint8> m_FontSizes;

    /// Set it to true if you want to generate samples with border symbols on the left and right.
    bool m_bWithNeighbourSymbols;

    /// How many spaces or some special chars should be between two neighbours symbols (if they are enabled).
    QString m_SpaceBetweenNeighbours;

    /// A flag whether we should well-center generated samples.
    bool m_bIsWellCentered;

    /// Sizes to upscale samples to.
    QList<quint8> m_UpscalingSizes;

    /// How many additional warped samples should we create.
    quint8  m_nAdditionalWarpedSamplesCount;

    /// How many additional samples with shifts applied should we create.
    quint8 m_nAdditionalShiftedSamplesCount;

    /// An alphabet of possible symbols in a sample database.
    QString m_SymbolsAlphabet;



    /// It's impossible to pretend samples without background textures, so we need a generator for textures.
    const TexturesGenerator m_cTextureGenerator;

};

#endif // SAMPLES_GENERATOR_HPP