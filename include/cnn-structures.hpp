#ifndef CNN_STRUCTURES_HPP
#define CNN_STRUCTURES_HPP

#include <dlib/data_io.h>
#include <dlib/dnn.h>

/// The global variable with input size of nets.
const static quint8 CNN_INPUT_SIDE_SIZE = 32;

/// Describing convolutional network's structure for accurate symbols recognition.
using CNNRecognition = dlib::loss_multiclass_log<                                       // loss function
                               dlib::fc<52,                                             // fully-connected layer with number of classes
                               dlib::max_pool<2,2,2,2,dlib::relu<dlib::con<720,2,2,1,1, // convolutional and pooling layers (2nd conv-layer)
                               dlib::max_pool<5,5,5,5,dlib::relu<dlib::con<115,8,8,1,1, // 1st convolutional layer
                               dlib::input<dlib::matrix<unsigned char>>>>>>>>>>;        // input layer

/// Describing convolutional network's structure for fast symbols recognition.
using CNNRecognitionFast = dlib::loss_multiclass_log<                               // loss function
                           dlib::fc<52,                                             // fully-connected layer with number of classes
                           dlib::max_pool<2,2,2,2,dlib::relu<dlib::con<256,2,2,1,1, // convolutional and pooling layers (2nd conv-layer)
                           dlib::max_pool<5,5,5,5,dlib::relu<dlib::con<96,8,8,1,1,  // 1st convolutional layer
                           dlib::input<dlib::matrix<unsigned char>>>>>>>>>>;        // input layer

/// Describing net's structure for text detection.
using CNNDetection = dlib::loss_multiclass_log<                               // loss function
                     dlib::fc<2,                                              // fully-connected layer (only 2 classes)
                     dlib::max_pool<2,2,2,2,dlib::relu<dlib::con<256,2,2,1,1, // convolutional and pooling layers (2nd conv layer)
                     dlib::max_pool<5,5,5,5,dlib::relu<dlib::con<96,8,8,1,1,  // 1st convolutional layer
                     dlib::input<dlib::matrix<unsigned char>>>>>>>>>>;        // input layer

#endif // CNN_STRUCTURES_HPP