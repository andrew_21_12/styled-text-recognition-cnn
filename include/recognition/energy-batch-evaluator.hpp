#ifndef ENERGY_BATCH_EVALUATOR
#define ENERGY_BATCH_EVALUATOR

#include <QThread>
#include <QWaitCondition>

#include <hunspell/hunspell.hxx>

/// Helps to evaluate word prediction energies in parallel.
class EnergyBatchEvaluator: public QThread
{
    // Qt features.
    Q_OBJECT

public:

    /// Construction. See variables description in the private variables list below this file.
    EnergyBatchEvaluator(quint8& rnEvaluatedBatchesCount,
                         QMutex& rUpdateBatchesCountMutex,
                         QWaitCondition& rCondAllBatchesEvaluated,
                         const QList<qreal>& rcConfidencesMiniBatch,
                         const QList<QRect>& rcBoxesMiniBatch,
                         const QList<QChar>& rcRecognizedCharsMiniBatch,
                         const quint32 cnStartCombinationInclusive,
                         const quint32 cnEndCombinationNonInclusive,
                         Hunspell* pHunspell,
                         QObject* pParent = Q_NULLPTR);



    /// Begins energy evaluation with provided data.
    void evaluate();

    /// Requests evaluator to stop computations.
    void stopEvaluation();

    /*!
     * Allows to access to the evaluated max energy value.
     *
     * \return Evaluated max energy.
     */
    qreal getEvaluatedMaxEnergy() const;

    /*!
     * Allows to access to the evaluated max energy combination.
     *
     * \return Evaluated max energy combination code.
     */
    quint32 getEvaluatedMaxEnergyCombination() const;

    /*!
     * Prediction of the batch.
     *
     * \return The word with the biggest energy from the batch.
     */
    QString getPredictedWord() const;





protected:

    /// Runs thread's evaluations.
    virtual void run();





private:

    /*!
  * Evaluates the energy for current windows with classifier confidences and its positions:
  * the more energy - the more probability that we have found some word.
  *
  * \param rcConfidences  confidences for symbols in provided windows.
  * \param rcWindowsBoxes windows boxes -
  *                       it's assumed that they are sorted and matched with confidences above by indices.
  * \param rcWord         a word to eval energy.
  * \param cAlpha         an energy coef for distances.
  * \param cBetta         an energy coef for confidences.
  * \param cGamma         an energy for word's knowledge.
  *
  * \return Energy value.
  */
    qreal evalEnergyForWindows(const QList<qreal>& rcConfidences,
                               const QList<QRect>& rcWindowsBoxes,
                               const QString& rcWord,
                               const qreal cAlpha = 2.95,
                               const qreal cBetta = 1.0,
                               const qreal cGamma = .05);



    /// To keep the number of batches which have completed their evaluations.
    quint8& m_rnEvaluatedBatchesCount;

    /// To update completed batches count without multi-thread conflicts.
    QMutex& m_rUpdateBatchesCountMutex;

    /// To wake the main thread being waited for result from all batches.
    QWaitCondition& m_rCondAllBatchesEvaluated;

    /// Confidences to perform evaluations.
    const QList<qreal>& m_rcConfidencesMiniBatch;

    /// Bounding boxes to perform evaluations.
    const QList<QRect>& m_rcBoxesMiniBatch;

    /// Recognized chars corresponding to boxes and confidences.
    const QList<QChar>& m_rcRecognizedCharsMiniBatch;

    /// The first (inclusive) combination to eval the energy.
    const quint32 m_cnStartCombinationInclusive;

    /// The last (exclusive) combination to eval the energy.
    const quint32 m_cnEndCombinationNonInclusive;

    /// To check predictions with a dictionary.
    Hunspell* m_pHunspell;



    /// To evaluate and return the max energy for batch.
    qreal m_EvaluatedMaxEnergy;

    /// To keep a combination corresponding to the max energy value.
    quint32 m_nEvaluatedMaxEnergyCombination;

    /// The word with the best energy.
    QString m_PredictedWord;

};

#endif // ENERGY_BATCH_EVALUATOR