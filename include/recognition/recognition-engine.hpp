#ifndef RECOGNITION_ENGINE_HPP
#define RECOGNITION_ENGINE_HPP

#include <QObject>

#include <hunspell/hunspell.hxx>

#include "cnn-structures.hpp"

/// Recognizes words on images.
class RecognitionEngine: private QObject
{
    // Qt features.
    Q_OBJECT

public:

    /*!
     * Default construction.
     *
     * \param pParent   the parent object of the engine.
     * \param rbSuccess whether the instance of the engine was successfully created.
     *
     * \throw An error if deserialization of the learned net has failed.
     */
    RecognitionEngine(bool& rbSuccess, QObject* pParent = Q_NULLPTR);

    /// Destruction.
    ~RecognitionEngine();

    // Setters.
    RecognitionEngine& setSymbolsAlphabet(const QString& rcSymbolsAlphabet);



    /*!
     * Tries to recognize a word presented on the image.
     *
     * \param rcImageFilePath      an absolute file path to the target image.
     * \param rOutRecognizedString a result of the recognition.
     * \param rPossibleError       a some possible error while recognition.
     * \param cbVerbose            a flag showing whether we need to output some status messages.
     */
    void recognizeDetectedWordImage(const QString& rcImageFilePath,
                                    QString& rOutRecognizedString,
                                    QString& rPossibleError,
                                    const bool cbVerbose = false);

    /*!
     * Tries to recognize a word presented on the image.
     *
     * \param rcWordImage          an image with some word.
     * \param rOutRecognizedString a result of the recognition.
     * \param rPossibleError       a some possible error while recognition.
     * \param cbVerbose            a flag showing whether we need to output some status messages.
     */
    void recognizeDetectedWordImage(const QImage& rcWordImage,
                                    QString& rOutRecognizedString,
                                    QString& rPossibleError,
                                    const bool cbVerbose = false);





private:

    /*!
     * Extracts all possible bounding boxes with features.
     *
     * \param rcImageToExtractBoundingBoxes a source image to find features in.
     * \param cnWidthToScale                a width to scale source image to.
     * \param cnHeightToScale               a height to scale source image to.
     * \param cMinSquareCoef                a min part of image's square which can be the square of features.
     * \param rOutBoundingBoxes             output found bounding boxes of features.
     * \param rPossibleError                a some possible error features detection.
     */
    void extractAllBoundingBoxes(const QImage& rcImageToExtractBoundingBoxes,
                                 const quint16 cnWidthToScale,
                                 const quint16 cnHeightToScale,
                                 const qreal cMinSquareCoef,
                                 QList<QRect>& rOutBoundingBoxes,
                                 QString& rPossibleError);

    /*!
     * Removes (and sorts) all non-necessary bounding boxes to make further computations faster.
     *
     * \param rInOutBoundingBoxes   a source (and out) bounding boxes to reduce the count of.
     * \param cnMinBoxHeight        the bounding box's height should be at least of this value to avoid being filtered.
     * \param cnMinBoxWidth         the bounding box's width should be at least of this value.
     * \param cTooBroadBoxWidthPart the bounding box will be dropped if it has width more then this part of the height.
     */
    void filterAndSortBoundingBoxes(QList<QRect>& rInOutBoundingBoxes,
                                    const quint16 cnMinBoxHeight,
                                    const quint16 cnMinBoxWidth,
                                    const qreal cTooBroadBoxWidthPart);

    /*!
     * Runs sliding windows to perform classifications for all sliding windows (bounding boxes).
     *
     * \param rcTargetImage                    an image to run classifications for.
     * \param rInOutBoundingBoxes              sliding windows to be checked and updated (a little bit on extension values below).
       \param cnBoxAdditionalCaptureHorizontal how many external horizontal space should we include in the bounding box.
     * \param cnBoxAdditionalCaptureVertical   --//-- vertical space.
     * \param cnBoxWidthAddition               a width extension for the sliding window.
     * \param cnBoxHeightAddition              a height extension.
     * \param rOutConfidences                  found confidences for recognized symbols.
     * \param rOutRecognizedSymbols            classified symbols from bounding boxes.
     * \param rPossibleError                   to write a possible critical error description.
     */
    void classifyAndModifyBoundingBoxes(const QImage& rcTargetImage,
                                        QList<QRect>& rInOutBoundingBoxes,
                                        const quint8 cnBoxAdditionalCaptureHorizontal,
                                        const quint8 cnBoxAdditionalCaptureVertical,
                                        const quint8 cnBoxWidthAddition,
                                        const quint8 cnBoxHeightAddition,
                                        QList<qreal>& rOutConfidences,
                                        QList<QChar>& rOutRecognizedSymbols,
                                        QString& rPossibleError);

    /*!
     * Filters classified bounding boxes using NMS and by removing similar windows with the same symbol.
     *
     * \param rInOutConfidences             all confidences to be reduced.
     * \param rInOutBoundingBoxes           bounding boxes to be reduced.
     * \param rInOutRecognizedSymbols       recognized symbols to be reduced.
     * \param cDuplicatedBoxesThresholdCoef which part of widths or heights sums of two boxes should be taken
     *                                      to evaluate a threshold of boxes similarity.
     * \param rcDeltasForNMS                NMS deltas values to perform NMS filtering.
     */
    void filterClassifiedBoundingBoxes(QList<qreal>& rInOutConfidences,
                                       QList<QRect>& rInOutBoundingBoxes,
                                       QList<QChar>& rInOutRecognizedSymbols,
                                       const qreal cDuplicatedBoxesThresholdCoef,
                                       const QList<quint8>& rcDeltasForNMS);

    /*!
     * Constructs string from provided values.
     *
     * \param rcConfidences                 confidences to construct string.
     * \param rcBoundingBoxes               bounding boxes to construct string.
     * \param rcRecognizedSymbols           recognized symbols to construct string.
     * \param cMaxSymbolsDistanceInWordCoef a coef showing which part of mean distance between all windows
     *                                      should we take as max distance between symbols in word.
     * \param cnMaxBatchSizeForWord         the max batch size to find the best symbols for word.
     * \param rOutRecognizedString          a recognized string.
     * \param cbVerbose                     a flag showing whether we need extra outputs.
     * \param rPossibleError                for critical errors.
     */
    void constructStringFromClassifications(const QList<qreal>& rcConfidences,
                                            const QList<QRect>& rcBoundingBoxes,
                                            const QList<QChar>& rcRecognizedSymbols,
                                            const qreal cMaxSymbolsDistanceInWordCoef,
                                            const quint8 cnMaxBatchSizeForWord,
                                            QString& rOutRecognizedString,
                                            const bool cbVerbose,
                                            QString& rPossibleError);



    /// An alphabet of symbols which are possible to be recognized.
    QString m_SymbolsAlphabet;

    /// A learned network to be used for recognition of symbols.
    CNNRecognition m_LearnedNet;

    /// A spell-checking engine for better recognition.
    Hunspell* m_pHunspell;

};

#endif // RECOGNITION_ENGINE_HPP