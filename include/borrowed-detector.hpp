#ifndef BORROWED_DETECTOR_HPP
#define BORROWED_DETECTOR_HPP

#include <QImage>

#include <opencv2/core.hpp>

#include "utility/images-converter.hpp"

using namespace cv;
using namespace std;

/// Just some ready text-detection methods from: http://stackoverflow.com/questions/23506105/extracting-text-opencv
class BorrowedDetector
{

public:

    static void detectAllTextRegions(const QImage& rcTargetImage, QList<QRect>& rFoundRegions)
    {
        rFoundRegions.clear();



        cv::Mat img;
        ImagesConverter::QImageToCvMat(rcTargetImage, img);
        cv::Mat img_gray, img_sobel, img_threshold, element;
        cvtColor(img, img_gray, CV_BGR2GRAY);
        cv::Sobel(img_gray, img_sobel, CV_8U, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
        cv::threshold(img_sobel, img_threshold, 0, 255, CV_THRESH_OTSU+CV_THRESH_BINARY);
        element = getStructuringElement(cv::MORPH_RECT, cv::Size(17, 3) );
        cv::morphologyEx(img_threshold, img_threshold, CV_MOP_CLOSE, element); //Does the trick
        std::vector< std::vector< cv::Point> > contours;
        cv::findContours(img_threshold, contours, 0, 1);
        std::vector<std::vector<cv::Point> > contours_poly( contours.size() );
        for( int i = 0; i < contours.size(); i++ )
            if (contours[i].size()>100)
            {
                cv::approxPolyDP( cv::Mat(contours[i]), contours_poly[i], 3, true );
                cv::Rect appRect( boundingRect( cv::Mat(contours_poly[i]) ));
                if (appRect.width>appRect.height)
                    rFoundRegions.append(QRect(appRect.x, appRect.y, appRect.width, appRect.height));
            }





//        Mat inImg, rgb;
//        ImagesConverter::QImageToCvMat(rcTargetImage, inImg);
//
//        // downsample and use it for processing
//        pyrDown(inImg, rgb);
//        Mat small;
//        cvtColor(rgb, small, CV_BGR2GRAY);
//
//        // morphological gradient
//        Mat grad;
//        Mat morphKernel = getStructuringElement(MORPH_ELLIPSE, Size(3, 3));
//        morphologyEx(small, grad, MORPH_GRADIENT, morphKernel);
//
//        // binarize
//        Mat bw;
//        threshold(grad, bw, 0.0, 255.0, THRESH_BINARY | THRESH_OTSU);
//
//        // connect horizontally oriented regions
//        Mat connected;
//        morphKernel = getStructuringElement(MORPH_RECT, Size(9, 1));
//        morphologyEx(bw, connected, MORPH_CLOSE, morphKernel);
//
//        // find contours
//        Mat mask = Mat::zeros(bw.size(), CV_8UC1);
//        vector<vector<Point>> contours;
//        vector<Vec4i> hierarchy;
//        findContours(connected, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
//
//        // filter contours
//        for(int idx = 0; idx >= 0; idx = hierarchy[idx][0])
//        {
//            Rect rect = boundingRect(contours[idx]);
//            Mat maskROI(mask, rect);
//            maskROI = Scalar(0, 0, 0);
//            // fill the contour
//            drawContours(mask, contours, idx, Scalar(255, 255, 255), CV_FILLED);
//            // ratio of non-zero pixels in the filled region
//            double r = (double)countNonZero(maskROI)/(rect.width*rect.height);
//
//            if (r > .45 /* assume at least 45% of the area is filled if it contains text */
//                &&
//                (rect.height > 8 && rect.width > 8) /* constraints on region size */
//                /* these two conditions alone are not very robust. better to use something
//                like the number of significant peaks in a horizontal projection as a third condition */
//                    )
//            {
//                rFoundRegions.append(QRect(rect.x, rect.y, rect.width, rect.height));
//            }
//        }





//        vector< Mat> channels;
//        text::computeNMChannels(inImg, channels);
//
//        int cn = (int)channels.size();
//        // Append negative channels to detect ER- (bright regions over dark background)
//        for (int c = 0; c < cn - 1; c++)
//            channels.push_back(255 - channels[c]);
//
//        Ptr< text::ERFilter> er_filter1 = text::createERFilterNM1(text::loadClassifierNM1("/Users/Andrew/Desktop/CNN/trained_classifierNM1.xml"),
//                                                                  16, 0.00015f, 0.13f, 0.2f, true, 0.1f);
//        Ptr< text::ERFilter> er_filter2 = text::createERFilterNM2(text::loadClassifierNM2("/Users/Andrew/Desktop/CNN/trained_classifierNM2.xml"), 0.5);
//
//        vector< vector< text::ERStat> > regions(channels.size());
//
//        for (int c = 0; c< (int)channels.size(); c++)
//        {
//            er_filter1->run(channels[c], regions[c]);
//            er_filter2->run(channels[c], regions[c]);
//        }
//
//        // Detect character groups
//        cout << "Grouping extracted ERs ... ";
//        vector< vector< Vec2i> > region_groups;
//        vector< Rect> groups_boxes;
//        text::erGrouping(inImg, channels, regions, region_groups, groups_boxes, text::ERGROUPING_ORIENTATION_HORIZ);
//
//        groups_draw(inImg, groups_boxes);
//        imshow("grouping", inImg);
//
//        waitKey(0);
    }

};

#endif // BORROWED_DETECTOR_HPP