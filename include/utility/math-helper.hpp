#ifndef MATH_HELPER_HPP
#define MATH_HELPER_HPP

#include <QList>

/// Simplifies evaluation of some common functions.
class MathHelper
{

public:

    /*!
     * Evaluates softmax values for provided source values.
     *
     * \param rcSrcValues       source values to eval softmax for.
     * \param rOutSoftmaxValues output softmax values.
     */
    static void softmax(const QList<qreal>& rcSrcValues, QList<qreal>& rOutSoftmaxValues);

    /*!
     * Applies Non-Maximal Suppression for provided list of values. Assumed that this list is sorted.
     *
     * \param rInOutValues a source and out list of values to apply the NMS for.
     * \param cnDelta      a delta in both directions to compare source values with, must be positive.
     */
    static void NMS(QList<qreal>& rInOutValues, const quint8 cnDelta);





private:

    /// Hiding construction.
    MathHelper()
    {};

};

#endif // MATH_HELPER_HPP