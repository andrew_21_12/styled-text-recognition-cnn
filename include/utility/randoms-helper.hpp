#ifndef RANDOMS_HELPER_HPP
#define RANDOMS_HELPER_HPP

/// Simplifies randoms generation.
class RandomsHelper
{

public:

    /*!
     * Generates random integer from the diapason (including boundary values).
     *
     * \param cnMin a minimal rand int to be generated.
     * \param cnMax a max rand int to be generated.
     *
     * \return Generated random int.
     */
    static int genRandIntInRange(const int cnMin, const int cnMax);

    /*!
     * Generates random true or false value.
     *
     * @return True or false randomly.
     */
    static bool flipCoin();

    /*!
     * Generates some random integer from zero (inclusively) and up to provided bound (exclusively).
     *
     * \param cnTopBound a top bound (exclusive) for integer to be generated.
     *
     * \return Generated random int.
     */
    static int getRandInt(const int cnTopBound);





private:

    /// Hiding construction.
    RandomsHelper()
    {};

};

#endif // RANDOMS_HELPER_HPP