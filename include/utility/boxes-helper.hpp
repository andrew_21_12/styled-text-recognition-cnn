#ifndef BOXES_HELPER_HPP
#define BOXES_HELPER_HPP

#include <QRect>

/// Some very basic ops with boxes.
class BoxesHelper
{

public:

    /*!
     * Evaluates distance between centers of two provided bounding boxes.
     *
     * \param rcBox1 the first box to eval distance.
     * \param rcBox2 the second.
     *
     * \return Evaluated distance.
     */
    static qreal distanceBetweenBoxesCenters(const QRect& rcBox1, const QRect& rcBox2);

    /*!
     * Evaluates distance between closest horizontal bounds of provided boxes.
     *
     * \param rcBoxLeft  the first box.
     * \param rcBoxRight the second box.
     *
     * \return Evaluated distance.
     */
    static qreal distanceBetweenBoxesBounds(const QRect& rcBoxLeft, const QRect& rcBoxRight);

};

#endif // BOXES_HELPER_HPP