#ifndef IMAGES_CONVERTER_HPP
#define IMAGES_CONVERTER_HPP

#include <QImage>

#include <dlib/data_io.h>

#include <opencv2/opencv.hpp>

/// Helps to invoke different image convertations.
class ImagesConverter
{

public:

    /*!
     * Converts provided QImage to the dlib's grayscale matrix format. Deep.
     *
     * \param rcSrcImage a source image to be converted.
     * \param rOutMatrix dlib's matrix to write convertation in.
     */
    static void QImageToGrayDlibMatrix(const QImage& rcSrcImage, dlib::matrix<unsigned char>& rOutMatrix);

    /*!
     * Converts provided OpenCV mat to the QImage format.
     *
     * \param rcSrcMat  a source mat to convert from.
     * \param rOutImage a converted image.
     * \param cbInCloneImageData a flag showing whether we should clone source image (to make a deep copy).
     */
    static void cvMatToQImage(const cv::Mat& rcSrcMat, QImage& rOutImage, const bool cbInCloneImageData = false);

    /*!
     * Converts QImage to OpenCV's mat.
     *
     * \param rcSrcImage         an image to be converted.
     * \param rOutMat            the result of the convertation.
     * \param cbInCloneImageData a flag showing whether we should clone source image
     *                           (always be copied in a case of RGB888 format).
     */
    static void QImageToCvMat(const QImage& rcSrcImage, cv::Mat& rOutMat, const bool cbInCloneImageData = true);

};

#endif // IMAGES_CONVERTER_HPP
