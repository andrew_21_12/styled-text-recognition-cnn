#ifndef IMAGES_PROCESSOR_HPP
#define IMAGES_PROCESSOR_HPP

#include <QImage>

/// Applies effects, stylizations and normalizations to samples.
class ImagesProcessor
{

public:

    /*!
     * Centers drawing in the image.
     *
     * \param rInOutImage       an image to apply well-centering for.
     * \param rcBackColor       a background color to fill non-symbol areas.
     * \param rnHorizontalShift an evaluated horizontal shift.
     * \param rnVerticalShift   an evaluated vertical shift.
     */
    static void wellCenterSymbolImage(QImage& rInOutImage,
                                      const QColor& rcBackColor,
                                      int& rnHorizontalShift,
                                      int& rnVerticalShift);

    /*!
     * Upscales image's content to the requested size (the result image of course is not linked with the target one).
     *
     * \param rcTargetImage       an image to be upscaled.
     * \param rcBackColor         to find bounds of the content.
     * \param cnTargetContentSide a required content size to upscale to.
     * \param rOutResultImage     a scaled result image.
     */
    static void upscaleSymbolImage(const QImage& rcTargetImage,
                                   const QColor& rcBackColor,
                                   const quint16 cnTargetContentSide,
                                   QImage& rOutResultImage);

    /*!
     * Applies the random warp to the image (the result image won't be linked with the target one).
     *
     * \param rcTargetImage   an image to apply warp to.
     * \param rcBackColor     a background color to fill appeared shifts with.
     * \param rOutResultImage an output image.
     * \param cnWarpMaxShift   this number describes how powerful warp distortion should be.
     */
    static void randomWarp(const QImage& rcTargetImage,
                           const QColor& rcBackColor,
                           QImage& rOutResultImage,
                           const quint16 cnWarpMaxShift = 4);

    /*!
     * Applies random shift (translation) for provided image.
     *
     * \param rcTargetImage   an image to apply shift to.
     * \param rcBackColor     a color to fill created empty spaces with.
     * \param rOutResultImage an image with the result of the shift application.
     * \param cnMaxShift      a maximum shift value to be applied.
     */
    static void randomShift(const QImage& rcTargetImage,
                            const QColor& rcBackColor,
                            QImage& rOutResultImage,
                            const quint16 cnMaxShift = 5);

    /*!
     * Resizes provided image to fit center of the frame with provided sides (empty spaces will be filled with mean color).
     *
     * \param rcTargetImage       an image to apply an operation for - won't be changed.
     * \param cnWidth             an output image width.
     * \param cnHeight            an output image height.
     * \param rOutResultImage     a result image.
     * \param rOutMeanColor       an evaluated mean color of the source image.
     * \param cnHorizontalPadding if we need an additional horizontal spacing for symbol.
     * \param cnVerticalPadding   if we need an additional vertical spacing for symbol.
     */
    static void resizeToFitCenterWithMeanBackground(const QImage& rcTargetImage,
                                                    const quint16 cnWidth, const quint16 cnHeight,
                                                    QImage& rOutResultImage,
                                                    QColor& rOutMeanColor,
                                                    const quint8 cnHorizontalPadding = 0,
                                                    const quint8 cnVerticalPadding = 0);



    /*!
     * Generates a random grayscale color shift to subtract from a white color
     * to obtain the new high-contrast intensity.
     *
     * \param cbIsDark    a flag showing whether the result color should be dark (or not).
     * \param cnThreshold a distance to the most intensive color (black or white).
     *
     * \return Generated shift.
     */
    static quint8 generateRandomGrayColorShift(const bool cbIsDark, const quint8 cnThreshold = 70);

    /*!
     * Draws provided symbol with colorizing above (on) the provided texture.
     *
     * \param rcSymbolImage         an image with drawn symbol.
     * \param cnColorShiftForSymbol a shift to apply for color of the symbol.
     * \param rcTexture             a texture to draw the symbol above of.
     * \param rOutResultImage       an output of the operation.
     */
    static void appendSymbolOnTextureWithColorizing(const QImage& rcSymbolImage,
                                                    const quint8 cnColorShiftForSymbol,
                                                    const QImage& rcTexture,
                                                    QImage& rOutResultImage);

    /*!
     * Inverts an image.
     *
     * \param rcTargetImage   a source image to be inverted.
     * \param rOutResultImage an inverted output image.
     */
    static void invertImageColors(const QImage& rcTargetImage, QImage& rOutResultImage);



    /*!
     * Normalizes contrast in the provided image to make it brighter.
     *
     * \param rInOutImage an image contrast normalization to be applied to.
     */
    static void normalizeContrast(QImage& rInOutImage);

    /*!
     * Applies ZCA whitening for a grayscale image.
     *
     * \see http://www.compvision.ru/forum/index.php?/topic/991-нормализация-изображения-приведение-к-форме-удобной-для-классификаторов/
     *
     * \param rInOutImage an image to apply the whitening for.
     * \param cEpsilon    the power constant of the whitening.
     */
    static void applyZCAWhitening(QImage& rInOutImage, const float cEpsilon = 7.0);

    /*!
     * Blurs provided image inplace.
     *
     * \param rInOutImage an image to be blurred.
     * \param cnPower     the power (size) of the blur to be applied.
     */
    static void blur(QImage& rInOutImage, const quint8 cnPower = 2);

    /*!
     * Applies k-means clusterization for image.
     *
     * \param rcTargetImage        an image to apple the clusterization for.
     * \param rOutClusterizedImage a clusterized result image.
     * \param rOutContrastQueImage an image with applied contrast que (if needed).
     * \param cbNeedContrastCue    a flag showing whether we need to apply a contrast que for image.
     * \param cnClustersCount      how many clusters should output image have.
     */
    static void clusterizeImageColorsAndApplyContrastQue(const QImage& rcTargetImage,
                                                         QImage& rOutClusterizedImage,
                                                         QImage& rOutContrastQueImage,
                                                         const bool cbNeedContrastCue = true,
                                                         const quint8 cnClustersCount = 5);



    /*!
     * Detects all features on the provided image.
     *
     * \see http://study.marearts.com/2015/06/opencv-mser-example-opencv-300.html for description of the MSER application.
     *
     * \param rcTargetImage  an image to find features in.
     * \param rFeatureBoxes  an output found features bounding boxes.
     * \param cMinSquareCoef which part of image's square should be taken as a minimal square for features.
     */
    static void detectFeatures(const QImage& rcTargetImage,
                               QList<QRect>& rFeatureBoxes,
                               const qreal cMinSquareCoef = 0.00002);





private:

    /*!
     * Checks image's validity and finds its bounds.
     *
     * \param rcTargetImage an image to check and find content bounds.
     * \param rcBackColor   a background color to find the content.
     * \param rnTop         a found top bound.
     * \param rnLeft        left bound.
     * \param rnBottom      bottom bound.
     * \param rnRight       right bound.
     * \param rbSuccess     if the check and content's search were successful.
     */
    static void checkImageAndGetContentBounds(const QImage& rcTargetImage, const QColor& rcBackColor,
                                              int& rnTop, int& rnLeft, int& rnBottom, int& rnRight,
                                              bool& rbSuccess);

};

#endif // IMAGES_PROCESSOR_HPP