#ifndef TEXTURES_GENERATOR_HPP
#define TEXTURES_GENERATOR_HPP

#include <QImage>

/// Generates various textures.
class TexturesGenerator
{

public:

    /// Initializes properties with basic values.
    TexturesGenerator();

    // Setters.
    TexturesGenerator& setNonTextTexturesImagesPaths(const QList<QString>& rcNonTextTexturesImagesPaths);
    TexturesGenerator& setTextTexturesImagesPaths(const QList<QString>& rcTextTexturesImagesPaths);



    /*!
     * Generates a texture from available samples.
     *
     * \param cnMinCropAndOutSize the min side to crop the texture and texture's output size.
     * \param rOutTexture         to write the generated texture.
     * \param rbOutIsLight        true - if the outputed texture is light, false - otherwise.
     * \param rbSuccessful        shows whether the generation was successful.
     * \param cbNoTextOnly        set it to true if you want to generate textures without texts.
     * \param cnThreshold         a threshold to the most (the least) intensive mean color.
     */
    void generateRandomTexture(const quint16 cnMinCropAndOutSize,
                               QImage& rOutTexture,
                               bool& rbOutIsLight,
                               bool& rbSuccessful,
                               const bool cbNoTextOnly = false,
                               const quint8 cnThreshold = 40) const;





private:

    /// Reloads non-text textures (from provided paths).
    void reloadNonTextTextures();

    /// Reloads textures with texts (from provided paths).
    void reloadTextTextures();



    /// All textures without texts to be loaded.
    QList<QString> m_NonTextTexturesImagesPaths;

    /// All textures with texts to be loaded.
    QList<QString> m_TextTexturesImagesPaths;

    /// To store loaded textures without text.
    QList<QImage> m_LoadedNonTextTextures;

    /// To store loaded textures with text.
    QList<QImage> m_LoadedTextTextures;

};

#endif // TEXTURES_GENERATOR_HPP