#ifndef SYMBOLS_ENCODER_HPP
#define SYMBOLS_ENCODER_HPP

#include <QString>

/// Allows to encode and decode symbols in some required formats.
class SymbolsEncoder
{

public:

    /*!
     * Encodes provided symbol to the corresponding folder name.
     *
     * \param rcSymbolIn a symbol to create the folder name for.
     * \param rDirOut    a dir name for provided symbol.
     * \param rcAlphabet an optional alphabet to check symbol's existence in.
     */
    static void encodeDir(const QChar& rcSymbolIn, QString& rDirOut, const QString& rcAlphabet = QString());

    /*!
     * Converts provided encoded directory name to the corresponding symbol.
     *
     * \param rcDirIn                          a directory to be decoded.
     * \param rSymbolOut                       a decoded output symbol.
     * \param rbSuccess                        a flag whether convertion was successful.
     * \param rcAlphabet                       an optional alphabet to check symbol's existence in.
     * \param cbConvertNonexistentUpperToLower set it to true, if you want to convert upper symbols to lower
     *                                         if the upper one was not presented in the alphabet
     *                                         (but the lower one was presented).
     */
    static void decodeDir(const QString& rcDirIn,
                          QChar& rSymbolOut,
                          bool& rbSuccess,
                          const QString& rcAlphabet = QString(),
                          const bool cbConvertNonexistentUpperToLower = false);



    /*!
     * Converts provided symbol to its code representation.
     *
     * \param rcSrcSymbol a symbol to be encoded.
     * \param rcAlphabet  an alphabet to encode the symbol with.
     * \param rOutCode    encoded symbol's code.
     * \param rbSuccess   if encoding was successful.
     */
    static void encodeSymbol(const QChar &rcSrcSymbol,
                             const QString& rcAlphabet,
                             unsigned long& rOutCode,
                             bool& rbSuccess);

    /*!
     * Decodes provided symbol's code to the symbol itself.
     *
     * \param cnSrcCode  a symbol's code.
     * \param rcAlphabet an alphabet to use for decoding.
     * \param rOutSymbol a decoded symbol.
     * \param rbSuccess  if everything was successful while decoding.
     */
    static void decodeSymbol(const unsigned long cnSrcCode,
                             const QString& rcAlphabet,
                             QChar& rOutSymbol,
                             bool& rbSuccess);





private:

    /// Hiding construction.
    SymbolsEncoder()
    {}

};

#endif // SYMBOLS_ENCODER_HPP