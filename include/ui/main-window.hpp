#ifndef MAIN_WINDOW_HPP
#define MAIN_WINDOW_HPP

#include <QMainWindow>

#include "ui_main-window.h"

#include "ui/selectable-graphics-scene.hpp"
#include "recognition/recognition-engine.hpp"



/// The root window of the UI.
class MainWindow: public QMainWindow, Ui::MainWindow
{
    // Qt features.
    Q_OBJECT

public:

    /// Construction.
    MainWindow(QWidget* pParent = Q_NULLPTR,
               Qt::WindowFlags flags = Qt::WindowFlags());





private slots:

    /// Provides a dialog to select some image.
    void onOpenFile();

    /// Detects text regions automatically and performs their recognition.
    void onDetectAndRecognize();

    /// Recognizes selected by user area.
    void onRecognizeSelected();

    /// Enables or disables menu actions.
    void onUpdateMenuActionsAvailability();





private:

    /// Binds actions for menu items.
    void configureMenuActions();

    /// Makes all preparations for UI.
    void configureUI();



    /// The main scene with selection.
    SelectableGraphicsScene* m_pSelectableGraphicsScene;

    /// To perform text recognitions.
    RecognitionEngine* m_pRecognitionEngine;

    /// An image to perform all ops with.
    QImage m_LoadedImage;

};

#endif // MAIN_WINDOW_HPP