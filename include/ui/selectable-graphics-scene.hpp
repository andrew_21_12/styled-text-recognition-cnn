#ifndef SELECTABLE_GRAPHICS_SCENE_HPP
#define SELECTABLE_GRAPHICS_SCENE_HPP

#include <QGraphicsScene>



/// Simple extension of graphics scene to add some selection features.
class SelectableGraphicsScene : public QGraphicsScene
{
    // Qt features.
    Q_OBJECT

public:

    /// Construction.
    explicit SelectableGraphicsScene(QObject* pParent = Q_NULLPTR);

    /// All mem releases go here.
    ~SelectableGraphicsScene();



    /// Use this by default to clear the whole scene, but to keep selection rect instance in the mem.
    void safeClear();

    /// Turns on the last selection again.
    void selectAgain();

    /// Disables current selection.
    void deselect();

    /*!
     * Public interface to check the state of the selection.
     *
     * \return True - if the selection is active, false - in another case.
     * */
    bool isSelectionActive();

    /*!
     * Public interface to get the current selection area (rectangle).
     *
     * \return Rect with the coordinates of the selection.
     * */
    QRect getSelectionRectangle();

    /*!
     * An utility method to check if something was loaded into the scene.
     *
     * \return True - if something was already loaded into the scene, false - otherwise.
     * */
    bool isSomethingLoaded();



    /*!
     * Adds a label with some recognition.
     *
     * \param rcBounds           recognized rectangle bounds.
     * \param rcRecognizedString recognized string.
     */
    void addRecognitionLabel(const QRect& rcBounds, const QString& rcRecognizedString);

    /// Removes all added recognition labels.
    void removeAllRecognitionLabels();





signals:

    /// Fires when we are turning on selection.
    void selectionApplied();

    /// Fires when we are disabling selection.
    void selectionDisabled();





protected:

    /*!
     * Overriding mouse press event to catch mouse click action.
     *
     * \param pEvent contains the data about click.
     * */
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* pEvent);

    /*!
     * Overriding mouse move event to catch mouse drag action.
     *
     * \param pEvent contains the data about move.
     * */
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* pEvent);

    /*!
     * Overriding mouse release event to catch mouse released action.
     *
     * \param pEvent contains the data about mouse release.
     * */
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* pEvent);





private:

    /*!
     * Evaluates current horizontal scale of the window and loaded image (scene).
     *
     * \return Evaluated coef of the horizontal scale.
     */
    qreal getWidthDensityCoef();

    /*!
     * Evaluates current vertical scale of the window and loaded image (scene).
     *
     * \return Evaluated coef of the vertical scale.
     */
    qreal getHeightDensityCoef();

    /*!
     * Checks if selection's size is big enough to be applied.
     *
     * \param cnSelectionWidth  current selection's width.
     * \param cnSelectionHeight current selection's height.
     *
     * \return True - if current selection is big enough, false - in another case.
     * */
    bool isSelectionBiggerThanMinimalPossible(const int cnSelectionWidth, const int cnSelectionHeight);

    /// The goal of this method is to create selection again after the clearing of this scene.
    void restoreSelection();



    /// The left upper bound of the selection.
    QPoint m_PointStart;

    /// To store the state of the mouse.
    bool m_bIsMousePressed;

    /// Graphical item to draw selection on the scene (and store its coordinates).
    QGraphicsRectItem* m_pGriSelection;

    /// To store and clear recognition rect.
    QList<QGraphicsRectItem*> m_AddedRects;

    /// To store and clear recognized texts.
    QList<QGraphicsTextItem*> m_AddedTexts;

};

#endif // SELECTABLE_GRAPHICS_SCENE_HPP
